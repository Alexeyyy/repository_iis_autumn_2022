import pandas
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

LOW = 1
MIDDLE = 2
HIGH = 3


def solve_task(path):
    dataset = pandas.read_csv(path)

    max_value, average_value, min_value = get_max_average_min_from_list(list(dataset['unemploymentrate']))

    def get_nearest_value(value):
        result = min([max_value, average_value, min_value], key=lambda x: abs(value - x))
        if result == max_value:
            return HIGH
        if result == average_value:
            return MIDDLE
        if result == min_value:
            return LOW

    dataset['unemploymentrate'] = dataset['unemploymentrate'].apply(get_nearest_value)

    dataset['oil prices'] = dataset['oil prices'].apply(round)

    dataset = dataset.loc[(np.isnan(dataset['oil prices']) == False) & (np.isnan(dataset['unemploymentrate']) == False)]

    def calculate_score():
        X = dataset[['oil prices']]
        y = round_value(dataset['unemploymentrate'].copy())

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.01, random_state=42)

        clf = DecisionTreeClassifier(random_state=241)
        clf.fit(X_train, y_train)

        print('\n', X.head())

        print('\nImportance of signs:', clf.feature_importances_)

        return clf.score(X_test, y_test)

    print("\nScore:", calculate_score())


def round_value(dictionary):
    for k, v in dictionary.items():
        dictionary[k] = int(round(dictionary[k]))

    return dictionary


def get_max_average_min_from_list(value):
    return max(value), round((max(value) - min(value)) / 2, 2), min(value)


if __name__ == '__main__':
    solve_task('Economic Data - 9 Countries (1980-2020).csv')
