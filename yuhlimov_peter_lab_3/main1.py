import pandas
import numpy as np
from sklearn.tree import DecisionTreeClassifier


def solve_task(path):
    dataset = pandas.read_csv(path, index_col='PassengerId')

    dataset = dataset.loc[(np.isnan(dataset['Pclass']) == False) & (np.isnan(dataset['Parch']) == False) &
                          (np.isnan(dataset['Fare']) == False)]

    X = dataset[['Pclass', 'Parch', 'Fare']]

    print(X.head())

    y = dataset['Survived']

    clf = DecisionTreeClassifier()
    clf.fit(X, y)

    print('\nImportance of signs:', clf.feature_importances_)


if __name__ == '__main__':

    solve_task('titanic.csv')
