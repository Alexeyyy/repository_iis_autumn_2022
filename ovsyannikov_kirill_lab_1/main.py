import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.datasets import make_circles
from sklearn.neural_network import MLPClassifier

cm_bright = ListedColormap(['#FF0000', '#8B0000'])
cm_bright1 = ListedColormap(['#FF4500', '#FFA500'])

def create_circles():
    x, y = make_circles(noise=0.2, factor=0.5, random_state=0)
    rng = np.random.RandomState(2)
    x += 2 * rng.uniform(size=x.shape)

    x = x[:, np.newaxis, 1]
    x_train, x_test, y_train, y_test = train_test_split(x, y)

    linear_regression(x_train, x_test, y_train, y_test)
    polynomial_regression(x_train, y_train)
    multi_parceptron(x_test, x_train, y_train)


def linear_regression(x_train, x_test, y_train, y_test):
    plt.scatter(x_test, y_test, color='red')
    model = LinearRegression().fit(x_train, y_train)
    y_predict = model.intercept_ + model.coef_ * x_test
    plt.title('Линейная регрессия')
    plt.plot(x_test, y_predict, color='purple')
    plt.show()
    print('------------------------------------------')
    print('Линейная регрессия')
    print('quality assessment:', model.score(x_train, y_train))
    print('------------------------------------------')


def polynomial_regression(x_train, y_train):
    plt.scatter(x_train, y_train, color='red')
    x_poly = PolynomialFeatures(degree=3).fit_transform(x_train)
    pol_reg = LinearRegression()
    model = pol_reg.fit(x_poly, y_train)
    y_predict = pol_reg.predict(x_poly)
    plt.title('Полиномиальная регрессия')
    plt.plot(x_train, y_predict, color='purple')
    plt.show()
    print('------------------------------------------')
    print('Полиномиальная регрессия')
    print('quality assessment:', model.score(x_poly, y_train))
    print('------------------------------------------')

def multi_parceptron(x_test, x_train, y_train):
    sc = StandardScaler()
    sc.fit(x_train)
    x_train_std = sc.transform(x_train)
    x_test_std = sc.transform(x_test)
    model = MLPClassifier(alpha=0.01, hidden_layer_sizes=(100,), random_state=1).fit(x_train_std, y_train)
    plt.scatter(x_train, y_train, color='red')
    plt.title('Многослойный персептрон')
    plt.plot(x_test_std, model.predict(x_test_std), color='purple')
    plt.show()
    print('------------------------------------------')
    print('Многослойный персептрон')
    print('quality assessment:', model.score(x_train, y_train))
    print('------------------------------------------')



create_circles()