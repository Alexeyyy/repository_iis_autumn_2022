

class Parameters:

    def __init__(self, hidden_layer_sizes, activation, solver, alpha, tol, max_fun, max_iter):
        self.hidden_layer_sizes = hidden_layer_sizes
        self.activation = activation
        self.solver = solver
        self.alpha = alpha
        self.tol = tol
        self.max_fun = max_fun
        self.max_iter = max_iter

    def __str__(self):
        return f'hidden_layer_sizes={self.hidden_layer_sizes}; ' + \
               f'activation={self.activation}; ' + \
               f'solver={self.solver}; ' + \
               f'alpha={self.alpha}; ' + \
               f'tol={self.tol}; ' + \
               f'max_fun={self.max_fun}; ' + \
               f'max_iter={self.max_iter}.'
