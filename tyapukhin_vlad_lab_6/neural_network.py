from sklearn.neural_network import MLPClassifier
from sklearn.metrics import mean_absolute_percentage_error
from parametrs import Parameters


def train(x_train, x_test, y_train, y_test, parameters: Parameters):
    clf = MLPClassifier(
        hidden_layer_sizes=parameters.hidden_layer_sizes,
        activation=parameters.activation,
        solver=parameters.solver,
        alpha=parameters.alpha,
        tol=parameters.tol,
        max_fun=parameters.max_fun,
        max_iter=parameters.max_iter,
    )

    clf.fit(x_train, y_train)
    y_predict = clf.predict(x_test)
    score = clf.score(x_test, y_test)

    mape = mean_absolute_percentage_error(y_test, y_predict)

    print(f'Score: {score}\nMAPE: {mape}')
