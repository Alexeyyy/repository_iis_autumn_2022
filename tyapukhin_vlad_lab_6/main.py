from sklearn.model_selection import train_test_split
from data import load
from neural_network import train
from parametrs import Parameters

import warnings
warnings.filterwarnings("ignore")

if __name__ == '__main__':
    X, y, names = load('PKRX.csv')

    for i in range(1):
        parameters = Parameters(
            hidden_layer_sizes=(100, 100),
            activation='logistic',
            solver='lbfgs',
            alpha=1e-6,
            tol=1e-6,
            max_fun=15000,
            max_iter=1000,
        )
        print(f'\nExperiment #{i + 1} with parameters {parameters}')

        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.05, random_state=42)

        train(X_train, X_test, Y_train, Y_test, parameters)
        i += 1
