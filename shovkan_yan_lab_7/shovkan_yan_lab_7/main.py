from text_reader import get_text
from data_generator import evaluate
from train_logic import create_parameters, training

if __name__ == '__main__':
    sequence, char_to_idx, idx_to_char = get_text()
    criterion, scheduler, n_epochs, loss_avg, device, model, optimizer = create_parameters(idx_to_char)

    training(n_epochs, model, sequence, device, criterion, optimizer, loss_avg, scheduler, char_to_idx, idx_to_char)
    model.eval()
    print(evaluate(model, char_to_idx, idx_to_char, device=device, temp=0.3, prediction_len=1000, start_text='. '))
