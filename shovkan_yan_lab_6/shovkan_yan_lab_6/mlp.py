import numpy as np
import pandas
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier


def create_train_and_test(data):
    return train_test_split(
        data[['salary_in_usd', 'company_size']], data['experience_level'], test_size=0.05, random_state=42)


def create_data():
    data = pandas.read_csv('ds_salaries.csv', on_bad_lines='skip')

    data['experience_level'] = data['experience_level'].apply(experience_level_to_num)
    data['company_size'] = data['company_size'].apply(company_size_to_num)
    data['salary_in_usd'] = data['salary_in_usd'].apply(int)

    return data


def experience_level_to_num(experience_level):
    if experience_level == "EN":
        return 0
    if experience_level == "MI":
        return 1
    if experience_level == "SE":
        return 2
    if experience_level == "EX":
        return 3


def company_size_to_num(company_size):
    if company_size == "S":
        return 0
    if company_size == "M":
        return 1
    if company_size == "L":
        return 2


def random_state_fit(x, y, x_test, y_test):
    mlr = MLPClassifier(max_iter=2000, n_iter_no_change=20,
                       activation='relu', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)
    mlr.fit(x, y)
    y_pred = mlr.predict(x_test)
    accuracy = accuracy_score(y_test, np.round(y_pred))

    return accuracy


def evaluate_mlp():
    data = create_data()
    x_train, x_test, y_train, y_test = create_train_and_test(data)

    accuracy = random_state_fit(x_train, y_train, x_test, y_test)

    print("Accuracy: ", accuracy)
