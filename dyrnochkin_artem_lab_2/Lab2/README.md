# Лабораторная работа №2
## Тема: Ранжирование признаков

**Как запустить лабораторную работу**

* Требуется установить и использовать технологии: python, conda, numpy, sklearn
* С помощью команды или компилятора запустить main.py

**Что делает программа**

* Генерирует исходные данные: 750 строк-наблюдений и 14 столбцов-признаков
* Создает и обучает модели: гребневую регрессию, сокращение признаков случайными деревьями, линейную корреляцию
* Находит среднее по каждому признаку

### Тестирование
[Видео](https://disk.yandex.ru/i/uXGvvl8LEb7FsQ)

* По среднему значению самыми важными оказались признаки x1, x4, x2, x14

* По гребневой регрессии x1, x4, x2, x14

* По линейной корреляции x1, x11, x14, x4

* По сокращению признаков случайными деревьями x14, x2, x4, x11


