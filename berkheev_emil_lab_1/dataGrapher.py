from sklearn.svm import SVC
from sklearn.datasets import make_moons, make_circles, make_classification
import numpy as np
import matplotlib.pyplot as plt

names = [
    "Linear",
    "Polynom",
    "Ridge"
]

classifiers = [
    SVC(kernel="linear", C=0.025),
    SVC(kernel="poly", C=0.025),
    SVC(kernel="sigmoid", C=0.025),
]

X, y = make_classification(
    n_features=2, n_redundant=0, n_informative=2, random_state=1, n_clusters_per_class=1
)
rng = np.random.RandomState(2)
X += 2 * rng.uniform(size=X.shape)
linearly_separable = (X, y)

datasets = [
    make_circles(noise=0.2, factor=0.5, random_state=1)
]

figure = plt.figure(figsize=(27, 9))
