import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LinearRegression, Perceptron, Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.preprocessing import PolynomialFeatures
from dataGrapher import names, classifiers, X, y, datasets, figure


class Graph:
    def __init__(self):
        self.iterateData(datasets, names, classifiers, X, y)
        plt.tight_layout()
        plt.show()
        
    def setUpGraph(self, ax, x_min, x_max, y_min, y_max):
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)
        ax.set_xticks(())
        ax.set_yticks(())
        
    def plotPoints(self, ax, X_train, y_train, cm_bright, X_test, y_test):
        # Plot the training points
        ax.scatter(
            X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright, edgecolors="k"
        )
            
        # Plot the testing points
        ax.scatter(
            X_test[:, 0],
            X_test[:, 1],
            c=y_test,
            cmap=cm_bright,
            edgecolors="k",
            alpha=0.6,
        )
        
    def setUpNames(self, ax, ds_cnt, x_max, y_min, score, name):
        if ds_cnt == 0:
            ax.set_title(name)
        ax.text(
            x_max - 0.3,
            y_min + 0.3,
            ("%.2f" % score).lstrip("0"),
            size=15,
            horizontalalignment="right",
        )
        
    def iterateClass(self, names, classifiers, i, X_train, y_train, X_test, y_test, cm_bright, x_min, x_max, y_min, y_max, ds_cnt, ds, cm, X):
        for name, clf in zip(names, classifiers):
            ax = plt.subplot(len(datasets), len(classifiers) + 1, i)
            clf.fit(X_train, y_train)
            score = clf.score(X_test, y_test)
            DecisionBoundaryDisplay.from_estimator(
                clf, X, cmap=cm, alpha=0.8, ax=ax, eps=0.5
            )
            
            self.setUpGraph(ax, x_min, x_max, y_min, y_max)
            self.plotPoints(ax, X_train, y_train, cm_bright, X_test, y_test)
            self.setUpNames(ax, ds_cnt, x_max, y_min, score, name)
            i += 1

    # iterate over datasets
    def iterateData(self, datasets, names, classifiers, X, y):
        i = 1
        for ds_cnt, ds in enumerate(datasets):
            # preprocess dataset, split into training and test part
            X, y = ds
            X = StandardScaler().fit_transform(X)
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size=0.4, random_state=42
            )
            
            x_min, x_max = X[:, 0].min() - 0.5, X[:, 0].max() + 0.5
            y_min, y_max = X[:, 1].min() - 0.5, X[:, 1].max() + 0.5
            
            cm = plt.cm.RdBu
            cm_bright = ListedColormap(["#FF0000", "#0000FF"])
            ax = plt.subplot(len(datasets), len(classifiers) + 1, i)
            if ds_cnt == 0:
                ax.set_title("Input data")
                
            self.plotPoints(ax, X_train, y_train, cm_bright, X_test, y_test)
            self.setUpGraph(ax, x_min, x_max, y_min, y_max)
            i += 1
            
            # iterate over classifiers
            self.iterateClass(names, classifiers, i, X_train, y_train, X_test, y_test, cm_bright, x_min, x_max, y_min, y_max, ds_cnt, ds, cm, X)
            
