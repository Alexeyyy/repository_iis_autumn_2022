import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import AgglomerativeClustering

from constants import *
from cars import create_data


def draw_clusters():
    data = create_data()
    cluster = AgglomerativeClustering(n_clusters=NUMBER_OF_CLUSTERS, affinity='euclidean', linkage='ward')

    plt.xlabel("Дата производства", fontweight="bold")
    plt.ylabel("Цена", fontweight="bold")
    data = data.loc[data['Price'] < MAX_PRICE]

    corr = np.array(data)
    cluster.fit_predict(corr)

    plt.scatter(corr[:, 0], corr[:, 1], c=cluster.labels_, cmap='rainbow')
    plt.show()

