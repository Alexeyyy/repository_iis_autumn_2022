from datetime import datetime

import pandas

def create_data():
    data = pandas.read_csv('car_price_prediction.csv', on_bad_lines='skip')
    return data[['Prod. year', 'Price']]
