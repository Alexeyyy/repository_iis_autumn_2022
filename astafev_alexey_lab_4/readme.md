#Лабораторная работа 4. Кластеризация
##Астафьев Алексей ПИбд-41

**Как запустить лабораторную работу?**

* Необходимо установить: python, sklearn, pandas, plotly
* Запустить класс main

**Что делает программа**

Производит кластеризацию среди данным по инсультам у людей по различным метрикам и кластеризуя их по статусу курения.

*Входные данные*
* Влияющие признаки - "'age', 'hypertension', 'heart_disease', 'avg_glucose_level', 'stroke'". Кластеризуемый признак - Temperature
* Файл healthcare.csv

*Выводы*
Представлены кластеризации по 4 группам c помощью алгоритма t-SNE.

При кластеризации изменялся уровень обучения и количество итерации обучения.


### Результаты тестирования

![alt text](images/img1.jpg)
![alt text](images/img2.jpg)
![alt text](images/img3.jpg)
![alt text](images/img4.jpg)
![alt text](images/img5.jpg)
![alt text](images/img6.jpg)
![alt text](images/img7.jpg)
![alt text](images/img8.jpg)

https://drive.google.com/file/d/1cdkVdqLXeg2QT5ytLkyX8fLTFbHztrhY/view?usp=sharing