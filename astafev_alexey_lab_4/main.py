from sklearn.manifold import TSNE
from data import fit, load
import plotly.express as px
import warnings

def show_plots(x, y, colors, plot_name, labels):
    fig = px.scatter(None, x=x, y=y, labels=labels, opacity=1, color=colors)
    fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')
    fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')

    fig.update_layout(title_text=plot_name, plot_bgcolor='white')
    fig.update_traces(marker=dict(size=4))

    fig.write_html(f'images/imag2.html')


def create_plot_info(m: TSNE):
    return f'learning_rate={m.learning_rate}; ' + \
           f'perplexity={m.perplexity}; ' + \
           f'n_iter={m.n_iter}; ' + \
           f'init={m.init}; ' + \
           f'angle={m.angle}'


if __name__ == '__main__':
    labels = {
        "x": "Smoking_status",
        "y": "Value",
    }

    X, Y = load()
    print(X)
    print(Y)
    warnings.filterwarnings("ignore")
    model = TSNE(
        learning_rate=1000,
        n_iter=1000,
        init='pca',
        perplexity=30,
        angle=0.1,
    )
    x_axis, y_axis = fit(model, X)

    show_plots(x=x_axis, y=y_axis, colors=Y.to_list(), plot_name=create_plot_info(model), labels=labels)


