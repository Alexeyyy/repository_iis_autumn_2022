import pandas as pd

def load():
    dataset = pd.read_csv('healthcare.csv')

    X = dataset[['hypertension', 'heart_disease', 'avg_glucose_level', 'stroke']]
    y = dataset['smoking_status']

    return X, y


def fit(model, x):
    transformed = model.fit_transform(x)

    return transformed[:, 0], transformed[:, 1]
