import numpy as np
import pandas
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction.text import TfidfVectorizer


def gender_to_number(gender):
    if gender == "Male":
        return 0
    return 1


def age_to_number(age):
    limits = age.split(' ')
    left = int(limits[0])
    if limits[2] != 'above':
        right = int(limits[2])
    else:
        right = 60
    return (left + right) / 2


def level_to_number(level):
    if level == "Unfit":
        return 0
    elif level == "Average":
        return 1
    elif level == "Good":
        return 2
    elif level == "Very good":
        return 3
    elif level == "Perfect":
        return 4


def frequency_to_number(frequency):
    if frequency == "Never":
        return 0
    elif frequency == "1 to 2 times a week":
        return 1
    elif frequency == "2 to 3 times a week":
        return 2
    elif frequency == "3 to 4 times a week":
        return 3
    elif frequency == "5 to 6 times a week":
        return 4
    elif frequency == "Everyday":
        return 5


def training_time_to_number(training_time):
    if training_time == "I don't really exercise":
        return 0
    elif training_time == "30 minutes":
        return 1
    elif training_time == "1 hour":
        return 2
    elif training_time == "2 hours":
        return 3
    elif training_time == "3 hours and above":
        return 4


def diet_to_number(diet):
    if diet == "No":
        return 0
    elif diet == "Not always":
        return 1
    elif diet == "Yes":
        return 2


def equipment_to_number(equipment_purchase):
    if equipment_purchase == "No":
        return 0
    return 1


def solve_titanic_classification():
    data = pandas.read_csv('titanic.csv', index_col='Passengerid')
    data = data.loc[(np.isnan(data['Pclass']) == False) & (np.isnan(data['Age']) == False)
                    & (np.isnan(data['Sex']) == False) & (np.isnan(data['Survived']) == False)]
    corr = data[['Pclass', 'Age', 'Sex']]
    print(corr.head())
    y = data['Survived']
    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(corr, y)
    importances = clf.feature_importances_
    print(importances)


def solve_fitness_classification():
    label_encoder = preprocessing.LabelEncoder()
    vectorizer = TfidfVectorizer()

    data = pandas.read_csv('fitness analysis.csv')
    data['Your gender '] = data['Your gender '].apply(gender_to_number)
    data['Your age '] = data['Your age '].apply(age_to_number)
    # data['Your name '] = label_encoder.fit_transform(data['Your name '])
    train_test_feature_matrix = vectorizer.fit_transform(data['Your name ']).toarray()
    a = pandas.DataFrame(train_test_feature_matrix)
    data['Your name '] = a[a.columns[1:]].apply(lambda x: sum(x.dropna().astype(float)), axis=1)
    data['How do you describe your current level of fitness ?'] = data[
        'How do you describe your current level of fitness ?'].apply(level_to_number)
    data['How often do you exercise?'] = data[
        'How often do you exercise?'].apply(frequency_to_number)
    data['How long do you spend exercising per day ?'] = data[
        'How long do you spend exercising per day ?'].apply(training_time_to_number)
    data['Would you say you eat a healthy balanced diet ?'] = data[
        'Would you say you eat a healthy balanced diet ?'].apply(diet_to_number)
    data['Have you ever purchased a fitness equipment?'] = data[
        'Have you ever purchased a fitness equipment?'].apply(equipment_to_number)

    corr = data[['Your name ', 'Your gender ', 'Your age ', 'How do you describe your current level of fitness ?',
                 'How often do you exercise?', 'How long do you spend exercising per day ?',
                 'Would you say you eat a healthy balanced diet ?', 'How healthy do you consider yourself?',
                 'Have you ever purchased a fitness equipment?']]
    print(corr.head())

    y = data['How important is exercise to you ?']

    x_train, x_test, y_train, y_test = train_test_split(corr, y, test_size=0.01, random_state=42)

    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(x_train, y_train)
    print('Оценка качества:', clf.score(x_test, y_test))
    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    solve_titanic_classification()
    solve_fitness_classification()
