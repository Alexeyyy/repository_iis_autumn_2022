# Лабораторная работа №3
## Выполнил: ПИбд-41 Овсянников Кирилл
## Деревья решений
***
### Запуск лабораторной

* Установить python, numpy, sklearn
* Запустить main.py
* Запустить main2.py
***
### Что делает программа?

#### 1 задание
* Данные из варианта: Age, Pclass, Fare.
* По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений),
  в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров два наиболее важных признака из трех рассматриваемых (по варианту).
* В лабораторной была использована модель DecisionTreeClassifier
***
#### 2 задание

* Решите с помощью библиотечной реализации дерева решений задачу из лабораторной работы
  «Веб-сервис «Дерево решений» по предмету «Методы искусственного интеллекта» на 99%
  ваших данных. Проверьте работу модели на оставшемся проценте, сделайте вывод.

* В лабораторной была использована модель DecisionTreeClassifier

#### Видео
* Ссылка: https://youtu.be/ErUxFJtTVP4

