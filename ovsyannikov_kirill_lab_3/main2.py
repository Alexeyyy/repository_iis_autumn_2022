import pandas
from sklearn.tree import DecisionTreeClassifier


def main2():
    data = pandas.read_csv('Lab3_dataset.csv')
    data.dropna()
    print(data)

    train_data = data.iloc[0:int(len(data) * 0.99), :]
    test_data = data.iloc[0:int(len(data) * 0.99):, :]

    clf = DecisionTreeClassifier(random_state=41)
    clf.fit(train_data[['FertRate', 'MedAge']],
            train_data['LandArea'])

    # Выводим значение
    parameters = clf.feature_importances_
    print(parameters)
    score = clf.score(test_data[['FertRate', 'MedAge']],
                      test_data['LandArea'])
    print(score)
