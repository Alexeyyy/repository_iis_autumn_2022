import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

from Lab3.main2 import main2


def main():

    data = pd.read_csv('obsheeZadanie.csv', index_col='Passengerid')
    clf = DecisionTreeClassifier(random_state=255)

    Y = data['2urvived']
    X = data[['Fare', 'Pclass', 'Age']]
    print(X)

    X_train, X_test, y_train, y_test = train_test_split(
        X, Y, test_size=0.05, random_state=42)

    clf.fit(X_train, y_train)

    print(clf.score(X_test, y_test))
    selected = clf.feature_importances_
    print(selected)


if __name__ == '__main__':
    main()
