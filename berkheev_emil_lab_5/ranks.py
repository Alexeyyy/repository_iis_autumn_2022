import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter
from models import create_model
from sklearn.metrics import mean_absolute_percentage_error


def get_ranks(linear, names):
    ranks = dict()

    ranks['Linear'] = sort_by_desc(rank_to_dict(linear.coef_, names))

    return ranks


def sort_by_desc(dictionary):
    return dict(sorted(dictionary.items(), key=itemgetter(1), reverse=True))


def calculate_mean_and_sort_list(ranks):
    mean = {}

    for key, value in ranks.items():
        print(key, value)
        for item in value.items():
            if item[0] not in mean:
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 4)

    return sort_by_desc(mean)


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)

    minmax = MinMaxScaler()

    ranks = minmax.fit_transform(np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 4), ranks)
    return dict(zip(names, ranks))

# Mean absolute percentage error regression loss
def calculate_mape(x_train, x_test, y_train, y_test):
    lasso = create_model(x_train, y_train)
    lasso_predict = lasso.predict(x_test)

    # Convert to lists to calculate MAPE
    y_test = list(y_test)
    lasso_predict = list(lasso_predict)

    return mean_absolute_percentage_error(y_test, lasso_predict)
