# Лабораторная работа #7. Рекуррентная нейронная сеть и задача генерации текста
## Астафьев Алексей ПИбд-41. Вариант 3

[Видео](https://drive.google.com/file/d/1rbnBErPLoF1LH0hsU9Xac_VPElGYwV1o/view?usp=share_link)

### Как запустить лабораторную работу
- Необходимо установить python, keras, tensorflow, numpy
- Запустить класс main

### Какие технологии использовали

Рекуррентные нейронные сети (RNN) — это класс нейронных сетей, которые хороши для моделирования последовательных данных, таких как временные ряды или естественный язык.

- SimpleRNN - Простая рекуррентная сеть состоит из inputs связанных ячеек с одинаковыми параметрами.
- Sequential - Модель подходит для простого стека слоев, где каждый слой имеет ровно один входной тензор и один выходной тензор.
- Embedding - Преобразовывает положительные целые числа (индексы) в плотные (dense) векторы фиксированного размера
### Что она делает

Рекурентрая сеть обучается на отрывке произведения Чака Паланика "Бойцовкий клуб" на английском языке и строит на основе этого новый текст, который начинается со слов "One day I"

Происходит подсчет всех слов отдельно и создается матрица, в которой по трем словам определяется, какое слово будет следовать за ними.

### Тесты

![alt text](images/100эпох1.jpg)
![alt text](images/100эпох2.jpg)

Test1. 100 Epoch. linear method

One day I can only there are to step to life is you have to trust tyler. tyler wanted me to fake a with his boss turned what to it wasn't for me. and the phone and someone into the after the bar out into the banquet with the elevator stopped between the kitchen with cold out hard as concrete to touch you're and he was part of project mayhem. tyler asked what i was the first year when my and i say the rule of project mayhem is you have to trust tyler. tyler wanted me to fake a with his boss

Test2. 20 Epoch. tanh method

One day I can around the of the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and the and

Test3. 100 Epoch. tanh method

One day I can around on and something marla says. the job in the whole world must be toilet i take the paper between two fingers and it out of his hand. the edge must his because his hand to his mouth and the commissioner said no. and please. me. no. them. and the space monkey says i'm but you're and he you're too young to here. marla says get the space monkey says i'm but you're and he you're too young to here. marla says get the space monkey says i'm but you're and he you're too young to here. marla says

Test4. 20 Epoch. linear method

One day I could be in a where you can i know about the marla said the elevator door to open end of the and the of the world. imagine tyler said windows and the side of the building and glass and on her and it was the whole of everything after the fat the barrel of the gun pressed against the back of the corniche the guy says but you have to the mischief and no questions. no and no i am stupid and all i can see the floor-to-ceiling windows and down to the house and cut up under the one

Test5. 120 Epoch. linear method

One day I guys at run these stand around in their smoking long thin the cars you are completely that night anyone knew any better a lot of a space animals myself totally had i know what i need to make a naked and angle got into theaters to run a movie projector is the people are walking toward the wet kiss are not tyler durden but you are you know coming back to me patrick madden was dead patrick madden tonight he says we're all people but people are and people sitting beside me from to say no sir used second says

Вывод: Из-за небольшого количества обучения эпох, фразы могут зациклиться из-за плохого обучения, чем больше эпох, тем более разнообразный текст.
Т.к. оригинальном тексте. в основном описываются мысли главного героя, и описание предметов и событий с помощью странных сравнении. Получившиеся тексты имеют такой же характер.