import pandas

text_file = 'resources/451.txt'
women_file = 'resources/women.csv'


def load_women(size=10000):
    data = pandas.read_csv(women_file)
    data = data[data['class_name'] == 'Sweaters']
    return str(data['review_text'].tolist())[:size]


def load_text(size=10000):
    return open(text_file, encoding="utf8").read(size)


print(load_women().__len__())