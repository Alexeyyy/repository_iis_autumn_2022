import numpy
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint

from data_forming import tokenize_words, get_x_y, print_predictions
from load_data import load_women
from models import get_model

if __name__ == '__main__':
    file = load_women()
    # preprocess the input data, make tokens
    processed_inputs = tokenize_words(file)
    # Получаем цифры
    chars = sorted(list(set(processed_inputs)))
    char_to_num = dict((c, i) for i, c in enumerate(chars))
    # Выводим количество цифр
    input_len = len(processed_inputs)
    vocab_len = len(chars)
    print("Total number of characters:", input_len)
    print("Total vocab:", vocab_len)

    seq_length = 100
    x_data, y_data = get_x_y(char_to_num, processed_inputs, input_len, seq_length)

    n_patterns = len(x_data)
    print("Total Patterns:", n_patterns)

    X = numpy.reshape(x_data, (n_patterns, seq_length, 1))
    X = X / float(vocab_len)

    y = np_utils.to_categorical(y_data)

    model = get_model(X, y)

    filepath = "resources/model_weights_saved.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    desired_callbacks = [checkpoint]

    model.fit(X, y, epochs=10, batch_size=256, callbacks=desired_callbacks)

    filename = "resources/model_weights_saved.hdf5"
    model.load_weights(filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam')

    num_to_char = dict((i, c) for i, c in enumerate(chars))

    start = numpy.random.randint(0, len(x_data) - 1)
    pattern = x_data[start]
    print("Random Seed:")
    print("\"", ''.join([num_to_char[value] for value in pattern]), "\"")

    print_predictions(vocab_len, model, num_to_char)
