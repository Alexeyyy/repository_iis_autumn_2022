import sys

import numpy
from nltk import RegexpTokenizer
from nltk.corpus import stopwords


def tokenize_words(input):
    # lowercase everything to standardize it
    input = input.lower()

    # instantiate the tokenizer
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(input)

    # if the created token isn't in the stop words, make it part of "filtered"
    filtered = filter(lambda token: token not in stopwords.words('english'), tokens)
    return " ".join(filtered)


def get_x_y(char_to_num, processed_inputs, input_len, seq_length):
    x_data = []
    y_data = []

    # loop through inputs, start at the beginning and go until we hit
    # the final character we can create a sequence out of
    for i in range(0, input_len - seq_length, 1):
        # Define input and output sequences
        # Input is the current character plus desired sequence length
        in_seq = processed_inputs[i:i + seq_length]
        # Out sequence is the initial character plus total sequence length
        out_seq = processed_inputs[i + seq_length]
        # We now convert list of characters to integers based on
        # previously and add the values to our lists
        x_data.append([char_to_num[char] for char in in_seq])
        y_data.append(char_to_num[out_seq])

    return x_data, y_data


def print_predictions(vocab_len, model, num_to_char, size=1000):
    for i in range(1000):
        x = numpy.reshape(pattern, (1, len(pattern), 1))
        x = x / float(vocab_len)
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)
        result = num_to_char[index]

        sys.stdout.write(result)

        pattern.append(index)
        pattern = pattern[1:len(pattern)]
