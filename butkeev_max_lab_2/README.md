# Лабораторная работа №2
## Тема: Ранжирование признаков

**Как запустить лабораторную работу**

* Требуется установить и использовать технологии: python, conda, numpy, sklearn
* С помощью команды или компилятора запустить main.py

**Что делает программа**

* Генерирует исходные данные: 750 строк-наблюдений и 14 столбцов-признаков
* Задает функцию-выход: регрессионную проблему Фридмана и добавляет зависимости признаков
* Создает и обучает модели: линейную регрессию, RFE, RFR
### Тестирование
[Видео](https://disk.yandex.ru/i/jV8_azMiPYNpLA)
![alt text](Result.png "Result")

* По среднему значению самыми важными оказались признаки x1, x4, x2, x11

* По лассо x1, x4, x2, x11
Метод отдал предпочтения важным элементам, тут не был отобран x3, но у него высокая оценка

* По RFE x1, x2, x3, x4
Метод отдал предпочтения важным элементам, так же он дал очень высокую оценку
взаимозависимым элементам x5, x11, x13

По RFR x14, x12, x2, x1
Метод отдал предпочтения важным элементам, тут не был отобран x14, но у него высокая оценка относительно остальных

Из рассмотренных моделей лучше всего определила признаки модель RFF

