from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Lasso
from sklearn.feature_selection import RFE, f_regression
from sklearn.preprocessing import MinMaxScaler
from constants import *
import numpy as np


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_data():
    np.random.seed(0)
    X = np.random.uniform(0, 1, (OBSERVATION_STRINGS, COLUMNS_SIGNS))

    Y = (FRIEDMAN_NUM * np.sin(np.pi * X[:, 0] * X[:, 1]) + FRIEDMAN_NUM2 * (X[:, 2] - .5) ** 2 +
         FRIEDMAN_NUM * X[:, 3] + FRIEDMAN_NUM3 * X[:, 4] ** FRIEDMAN_NUM3 + np.random.normal(0, 1))

    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (OBSERVATION_STRINGS, 4))

    lasso = Lasso(alpha=0.001)
    lasso.fit(X, Y)

    rfe = RFE(lasso, step=2)
    rfe.fit(X, Y)

    rfr = RandomForestRegressor(max_depth=2, random_state=0)
    rfr.fit(X,Y)

    return lasso, rfe, rfr


def create_ranks():
    data = create_data()
    names = ["x%s" % i for i in range(1, COLUMNS_SIGNS+1)]

    ranks = dict()

    ranks["LASSO"] = rank_to_dict(data[0].coef_, names)
    ranks["RFE"] = rank_to_dict(-1 * data[1].ranking_ + np.max(data[1].ranking_), names)
    ranks["RFR"] = rank_to_dict(data[2].feature_importances_, names)

    return ranks


def calculate_mean(ranks):
    mean = {}
    for key, value in ranks.items():
        for item in value.items():
            if (item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)
    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print("MEAN", mean)


def get_ranks_and_mean():
    ranks = create_ranks()
    for obj in ranks.items():
        print(obj)

    calculate_mean(ranks)
