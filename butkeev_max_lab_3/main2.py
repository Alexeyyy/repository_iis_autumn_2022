from datetime import datetime

import pandas
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor


def create_data():
    data = pandas.read_csv('Netflix_Originals.csv', on_bad_lines='skip')

    unique_numbers = list(set(data['Genre']))
    data['Genre'] = data['Genre'].apply(unique_numbers.index)
    data['IMDB Score'] = data['IMDB Score'].apply(int)

    return data


def processing_data():
    data = create_data()

    corr = data[['Runtime', 'Genre']]
    print(corr)

    y = data['IMDB Score']
    clf = DecisionTreeClassifier(random_state=241)

    X_train, X_test, y_train, y_test = train_test_split(
        corr, y, test_size=0.01, random_state=42)

    clf.fit(X_train, y_train)

    print(clf.score(X_test, y_test))

    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    processing_data()
