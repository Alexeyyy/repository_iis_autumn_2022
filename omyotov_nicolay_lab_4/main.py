import plotly.express as px
import pandas as pd
from sklearn.manifold import TSNE


def load_data(filename):
    data = pd.read_csv(filename, index_col='filename')
    Y = data['label']
    X = data[[
        'chroma_stft_var',
        'rms_var',
        # 'spectral_centroid_var',
        # 'spectral_bandwidth_var',
        # 'rolloff_var',
        # 'zero_crossing_rate_var',
        'harmony_var',
        # 'perceptr_var',
        'tempo',
        # 'mfcc1_var',
        # 'mfcc2_var',
        # 'mfcc3_var',
        # 'mfcc4_var',
        # 'mfcc5_var',
        # 'mfcc6_var',
        # 'mfcc7_var',
        # 'mfcc8_var',
        # 'mfcc9_var',
        # 'mfcc10_var',
        # 'mfcc11_var',
        # 'mfcc12_var',
        # 'mfcc13_var',
        # 'mfcc14_var',
        # 'mfcc15_var',
        # 'mfcc16_var',
        # 'mfcc17_var',
        # 'mfcc18_var',
        # 'mfcc19_var',
        # 'mfcc20_var',
        # 'chroma_stft_mean',
        # 'rms_mean',
        # 'spectral_centroid_mean',
        # 'spectral_bandwidth_mean',
        # 'rolloff_mean',
        # 'zero_crossing_rate_mean',
        # 'harmony_mean',
        # 'perceptr_mean',
        # 'mfcc1_mean',
        # 'mfcc2_mean',
        # 'mfcc3_mean',
        # 'mfcc4_mean',
        # 'mfcc5_mean',
        # 'mfcc6_mean',
        # 'mfcc7_mean',
        # 'mfcc8_mean',
        # 'mfcc9_mean',
        # 'mfcc10_mean',
        # 'mfcc11_mean',
        # 'mfcc12_mean',
        # 'mfcc13_mean',
        # 'mfcc14_mean',
        # 'mfcc15_mean',
        # 'mfcc16_mean',
        # 'mfcc17_mean',
        # 'mfcc18_mean',
        # 'mfcc19_mean',
        # 'mfcc20_mean',
    ]]

    return X, Y


def fit_model(model, X):
    transformed = model.fit_transform(X)

    return transformed[:, 0], transformed[:, 1]


def show_plots(Y, x_axis, y_axis, plot_name, iteration: int):
    only_labels = Y.to_list()
    fig = px.scatter(None, x=x_axis, y=y_axis,
                     labels={
                         "x": "Dimension 1",
                         "y": "Dimension 2",
                     },
                     opacity=1, color=only_labels)
    fig.update_layout(dict(plot_bgcolor='white'))
    fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')
    fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')

    fig.update_layout(title_text=plot_name)
    fig.update_traces(marker=dict(size=3))
    fig.write_image('imgs/'+str(iteration)+'. '+plot_name+".png")


def build_launch_name(filename: str, model: TSNE):
    data = [
        filename, ' t-SNE ',
        'learning_rate=', str(model.learning_rate), ' ',
        'perplexity=', str(model.perplexity), ' ',
        'early_exaggeration=', str(model.early_exaggeration), ' ',
        'n_iter=', str(model.n_iter), ' ',
        'n_iter_without_progress=', str(model.n_iter_without_progress), ' ',
        'min_grad_norm=', str(model.min_grad_norm), ' ',
        'metric=', str(model.metric), ' ',
        'init=', str(model.init), ' ',
        'method=', str(model.method), ' ',
        'angle=', str(model.angle),
    ]
    return ''.join(data)


def launch(filename, X, Y, model: TSNE, iteration: int):
    x_axis, y_axis = fit_model(model, X)
    plot_name = build_launch_name(filename, model)
    show_plots(Y, x_axis, y_axis, plot_name=plot_name, iteration=iteration)


def main():
    iteration = 90
    for filename in ['genres_features_30_sec.csv', 'genres_features_3_sec.csv']:
        X, Y = load_data(filename)
        for learning_rate in [10, 200, 580, 770, 1000]:
            iteration += 1
            print('iteration', iteration)
            model = TSNE(learning_rate=learning_rate, init='random',)
            launch(filename=filename, X=X, Y=Y,
                   model=model, iteration=iteration)


main()
