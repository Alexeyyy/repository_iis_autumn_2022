from sklearn.linear_model import LinearRegression, Ridge
from sklearn.feature_selection import f_regression


def make_linear_regression(x, y):
    model = LinearRegression()
    model.fit(x, y)
    return model


def make_ridge_regression(x, y):
    model = Ridge()
    model.fit(x, y)
    return model


def make_f_regression(x, y):
    model = f_regression(x, y, center=True)
    return model
