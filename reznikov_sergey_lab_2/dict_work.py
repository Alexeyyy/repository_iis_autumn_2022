import numpy as np
from sklearn.preprocessing import MinMaxScaler


def get_mean(ranks):
    mean = dict()

    for key, value in ranks.items():
        for item in value.items():
            if item[0] not in mean:
                mean[item[0]] = 0
            mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    mean = dict(sorted(mean.items(), key=lambda rec: rec[1], reverse=True))
    return mean


# Генерим словарь "коэффициент - значение"
def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))
