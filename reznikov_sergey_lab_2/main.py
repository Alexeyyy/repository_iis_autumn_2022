from dataset import generate_data
import models
import dict_work

if __name__ == "__main__":
    # Создаём данные
    x, y = generate_data()
    linear = models.make_linear_regression(x, y)
    ridge = models.make_ridge_regression(x, y)
    f_regression = models.make_f_regression(x, y)

    # Создаём словарь и добавляем туда данные по регрессиям
    names = ["x%s" % i for i in range(1, 15)]
    ranks = dict()
    ranks["Linear"] = dict_work.rank_to_dict(linear.coef_, names)
    ranks["Ridge"] = dict_work.rank_to_dict(ridge.coef_, names)
    ranks["f_regression"] = dict_work.rank_to_dict(f_regression[0], names)

    # Получаем средние значения
    mean = dict_work.get_mean(ranks)
    print("Средние значения:")
    print(mean)

    for key, value in ranks.items():
        ranks[key] = dict(sorted(value.items(), key=lambda item: item[1], reverse=True))
    # Выводим значения по каждой модели
    for key, value in ranks.items():
        print(key)
        print(value)
