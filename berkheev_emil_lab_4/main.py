from sklearn.cluster import KMeans
import pandas
import numpy as np
from matplotlib import pyplot as plt

factors = ["minTFact","maxTFact","glevelFact","longFact", "qFact"]
kmeans = KMeans(n_clusters=3)
data = pandas.read_csv("weather.csv")

i=0
for i in range(len(factors)):
    corr = data[["Temp", factors[i]]]
    kmeans.fit(corr)
    plt.xlabel(factors[i], fontsize=14, fontweight="bold")
    plt.ylabel("Temp", fontsize=14, fontweight="bold")

    plt.scatter(corr.values[:, 0], corr.values[:, 1], c=kmeans.labels_)
    plt.show()
