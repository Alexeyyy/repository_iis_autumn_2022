from datetime import datetime

import pandas
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor


def date_to_season_to_num(date_str):
    date = datetime.strptime(date_str, "%m/%d/%Y %H:%M")
    if date.month == 12 or date.month <= 2:
        return 0
    if date.month >= 3 or date.month <= 5:
        return 1
    if date.month >= 6 or date.month <= 8:
        return 2
    if date.month >= 9 or date.month <= 11:
        return 3


def create_data():
    data = pandas.read_csv('scrubbed.csv', on_bad_lines='skip')

    data['datetime'] = data['datetime'].apply(date_to_season_to_num)
    data['duration (seconds)'] = data['duration (seconds)'].apply(int)

    unique_numbers = list(set(data['city']))
    data['city'] = data['city'].apply(unique_numbers.index)

    return data


def processing_data():
    data = create_data()

    corr_vonappa = data[['datetime', 'city']]
    print(corr_vonappa)

    y = data['duration (seconds)']
    clf = DecisionTreeClassifier(random_state=241)

    X_train, X_test, y_train, y_test = train_test_split(
        corr_vonappa, y, test_size=0.01, random_state=42)

    clf.fit(X_train, y_train)

    print(clf.score(X_test, y_test))

    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    processing_data()