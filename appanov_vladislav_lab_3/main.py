import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np


def sex_to_bool(sex):
    if sex == "male":
        return 0
    return 1


def processing_data():
    data = pandas.read_csv('titanic.csv', index_col='Passengerid')

    data['Sex'] = data['Sex'].apply(sex_to_bool)

    data = data.loc[(np.isnan(data['Sex']) == False) & (np.isnan(data['Age']) == False)
                    & (np.isnan(data['sibsp']) == False)]

    corr_vonappa = data[['Sex', 'Age', 'sibsp']]
    print(corr_vonappa)

    y = data['2urvived']

    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(corr_vonappa, y)

    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    processing_data()

