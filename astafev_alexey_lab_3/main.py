import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier as tree

def output_data(X, Y):
    print(X)

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.05, random_state=42)

    clf = tree(random_state=241).fit(X_train, y_train)

    print("SCORE:")
    print(clf.score(X_test, y_test))

    print("MEANS:")
    print(clf.feature_importances_)


def titanic_data():
    data = pnd.read_csv("traintitanic.csv", index_col="Passengerid")
    X = data[["Age", "sibsp", "Parch", ]]
    Y = data["Survived"]
    output_data(X, Y)


def healthcare_data():
    data = pnd.read_csv("healthcare.csv", index_col="id")
    X = data[["age", "hypertension", "heart_disease", "glucose_level", ]]
    Y = data["stroke"]
    output_data(X, Y)

print("________________Titanic_Dataset________________")
titanic_data()
print("________________Healthcare_Dataset________________")
healthcare_data()
