# Лабораторная работа №3
## Астафьев Алексей ПИбд-41
## Тема: Деревья решений

**Как запустить лабораторную работу**

* Необходимо установить: python, sklearn, pandas
* Запустить класс main

**Что делает программа**

* Выполняет ранжирование признаков для регрессионной модели

**Что делает программа**

Программа решает задачу классификации о пассажирах Титаника с помощью дерева решений, в которой по характеристикам пассажиров необходимо найти у выживших пассажиров два наиболее важных признака из трех рассматриваемых.

*Входные данные:*

* Признаки - Age, sibsp, Parch
* CSV-файл - traintitanic.csv

*Вывод:*
Из полученных данных можно сделать вывод, что возраст важная характеристика, остальные две оказались менее важными.
![avatar](image/tit_res.jpg)


Решите с помощью библиотечной реализации дерева решений задачу из лабораторной работы «Веб-сервис «Дерево решений» по предмету «Методы искусственного интеллекта» на 99% ваших данных.
Также требуется найти по различным характеристикам наиболее влиятельные признаки.
Проверьте работу модели на оставшемся проценте, сделайте вывод.

*Входные данные*
* Основной - наличие инсульта(stroke), в качестве зависимых "age", "hypertension", "heart_disease", "glucose_level".
* Файл healthcare.csv

*Вывод:*
Из полученных данных можно сделать вывод, что возраст и уровень глюкозы важные характеристики, остальные оказались менее важными.
![avatar](image/heal_res.jpg)

https://drive.google.com/file/d/1sMXIw8ivcz2GaTtM5FvyEU5IOdCwJbZo/view?usp=sharing