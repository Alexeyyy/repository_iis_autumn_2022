## Лабораторная работа №1

### Работа с типовыми наборами данных и различными моделями

#### Выполнила студентка группы ПИбд-41 __Савкина Ксения__ (Вариант № 21)

__Запуск лабораторной работы__:

* установить python, numpy, matplotlib, sklearn
* ввести команду python main.py

__Язык программировния__: `Python`

__Среда разработки__: `PyCharm`

__Ссылка на видео__: https://youtu.be/3LP2FG3tTjc

__Описание__: программный продукт для сравнения математических моделей, построенных по типу данных make_classification.

Сравниваемые модели:

* линейная регрессия
* полиномиальная регрессия (со степенью 5)
* гребневая полиномиальная регрессия (со степенью 5, alpha = 1.0)

Программа строит график и выводит оценку качества для каждой модели. Оценкой качества является коэффициент детерминации,
чем ближе эта величина к 1, тем лучше модель описывает реальность.

__Тесты__:

* Линейная регрессия

![linear](linear.png)

Оценка качества: 0.4550664548628398

* Полиномиальная регрессия

![polynomial](polynomial.png)

Оценка качества: 0.5151094826547338

* Гребневая полиномиальная регрессия

![ridge_polynomial](ridge_polynomial.png)

Оценка качества: 0.5150887186864794

Полиномиальная и гребневая полиномиальная регрессии показывают практически одинаковую оценку, линейная регрессия имеет
наименьшую оценку.





