import config
import numpy as np


def generate_dataset():
    np.random.seed(0)
    size = 750
    x = np.random.uniform(0, 1, (size, config.FEATURES_AMOUNT))

    # Задаем функцию-выход: регрессионную проблему Фридмана
    y = (10 * np.sin(np.pi*x[:, 0]*x[:, 1]) + 20*(x[:, 2] - .5)**2 +
         10*x[:, 3] + 5*x[:, 4]**5 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    x[:, 10:] = x[:, :4] + np.random.normal(0, .025, (size, 4))
    return x, y
