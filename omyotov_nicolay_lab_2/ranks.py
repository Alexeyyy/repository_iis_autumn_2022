import config
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(config.FEATURES_AMOUNT, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def flip_array(arr):
    return-1 * arr + np.max(arr)


def get_ranks(lasso, rfe, f):
    ranks = dict()
    names = ["x%s" % i for i in range(1, config.FEATURES_AMOUNT+1)]

    ranks[config.LASSO_TITLE] = rank_to_dict(lasso.coef_, names)
    ranks[config.RFE_TITLE] = rank_to_dict(flip_array(rfe.ranking_), names)
    ranks[config.F_REGRESSION_TITLE] = rank_to_dict(f, names)

    return ranks


def calc_mean(ranks):
    mean = {}
    for key, value in ranks.items():
        print(key, value)
        for item in value.items():
            if(item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]
    for key, value in mean.items():
        res = value/len(ranks)
        mean[key] = round(res, 2)
    return sorted(mean.items(), key=itemgetter(1), reverse=True)
