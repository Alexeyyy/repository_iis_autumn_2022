from sklearn.linear_model import Lasso
from sklearn.feature_selection import RFE, f_regression


def fit_models(x, y):
    lasso = Lasso(alpha=0.001)
    lasso.fit(x, y)

    rfe = RFE(lasso, step=2)
    rfe.fit(x, y)

    f, pval = f_regression(x, y, center=False)

    return lasso, rfe, f
