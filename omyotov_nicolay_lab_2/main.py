from dataset import generate_dataset
from fit import fit_models
from ranks import calc_mean, get_ranks

x, y = generate_dataset()

lasso, rfe, f = fit_models(x, y)

ranks = get_ranks(lasso, rfe, f)

mean = calc_mean(ranks)

print("MEAN", mean)
