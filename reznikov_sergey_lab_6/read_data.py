import pandas as pd


def read_csv(file_name):
    return pd.read_csv(file_name).dropna()


def get_2_D(data):
    return data[[
        'GDP ($ per capita)', 'Literacy (%)', 'Infant mortality (per 1000 births)']]


def get_1_D(data):
    return data[[
        'GDP ($ per capita)', 'Infant mortality (per 1000 births)']]


def get_3_D(data):
    return data[[
        'GDP ($ per capita)', 'Literacy (%)', 'Growth Rate', 'Infant mortality (per 1000 births)']]
