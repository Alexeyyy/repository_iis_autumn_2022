from MLP import create_train_and_test, random_state_fit
from read_data import read_csv, get_1_D, get_2_D, get_3_D

mixed_data_file = "resources/mixed_data.csv"


if __name__ == '__main__':
    data = get_1_D(read_csv(mixed_data_file))

    accuracy = 0
    X_train, X_test, y_train, y_test = create_train_and_test(data)
    for i in range(100):
        current_accuracy = random_state_fit(X_train, y_train, X_test, y_test)
        print(f"itt {i} accuracy {current_accuracy}")
        accuracy = accuracy + current_accuracy
    print(f"столбцы {data.columns}, Точность (100 иттераций)", accuracy/100)

