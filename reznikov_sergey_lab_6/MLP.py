import numpy as np
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor


def create_train_and_test(data):
    if len(data.columns) == 3:
        return train_test_split(
            data[['GDP ($ per capita)', 'Literacy (%)']], data['Infant mortality (per 1000 births)'], test_size=0.1)
    elif len(data.columns) == 2:
        return train_test_split(
            data[['GDP ($ per capita)']], data['Infant mortality (per 1000 births)'], test_size=0.1)
    else:
        return train_test_split(
            data[['GDP ($ per capita)', 'Literacy (%)', 'Growth Rate']], data['Infant mortality (per 1000 births)'],
            test_size=0.1)


def random_state_fit(x, y, x_test, y_test):
    mlr = MLPRegressor(max_iter=2000,
                       activation='identity', hidden_layer_sizes=[2], tol=0.00001, solver="lbfgs", random_state=1)
    mlr.fit(x, y)
    y_predict = mlr.predict(x_test)
    accuracy = r2_score(y_test, y_predict)
    return accuracy
