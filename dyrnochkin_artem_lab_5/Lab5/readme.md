###### ПИбд-41 Дырночкин Артём

# Лабораторная работа №5

## Регрессия

### Как запустить лабораторную работу

1. Установить python, pandas, sklearn
1. Запустить команду `python main.py`

### Использованные технологии

python, pandas, sklearn

### Что делает программа?

Считывает данные из файла и находит точность по признакам в гребневой модели.

### Тестирование
[Видео](https://disk.yandex.ru/i/rI57Pdy-VGLDuw)

