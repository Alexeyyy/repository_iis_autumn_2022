import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data.csv', sep=',')
features = ['moving_time', 'max_speed', 'difficulty', 'min_elevation', 'downhill', 'length_2d']

# prepare data for classification
data2 = pd.DataFrame()
le = LabelEncoder()
data2['length_2d'] = le.fit_transform(data['length_2d'])
data2['moving_time'] = data['moving_time']
unique_numbers = list(set(data['difficulty']))
data2['difficulty'] = data['difficulty'].apply(unique_numbers.index)
scaler = preprocessing.MinMaxScaler()
data2 = pd.DataFrame(data=scaler.fit_transform(data2), columns=data2.columns)


def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram
    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack(
        [model.children_, model.distances_, counts]
    ).astype(float)

    dendrogram(linkage_matrix, **kwargs)


X = data2
# Agglomerative Clustering.
# Recursively merges a pair of clusters of sample data; uses linkage distance.
model = AgglomerativeClustering(distance_threshold=0, n_clusters=None)
model = model.fit(X)
plt.title("Hierarchical Clustering Dendrogram for Hikes")
# build dendrogram
plot_dendrogram(model, truncate_mode="level", p=3)
plt.show()
