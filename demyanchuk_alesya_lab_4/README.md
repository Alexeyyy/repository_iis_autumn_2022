## Лабораторная работа №4

### Кластеризация

## Выполнила студентка группы ПИбд-41 Алеся Демянчук

### Как запустить лабораторную работу:

* установить python, numpy, matplotlib, sklearn
* запустить проект (стартовая точка класс main)

### Какие технологии использовались:

* ЯП `Python`, библиотеки numpy, matplotlib, sklearn
* Среда разработки `IntelliJ IDEA`

### Что делает лабораторная работа:

* Кластеризирует данные о горных походах исходя из трудности похода, протяженности маршрута и времени, затраченного на маршрут. Ожидаем, что разбиение маршрутов будет на три кластера (простой, средний, сложный).

### Примеры работы:

#### Результаты:
### Кластеризация разбила наши походы на три большие группы, как мы этого и ожидали, значит алгоритм с задачей справился.

#### Графики результатов кластеризации:

![This is an image](hikefourlab.png)

***Ссылка на видео https://drive.google.com/file/d/1vosI4V0PxfTBglzG24ftUBjJINkgEIbk/view?usp=sharing***
