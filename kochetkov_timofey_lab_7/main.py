import numpy
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from data import load_text
from config import *
from helper import compute_seq,predict
from model import define_model, compile, fit,load_model_weights

raw_text = load_text()
# сопоставление уникальных символов с целыми числами
chars = sorted(list(set(raw_text)))
char_to_int = dict((c, i) for i, c in enumerate(chars))

n_chars = len(raw_text)
n_vocab = len(chars)
print("Всего символов: ", n_chars)
print("Различных символов: ", n_vocab)

# подготовить набор данных
seq_length = len(INPUT_SEQ)
dataX, dataY = compute_seq(raw_text,char_to_int, seq_length)
n_patterns = len(dataX)
print("Кол-во паттернов обучения: ", n_patterns)

# изменить форму X на [выборки, временные шаги, функ]
X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
X = X / float(n_vocab)

# преобразование
y = np_utils.to_categorical(dataY)

#получаем модель
model = define_model(X, y)

#загружаем веса, если есть файлы
load_model_weights(model,'weights-improvement-20-1.7184.hdf5')

model.compile(loss='categorical_crossentropy', optimizer='adam')

# определяем точки
filepath="weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]
# заполняем модель
#fit(model, X, y, callbacks_list)

#или предсказываем
pattern = [char_to_int[char] for char in INPUT_SEQ.lower()]
predict(model, pattern, chars, n_vocab, seq_length)
