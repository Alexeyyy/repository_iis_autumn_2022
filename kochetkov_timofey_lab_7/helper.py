import sys
import numpy

def compute_seq(text, char_to_int, length):
    dataX = []
    dataY = []

    for i in range(0, len(text) - length, 1):
        seq_in = text[i:i + length]
        seq_out = text[i + length]

        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])

    return dataX, dataY

def predict(model, pattern, chars, n_vocab, prediction_len):
    print("Предсказание:\n")
    for i in range(prediction_len):
        x = numpy.reshape(pattern, (1, len(pattern), 1)) / float(n_vocab)
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)

        sys.stdout.write(chars[index])

        pattern.append(index)
        pattern = pattern[1:len(pattern)]

