from keras import Sequential
from keras.layers import LSTM, Dropout, Dense
from config import *

def define_model(X, y):
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    return model

def compile(model):
    model.compile(loss='categorical_crossentropy', optimizer='adam')

def fit(model, X, y, callbacks_list):
    model.fit(X, y, epochs=EPOCH_SIZE, batch_size=BATCH_SIZE, callbacks=callbacks_list)

def load_model_weights(model, filename):
    model.load_weights(filename)
