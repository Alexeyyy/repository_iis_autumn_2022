import numpy as np
from sklearn.datasets import make_moons
from Models import Models
from sklearn.model_selection import train_test_split

models_manager = Models()

data = make_moons(noise=0.3, random_state=0)

X, y = data

X = X[:, np.newaxis, 1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)

models_manager.linear(X_train, X_test, y_train, y_test)

models_manager.polynomial(X_train, y_train)

models_manager.ridge(X_train, X_test, y_train, y_test)
