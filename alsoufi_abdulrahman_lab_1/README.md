# Лабораторная работа №1
## Тема: Работа с типовыми наборами данных и различными моделями
## вариант-1
## Аль-суфи Абдулрахман Али Хассан - ПИбд-41

**Как запустить лабораторную работу**

* Использовать технологии: python, matplotlib, numpy, sklearn

**Что делает программа**

* Генерирует данные  make_moons (noise=0.3, random_state=rs)
* Сравнивает 3 модели: 
  1. Linear Regression.
  2. Polynomial Regression (with a degree of 3).
  3. Ridged Polynomial Regression (with degree 3, alpha = 1.0).

### Video
[Video](https://youtu.be/JRJi9kxV5JA)
