import numpy as np
from matplotlib import pyplot as plt
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

x, y = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=1,
                           n_clusters_per_class=1)


def linear(x_train, x_test, y_train, y_test):
    plt.scatter(x_test, y_test)
    model = LinearRegression().fit(x_train, y_train)
    y_predict = model.intercept_ + model.coef_ * x_test
    plt.title('Линейная регрессия')
    plt.plot(x_test, y_predict, color='red')
    plt.show()
    print('Линейная регрессия')
    print('Оценка качества:', model.score(x_train, y_train))


def polynomial(x_train, y_train):
    plt.scatter(x_train, y_train)
    x_poly = PolynomialFeatures(degree=5).fit_transform(x_train)
    pol_reg = LinearRegression()
    model = pol_reg.fit(x_poly, y_train)
    y_predict = pol_reg.predict(x_poly)
    plt.title('Полиномиальная регрессия')
    plt.plot(x_train, y_predict, color='red')
    plt.show()
    print('Полиномиальная регрессия')
    print('Оценка качества:', model.score(x_poly, y_train))


def ridge_polynomial(x_train, x_test, y_train, y_test):
    plt.scatter(x_test, y_test)
    pipeline = Pipeline([("polynomial_features", PolynomialFeatures(degree=5)), ("ridge", Ridge(alpha=1.0))])
    model = pipeline.fit(x_train, y_train)
    y_predict = pipeline.predict(x_test)
    plt.title('Гребневая полиномиальная регрессия')
    plt.plot(x_test, y_predict, color='red')
    plt.show()
    print('Гребневая полиномиальная регрессия')
    print('Оценка качества:', model.score(x_train, y_train))


x = x[:, np.newaxis, 1]

x_train, x_test, y_train, y_test = train_test_split(x, y)

linear(x_train, x_test, y_train, y_test)

polynomial(x_train, y_train)

ridge_polynomial(x_train, x_test, y_train, y_test)