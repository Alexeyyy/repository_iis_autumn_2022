from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
from sklearn.linear_model import Ridge
from sklearn.preprocessing import MinMaxScaler
import numpy as np

strings_length = 750
columns = 14
first = 10
second = 20
third = 5


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def init_data():
    np.random.seed(0)
    x = np.random.uniform(0, 1, (strings_length, columns))
    y = (first * np.sin(np.pi * x[:, 0] * x[:, 1]) + second * (x[:, 2] - .5) ** 2 +
         first * x[:, 3] + third * x[:, 4] ** third + np.random.normal(0, 1))
    x[:, 10:] = x[:, :4] + np.random.normal(0, .025, (strings_length, 4))

    # Гребневая регрессия
    rd = Ridge()
    rd.fit(x, y)

    # Рекурсивное сокращение признаков
    rfe = RFE(rd)
    rfe.fit(x, y)

    # Сокращение признаков Случайными деревьями
    rfr = RandomForestRegressor(max_depth=2, random_state=0)
    rfr.fit(x, y)

    return rd, rfe, rfr


def init_ranks():
    data = init_data()
    names = ["x%s" % i for i in range(1, 15)]

    ranks = dict()

    ranks["Гребневая регрессия"] = rank_to_dict(data[0].coef_, names)
    ranks["Рекурсивное сокращение признаков"] = rank_to_dict(-1 * data[1].ranking_ + np.max(data[1].ranking_), names)
    ranks["Сокращение признаков Случайными деревьями"] = rank_to_dict(data[2].feature_importances_, names)

    return ranks


def calculate_mean(ranks):
    mean = {}

    for key, value in ranks.items():
        for item in value.items():
            if (item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)
    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print("Среднее значение", mean)


def execute_program():
    ranks = init_ranks()
    for rank in ranks.items():
        print(rank)
    calculate_mean(ranks)
