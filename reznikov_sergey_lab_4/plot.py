import math

from matplotlib import pyplot as plt
from scipy.cluster import hierarchy
from scipy.cluster import hierarchy as clust

from constants import CLUSTER_NUMBER


def plot_dendrogram(model, method, data):
    description = 'Hierarchical Clustering Dendrogram; number of clusters: {0}, linkage method: {1}'.format(
        CLUSTER_NUMBER, method)
    cm = plt.cm.RdBu

    # Выведем все изначальные данные
    current_subplot = plt.subplot(1, 2, 1)
    clusters = clust.fcluster(Z=model, t=CLUSTER_NUMBER, criterion='maxclust')
    current_subplot.scatter(
        data[['Density (per km²)']], data[['Growth Rate']], c=clusters, cmap=cm)
    current_subplot.set_title("Representation of clusters")
    current_subplot.set_xlabel("Density (per km²)")
    current_subplot.set_ylabel('Growth Rate')

    # Выведем дендрограмму
    current_subplot = plt.subplot(1, 2, 2)
    current_subplot.set_title(description)
    current_subplot.set_ylabel('Euclidean Distance')
    hierarchy.dendrogram(model, truncate_mode='level', p=int(math.log2(CLUSTER_NUMBER)), ax=current_subplot)
    plt.show()
