import pandas
from scipy.cluster import hierarchy


def read_data():
    data = pandas.read_csv('resources/world_population.csv')
    data = data.dropna()
    # Возьмем следующую инфу для кластеризации (цена билета и номер класса)
    data = data[['Density (per km²)', 'Growth Rate']]
    data = clear_data(data)
    return data[['Density (per km²)', 'Growth Rate']]


def clear_data(data):
    columns = data.columns
    result = data
    for column in columns:
        mean = data[column].mean()
        std = data[column].std()
        result = result.drop(result[result[column] > mean + std * 2].index)
        result = result.drop(result[result[column] < mean - std * 2].index)
    return result


def get_model(data, method):
    clusters = hierarchy.linkage(data, method=method)
    return clusters
