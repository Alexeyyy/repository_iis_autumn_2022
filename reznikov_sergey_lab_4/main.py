from constants import link_methods
from model import get_model, read_data
from plot import plot_dendrogram

if __name__ == '__main__':
    data = read_data()
    for method in link_methods:
        model = get_model(data, method)
        plot_dendrogram(model, method, data)
