import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np


MAX_LENGTH_STR = 255
COUNT_SYMBOLS_ALPHABET = 30
HASH_SPACE = MAX_LENGTH_STR * COUNT_SYMBOLS_ALPHABET

data = pandas.read_csv('titanic.csv', index_col='PassengerId')


# функция для приведения пола к числу
def Sex_to_bool(sex):
    if sex == "male":
        return 0
    return 1


def name_to_number(name):
    return hash(name) % HASH_SPACE


# приведение пола к числу

data['Sex'] = data['Sex'].apply(Sex_to_bool)
data['Name'] = data['Name'].apply(name_to_number)


# выгрузка непустых данных

data=data.loc[(np.isnan(data['Pclass']) == False) & (np.isnan(data['Name']) == False) &
              (np.isnan(data['Sex']) == False) & (np.isnan(data['Survived']) == False)]

# отбор нужных столбцов
corr = data[['Pclass', 'Name', 'Sex']]
# респечатка первых 3 строк данных
print(corr)

# определение целевой переменной
y = data['Survived']
# создание и обучение дерева решений
clf = DecisionTreeClassifier(random_state=241)
clf.fit(corr, y)
# получение и распечатка важностей признаков
importances = clf.feature_importances_
print(importances)
