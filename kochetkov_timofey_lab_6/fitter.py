from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score

def random_state_fit(i, x_train, y_train, x_test, y_test, func):
    print("Num: ", i)
    mlp = MLPClassifier(random_state=i, max_iter=2000, n_iter_no_change=20,
                        activation=func, alpha=0.01, hidden_layer_sizes=[100, 100], tol=0.0000001)
    mlp.fit(x_train, y_train)
    predictions = mlp.predict(x_test)
    acc_mlp = accuracy_score(y_test, predictions)
    print("func ", func)
    print("Accuracy: ", acc_mlp)
    return acc_mlp

