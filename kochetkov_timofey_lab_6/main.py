import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from fitter import random_state_fit
import statistics
from bringingData import bring

data = pd.read_csv('StudentsPerformance.csv')

x, y = bring(data)

x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=0.2, random_state=42)

funcs = ['relu','identity','tanh','logistic']
acc = []
for i in range(0,len(funcs)):
    i = int(i)
    acc.append(random_state_fit(i, x_train, y_train, x_test, y_test, funcs[i]))

print("\n Результаты: ")
print(min(acc), statistics.median(acc), max(acc), np.std(acc))
