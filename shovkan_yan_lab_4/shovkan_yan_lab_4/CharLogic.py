import pandas
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans


def draw_clusters():
    factors = [["experience_level", ['EN', 'MI', 'SE', 'EX']], ["company_size", ['S', 'M', 'L']]]
    kmeans = KMeans(n_clusters=3)
    data = create_data()

    for i in range(len(factors)):
        corr = data[["salary_in_usd", factors[i][0]]]
        kmeans.fit(corr)
        plt.xlabel("salary_in_usd", fontsize=14, fontweight="bold")
        plt.ylabel(factors[i][0], fontsize=14, fontweight="bold")
        plt.yticks(range(len(factors[i][1])), factors[i][1])
        plt.scatter(corr.values[:, 0], corr.values[:, 1], c=kmeans.labels_)
        plt.show()


def experience_level_to_num(experience_level):
    if experience_level == "EN":
        return 0
    if experience_level == "MI":
        return 1
    if experience_level == "SE":
        return 2
    if experience_level == "EX":
        return 3


def company_size_to_num(company_size):
    if company_size == "S":
        return 0
    if company_size == "M":
        return 1
    if company_size == "L":
        return 2


def create_data():
    data = pandas.read_csv('ds_salaries.csv', on_bad_lines='skip')

    data['experience_level'] = data['experience_level'].apply(experience_level_to_num)
    data['company_size'] = data['company_size'].apply(company_size_to_num)
    data['salary_in_usd'] = data['salary_in_usd'].apply(int)

    return data[['experience_level', 'company_size', 'salary_in_usd']]
