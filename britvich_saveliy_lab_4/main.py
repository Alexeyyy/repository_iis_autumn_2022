from sklearn.cluster import KMeans
import pandas

from matplotlib import pyplot as plt

factors = ["main","max","Days"]
kmeans = KMeans(n_clusters=4)
data = pandas.read_csv("People.csv")

i=0
for i in range(len(factors)):
    corr = data[["People", factors[i]]]
    kmeans.fit(corr)


    plt.xlabel(factors[i], fontsize=12, fontweight="bold")
    plt.ylabel("People", fontsize=12, fontweight="bold")

    plt.scatter(corr.values[:, 0], corr.values[:, 1], c=kmeans.labels_)
    plt.show()










