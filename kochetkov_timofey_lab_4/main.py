from sklearn.cluster import KMeans
import pandas


data = pandas.read_csv('StudentsPerformance.csv')

corr = data[['math score', 'reading score', 'writing score']]

model = KMeans(n_clusters=3)
model.fit(corr.values)


all_predict = model.predict(corr.values)
print(all_predict)

print("Средний класс:")
labelNorm = model.predict([[72, 72, 74]])
print(labelNorm)

print("Низкий класс:")
labelLow = model.predict([[40, 50, 44]])
print(labelLow)

print("Высокий класс:")
labelHigh = model.predict([[88, 90, 85]])
print(labelHigh)