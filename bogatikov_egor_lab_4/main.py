from sklearn.cluster import KMeans
import pandas
from matplotlib import pyplot as plt


def main():
    factors = ["minTFact", "maxTFact", "longFact","Lat"]
    k_means = KMeans(n_clusters=4)
    data = pandas.read_csv("weather.csv")

    i = 0
    for i in range(len(factors)):
        corr = data[["Temp", factors[i]]]
        k_means.fit(corr)
        plt.xlabel("Temperature")
        plt.ylabel(factors[i])

        plt.scatter(corr.values[:, 1], corr.values[:, 0])
        plt.show()


if __name__ == '__main__':
    main()
