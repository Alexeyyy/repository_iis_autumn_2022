import pandas
import numpy as np
from config import *

def launch_to_bool(lunch):
    if lunch == lunch_standart:
        return 1
    return 0

def course_to_bool(course):
    if course == course_completed:
        return 1
    return 0

def bring():
    data = pandas.read_csv(file_name)

    data[lunch_name] = data[lunch_name].apply(launch_to_bool)
    data[test_prep_name] = data[test_prep_name].apply(course_to_bool)
    data = data.loc[(np.isnan(data[lunch_name]) == False) & (np.isnan(data[test_prep_name]) == False)]
    corr = data[corr_names]
    y = data[math_score_name]
    return corr, y