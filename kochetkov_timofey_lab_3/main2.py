from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from bringingData import bring

corr, y = bring()

x_train, x_test, y_train, y_test = train_test_split(
        corr, y, test_size=0.2, random_state=42)

clf = DecisionTreeClassifier(random_state=241)
clf.fit(x_train, y_train)
importances = clf.feature_importances_

print(importances)
print(clf.score(x_test, y_test))