import pandas as pd
from datetime import datetime

WINTER = 1
SPRING = 2
SUMMER = 3
AUTUMN = 4


def load(path):
    dataset = pd.read_csv(path)
    dataset['Date'] = dataset['Date'].apply(date_to_season)

    names = ['Open', 'High', 'Low', 'Close', 'Adj Close', 'Volume']

    X = dataset[names]
    y = dataset['Date']

    return X, y, names


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return WINTER
    if 3 <= date.month <= 5:
        return SPRING
    if 6 <= date.month <= 8:
        return SUMMER
    if 9 <= date.month <= 11:
        return AUTUMN
