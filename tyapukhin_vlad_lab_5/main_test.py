from sklearn.model_selection import train_test_split
from data import load
from test import calculate_mape

if __name__ == '__main__':
    X, y, names = load('PKRX.csv')

    for test_size, alpha in [[0.01, 0.1], [0.01, 0.01], [0.01, 0.001], [0.01, 0.0001], [0.05, 0.001], [0.1, 0.001]]:
        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=test_size, random_state=42)

        mape = calculate_mape(X_train, X_test, Y_train, Y_test, alpha=alpha)
        print(f'MAPE for test_size={test_size} and alpha={alpha} is {float("{:.3f}".format(mape))}')
