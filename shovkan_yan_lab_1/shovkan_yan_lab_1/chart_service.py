import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.axes import Axes
from matplotlib import pyplot as plt

count = 0
max_chart = 5
column = 5
row = 1


class ChartService:
    def create_chart(self, train, linears, ridges, polynomials, xx0, xx1):
        cm = plt.cm.RdBu
        cm_bright = ListedColormap(['#FF0000', '#0000FF'])

        current_subplot = plt.subplot(row, column, self.increment())
        current_subplot.scatter(train[0][:, 0], train[0][:, 1], c=train[2], cmap=cm_bright)

        current_subplot = plt.subplot(row, column, self.increment())
        current_subplot.scatter(train[1][:, 0], train[1][:, 1], c=train[3], cmap=cm_bright, alpha=0.6)

        current_subplot = plt.subplot(row, column, self.increment())
        self.create_gradient(linears[0], current_subplot=current_subplot, title="Линейная",
                             score=linears[1], xx0=xx0, xx1=xx1, cm=cm)

        current_subplot = plt.subplot(row, column, self.increment())
        self.create_gradient(polynomials[0], current_subplot=current_subplot, title=" Полиномиальная регрессия",
                             score=polynomials[1], xx0=xx0, xx1=xx1, cm=cm)

        current_subplot = plt.subplot(row, column, self.increment())
        self.create_gradient(ridges[0], current_subplot=current_subplot, title="Гребневая полиномиальная регрессия",
                             score=ridges[1], xx0=xx0, xx1=xx1, cm=cm)

    def create_plot(self, x, train, linears, ridges, polynomials):
        current_chart = 3
        h = .02
        x0_min, x0_max = x[:, 0].min() - .5, x[:, 0].max() + .6
        x1_min, x1_max = x[:, 1].min() - .5, x[:, 1].max() + .6
        xx0, xx1 = np.meshgrid(np.arange(x0_min, x0_max, h),
                               np.arange(x1_min, x1_max, h))

        cm_bright = ListedColormap(['#FF0000', '#0000FF'])

        self.create_chart(train, linears, ridges, polynomials, xx0, xx1)

        for i in range(current_chart, max_chart + 1):
            current_subplot = plt.subplot(row, column, i)
            current_subplot.scatter(
                train[0][:, 0], train[0][:, 1], c=train[2], cmap=cm_bright)
            current_subplot.scatter(
                train[1][:, 0], train[1][:, 1], c=train[3], cmap=cm_bright, alpha=0.6)

        plt.show()

    def create_gradient(self, model, current_subplot: Axes, title: str, score: float, xx0, xx1, cm):
        current_subplot.set_title(title)
        try:
            if hasattr(model, "decision_function"):
                Z = model.decision_function(np.c_[xx0.ravel(), xx1.ravel()])
            elif hasattr(model, "predict"):
                Z = model.predict(np.c_[xx0.ravel(), xx1.ravel()])
            elif hasattr(model, "predict"):
                Z = model.predict(np.c_[xx0.ravel(), xx1.ravel()])
        except AttributeError:
            pass

        Z = Z.reshape(xx0.shape)
        current_subplot.contourf(xx0, xx1, Z, cmap=cm, alpha=.8)
        current_subplot.set_xlim(xx0.min(), xx0.max())
        current_subplot.set_ylim(xx0.min(), xx1.max())
        current_subplot.set_xticks(())
        current_subplot.set_yticks(())
        current_subplot.text(xx0.max() - .3, xx1.min() + .3, ('%.2f' % score),
                             size=15, horizontalalignment='right')

    def increment(self):
        global count
        count += 1
        return count

