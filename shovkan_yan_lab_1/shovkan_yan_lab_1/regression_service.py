import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.datasets import make_circles
from sklearn.linear_model import LinearRegression, Ridge
from chart_service import ChartService


class RegressionService:
    def __init__(self):
        x, data = self.create_data_circles()

        linear = self.create_regression(data, LinearRegression())
        ridge = self.create_regression(data, Ridge(alpha=1))
        polynomial = self.create_regression(data, PolynomialFeatures(degree=4))

        chart = ChartService()
        chart.create_plot(x, data, linear, ridge, polynomial)

    def create_data_circles(self):
        x, y = make_circles(noise=0.2, factor=0.5)
        rng = np.random.RandomState(2)
        x += 2 * rng.uniform(size=x.shape)
        return x, train_test_split(
            x, y, test_size=.05, random_state=42)


    def create_regression(self, train_test_splits, model):
        if isinstance(model, LinearRegression):
            model.fit(train_test_splits[0], train_test_splits[2])  # train
            score = model.score(train_test_splits[1], train_test_splits[3])  # test
            return model, score

        if isinstance(model, PolynomialFeatures):
            pipeline = Pipeline([("polynomial_features", model), ("linear_regression", LinearRegression())])
            pipeline.fit(train_test_splits[0], train_test_splits[2])
            scores = cross_val_score(pipeline, train_test_splits[1], train_test_splits[3],
                                     scoring="neg_mean_squared_error", cv=5)

            return pipeline, -scores.mean()

        if isinstance(model, Ridge):
            pipeline = Pipeline([("polynomial_features", PolynomialFeatures(degree=4)), ("linear_regression", model)])
            pipeline.fit(train_test_splits[0], train_test_splits[2])
            scores = cross_val_score(pipeline, train_test_splits[1], train_test_splits[3],
                                     scoring="neg_mean_squared_error", cv=5)

            return pipeline, -scores.mean()
