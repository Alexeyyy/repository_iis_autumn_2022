from datetime import datetime

import pandas
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor


def create_data():
    data = pandas.read_csv('car_price_prediction.csv', on_bad_lines='skip')

    unique_numbers = list(set(data['Manufacturer']))
    data['Manufacturer'] = data['Manufacturer'].apply(unique_numbers.index)

    return data


def processing_data():
    data = create_data()

    corr = data[['Prod. year', 'Manufacturer']]
    print(corr)

    y = data['Price']
    clf = DecisionTreeClassifier(random_state=241)

    X_train, X_test, y_train, y_test = train_test_split(
        corr, y, test_size=0.01, random_state=42)

    clf.fit(X_train, y_train)

    print(clf.score(X_test, y_test))

    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    processing_data()
