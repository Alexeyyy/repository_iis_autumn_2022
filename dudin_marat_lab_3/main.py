import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np


def processing_data():
    data = pandas.read_csv('titanic.csv', index_col='Passengerid')

    data = data.loc[(np.isnan(data['Pclass']) == False) & (np.isnan(data['Age']) == False)
                    & (np.isnan(data['Embarked']) == False)]

    corr = data[['Pclass', 'Age', 'Embarked']]
    print(corr)

    y = data['2urvived']

    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(corr, y)

    importances = clf.feature_importances_
    print(importances)


if __name__ == '__main__':
    processing_data()

