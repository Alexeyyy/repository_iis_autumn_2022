import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

from dataUFO import create_data


def create_train_and_test(data):
    return train_test_split(
        data[['datetime', 'city']], data['duration (seconds)'], test_size=0.05, random_state=42)


def random_state_fit(x, y, x_test, y_test):
    mlr = MLPRegressor(max_iter=2000, n_iter_no_change=20,
                       activation='relu', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)
    mlr.fit(x, y)
    y_pred = mlr.predict(x_test)
    accuracy = accuracy_score(y_test, np.round(y_pred))

    return accuracy


def evaluate_MLP():
    data = create_data()
    X_train, X_test, y_train, y_test = create_train_and_test(data)

    accuracy = random_state_fit(X_train, y_train, X_test, y_test)

    print("Точность ", accuracy)