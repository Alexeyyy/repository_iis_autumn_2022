# Лабораторная работа №6
## Тема: Нейронная сеть

**Как запустить лабораторную работу**

* Требуется установить и использовать технологии: python, conda, numpy, sklearn
* С помощью команды или компилятора запустить main.py/main2.py

**Что делает программа 1**

* Считывает данные из файла: 10000 строк информации о контактах с НЛО. Выделяет информацию о времени года, городе и времени контакта.
* Обучает многослойный перцептрон MLPRegressor определять время года на основе времени контакта и местоположения контакта
* Тестирует MLPRegressor и определяет точность

### Из-за нулевой точности, я решил обучить перцептрон на основе данных о показателях организма человека

**Что делает программа 2**

* Считывает данные из файла: 300 строк информации о показателях организма человека. Выделяет информацию о холестерине, давлении и сахаре.
* Обучает многослойный перцептрон MLPRegressor определять повышен ли сахар у человека по давлению и холестерину
* Тестирует MLPRegressor и определяет точность

### Тестирование
[Видео](https://drive.google.com/file/d/14uNNbm3c2GiBpXi5lxzU7sq6sbYEg5yc/view?usp=sharing)
![alt text](Result.png "Result")
![alt text](Result2.png "Result2")

* Точность определения времени года при контакте с НЛО: 0. Показатель очень плохой, проблема в том, что в каждый сезон время контакта сильно не отличается, так же нет никакой связи между временем года и тем в какой город прилетают НЛО

* Точность определения повышен ли сахар: 0.8125. Что является очень хорошим показателем.
