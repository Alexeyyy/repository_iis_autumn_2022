from datetime import datetime

import pandas

from constants import NUMBER_OF_LINES


def date_to_season_to_num(date_str):
    date = datetime.strptime(date_str, "%m/%d/%Y %H:%M")
    if date.month == 12 or date.month <= 2:
        return 1
    if 3 <= date.month <= 5:
        return 2
    if 6 <= date.month <= 8:
        return 3
    if 9 <= date.month <= 11:
        return 4


def create_data():
    data = pandas.read_csv('scrubbed.csv', on_bad_lines='skip')
    data = data.sample(frac=1)
    data = data[:NUMBER_OF_LINES]

    data['datetime'] = data['datetime'].apply(date_to_season_to_num)
    data['duration (seconds)'] = data['duration (seconds)'].apply(int)

    unique_numbers = list(set(data['city']))
    data['city'] = data['city'].apply(unique_numbers.index)

    return data
