from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
from constants import *
import numpy as np


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_data():
    np.random.seed(0)
    X = np.random.uniform(0, 1, (OBSERVATION_STRINGS, COLUMNS_SIGNS))
    # Задаем функцию-выход: регрессионную проблему Фридмана
    Y = (FRIEDMAN_NUM * np.sin(np.pi * X[:, 0] * X[:, 1]) + FRIEDMAN_NUM2 * (X[:, 2] - .5) ** 2 +
         FRIEDMAN_NUM * X[:, 3] + FRIEDMAN_NUM3 * X[:, 4] ** FRIEDMAN_NUM3 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (OBSERVATION_STRINGS, 4))

    # линейная модель
    lr = LinearRegression()
    lr.fit(X, Y)
    # рекурсивное сокращение признаков
    rfe = RFE(lr)
    rfe.fit(X, Y)
    # сокращение признаков случайными деревьями
    rfr = RandomForestRegressor(max_depth=2, random_state=0)
    rfr.fit(X, Y)

    return lr, rfe, rfr


def create_ranks():
    data = create_data()
    names = ["x%s" % i for i in range(1, 15)]

    ranks = dict()

    ranks["Linear Regression"] = rank_to_dict(data[0].coef_, names)
    ranks["Recursive Feature Elimination"] = rank_to_dict(-1 * data[1].ranking_ + np.max(data[1].ranking_), names)
    ranks["Random Forest Regressor"] = rank_to_dict(data[2].feature_importances_, names)

    return ranks


def calculate_mean(ranks):
    # Создаем пустой список для данных
    mean = {}
    # «Бежим» по списку ranks
    # As you are in python3 , use dict.items() instead of dict.iteritems(), iteritems() was removed in python3
    for key, value in ranks.items():
        # «Пробегаемся» по списку значений ranks, которые являются парой имя:оценка
        for item in value.items():
            # имя будет ключом для нашего mean
            # если элемента с текущим ключем в mean нет - добавляем
            if (item[0] not in mean):
                mean[item[0]] = 0
                # суммируем значения по каждому ключу-имени признака
                mean[item[0]] += item[1]

    # находим среднее по каждому признаку
    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)
    # сортируем и распечатываем список
    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print("MEAN", mean)


def get_ranks_and_mean():
    ranks = create_ranks()
    print(ranks)
    calculate_mean(ranks)
