import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np

PATH_TITANIC = 'titanic.csv'
INDEX_COL = 'PassengerId'
MAX_LENGTH_STR = 255
COUNT_SYMBOLS_ALPHABET = 30
HASH_SPACE = MAX_LENGTH_STR * COUNT_SYMBOLS_ALPHABET


def load_data(path, index_col):
    return pandas.read_csv(path, index_col=index_col)


def sex_to_bool(sex):
    if sex == "male":
        return 1
    elif sex == "female":
        return 0
    return np.NaN


def name_to_number(name):
    return hash(name) % HASH_SPACE


data = load_data(PATH_TITANIC, INDEX_COL)
table = data[['Name', 'Sex', 'Age']]
print(table)

data['Sex'] = data['Sex'].apply(sex_to_bool)
data['Name'] = data['Name'].apply(name_to_number)

data = data.loc[(np.isnan(data['Name']) == False) & (np.isnan(data['Sex']) == False)
                & (np.isnan(data['Age']) == False)]

table = data[['Name', 'Sex', 'Age']]
print(table)

x = data[['Name', 'Sex', 'Age']]
y = data['Survived']
model = DecisionTreeClassifier()
model.fit(x, y)

z = model.feature_importances_
print(z)
