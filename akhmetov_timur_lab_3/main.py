import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split


data = pd.read_csv("1.csv", index_col="Passengerid")
clf = DecisionTreeClassifier(random_state=241)

# Выбор столбцов
Y = data["2urvived"]
X = data[["Sex", "Age", "sibsp", ]]
print(X)

X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.05, random_state=42)
clf.fit(X_train, y_train)

# Качество модели
print(clf.score(X_test, y_test))

# Важность
print(clf.feature_importances_)


