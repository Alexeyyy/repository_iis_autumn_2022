from datetime import datetime

import pandas
from sklearn import preprocessing
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

WINTER = 0
SPRING = 1
SUMMER = 2
AUTUMN = 3


# The problem of currencies
def solve_task(path):
    dataset = pandas.read_csv(path)

    dataset['Date'] = dataset['Date'].apply(date_to_season)
    dataset['Transaction'] = dataset['Transaction'].apply(transaction_to_number)
    dataset['Cost'] = dataset['Cost'].apply(int)


    def calculate_score():
        X = dataset[['Date', 'Transaction']]
        print(X)
        y = dataset['Cost']

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.01, random_state=42)

        lab = preprocessing.LabelEncoder()

        clf = DecisionTreeClassifier(random_state=241)
        clf.fit(X_train, y_train)

        print('\n', X.head())

        print('\nImportance of signs:', clf.feature_importances_)


        return clf.score(X_test, y_test)

    print("\nScore:", calculate_score())


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return WINTER
    if 3 <= date.month <= 5:
        return SPRING
    if 6 <= date.month <= 8:
        return SUMMER
    if 9 <= date.month <= 11:
        return AUTUMN


def transaction_to_number(transaction_str):
    if transaction_str == 'Option Exercise':
        return 1
    if transaction_str == 'Sale':
        return 2

if __name__ == '__main__':
    solve_task('TSLA.csv')