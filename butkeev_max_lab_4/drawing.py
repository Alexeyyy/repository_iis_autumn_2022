import numpy as np
from colorcet import palette
from matplotlib import pyplot as plt
from sklearn.cluster import AgglomerativeClustering
from sklearn.manifold import TSNE

from constants import *
from ufa import create_data


def draw_clusters():
    data = create_data()
    tsne = TSNE(n_components=3, learning_rate='auto',  init='random', perplexity=3)
    time = ['малая','среднее','большая']

    plt.xlabel("длительность", fontsize=FONT_SIZE, fontweight="bold")
    plt.ylabel("жанр", fontsize=FONT_SIZE, fontweight="bold")

    corr_butkeev = np.array(data)
    tsne_result = tsne.fit_transform(corr_butkeev)
   # tsne.fit_predict(corr_butkeev)

    plt.scatter(tsne_result[:, 0], tsne_result[:, 1], c=data['Runtime'].to_list(), cmap='rainbow')
    plt.show()
