from datetime import datetime

import pandas


from constants import *

def duration(time):
    if time > 120:
        time = 3
    elif time > 60 or time < 120:
        time = 2
    elif time < 60:
        time = 1
    return time


def create_data():
    data = pandas.read_csv('Netflix_Originals.csv', on_bad_lines='skip')

    unique_numbers = list(set(data['Genre']))
    data['Genre'] = data['Genre'].apply(unique_numbers.index)
    data['Runtime'].apply(duration)

    return data[['Runtime', 'Genre']]
