import pandas as pd

def load():
    dataset = pd.read_csv('healthcare-dataset-stroke-data.csv')
    dataset['smoking_status'] = dataset['smoking_status'].apply(smoking_status_to_int)

    names = ['hypertension', 'heart_disease', 'avg_glucose_level', 'stroke']

    X = dataset[names]
    y = dataset['smoking_status']

    return X, y, names


def smoking_status_to_int(status):
    if status == "smoked":
        return 0
    if status == "smokes":
        return 1
    if status == "formerly smoked":
        return 2
    if status == "Unknown":
        return 4
    if status == "never smoked":
        return 5