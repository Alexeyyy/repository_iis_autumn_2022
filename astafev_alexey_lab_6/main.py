from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import make_column_transformer
from data import load
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
import warnings


def train(X_train, X_test, y_train, y_test):
    mlp = MLPClassifier(random_state=321, solver="sgd", activation="tanh", alpha=0.01, hidden_layer_sizes=(2, ), max_iter=2000, tol=0.00000001)

    pipe = make_pipeline(column_trans, mlp)
    pipe.fit(X_train, y_train)
    predictions = pipe.predict(X_test)
    score = accuracy_score(y_test, predictions)
    print(score)


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    X, y, names = load()

    column_trans = make_column_transformer(
        (OneHotEncoder(handle_unknown='ignore'), names),
        remainder='passthrough'
    )

    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.05, random_state=42)

    train(X_train, X_test, Y_train, Y_test)


