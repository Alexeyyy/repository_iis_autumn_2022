# Лабораторная работа 6. Нейронная сеть
## Астафьев Алексей ПИбд-41. Вариант 3

Класс MLPClassifier реализует алгоритм многослойного перцептрона (MLP), который обучается с использованием обратного распространения.

### Как запустить лабораторную работу

1. Установить python, pandas, sklearn
2. Запустить команду `python main.py`

### Использованные технологии

python, pandas, sklearn

### Что делает программа?

Программа на оcнове данных из файла healthcare-dataset-stroke-data.csv обучает модель нейроной сети MLPClassifier.

### Тестирование
[Видео](https://drive.google.com/file/d/1wEryAuT1tDNUOzLENozd4yJ1a0T69uvW/view?usp=sharing)