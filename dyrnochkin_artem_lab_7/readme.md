###### ПИбд-41 Дырночкин Артём

# Лабораторная работа №7

## Рекуррентная нейронная сеть и задача генерации текста

### Как запустить лабораторную работу

1. Перейти на сайт <https://colab.research.google.com>
1. Запустить файл Lab7.ipynb

### Использованные технологии

python, numpy, time, keras

### Что делает программа?

Программа должна сгенирировать текст на основе рекурентной нейронной сети.
Данные: файл с английским текстом.
Для 7 лабораторной работы было принято решение использовать google colab, так как мой старенький компьютер не мог справиться с обучение модели и поэтому google colab стал хорошим способом решения данной лабораторной работы.


### Тестирование

Epoch 1/5
1806/1806 [==============================] - 32s 14ms/step - loss: 2.7115
Epoch 2/5
1806/1806 [==============================] - 26s 14ms/step - loss: 2.4628
Epoch 3/5
1806/1806 [==============================] - 26s 14ms/step - loss: 2.4068
Epoch 4/5
1806/1806 [==============================] - 26s 14ms/step - loss: 2.3738
Epoch 5/5
1806/1806 [==============================] - 26s 15ms/step - loss: 2.3487
Выполнялось:2.32 минут
Seed: 
" on her shoulder... blaze
     of sudden brightness... it's too curious...
     olga's appeared upon  "
==============================
the soaete
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
     the soae to the to the sore
Done

5 эпох прошли за 2 минуты 32 секунды.

