from sklearn.linear_model import LogisticRegression


def create_model(x, y):
    logistic = LogisticRegression(random_state=0)
    logistic.fit(x, y)

    return logistic
