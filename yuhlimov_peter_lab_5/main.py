from sklearn.metrics import mean_absolute_percentage_error
from sklearn.model_selection import train_test_split

from models import create_model
from ranks import calculate_mean_and_sort_list, get_ranks
from data import load

if __name__ == '__main__':
    X, y, names = load('Economic Data - 9 Countries (1980-2020).csv')

    print("MEAN", calculate_mean_and_sort_list(get_ranks(create_model(X, y), names)))

    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.01, random_state=42)

    mape = mean_absolute_percentage_error(list(Y_test), list(create_model(X_train, Y_train).predict(X_test)))

    print(f'MAPE value is {float("{:.3f}".format(mape))}')
