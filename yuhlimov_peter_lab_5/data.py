import pandas as pd


def load(path):
    dataset = pd.read_csv(path)

    unique_county = list(set(dataset['country']))

    dataset['country'] = dataset['country'].apply(unique_county.index)
    dataset['oil prices'] = dataset['oil prices'].apply(round)

    names = ['year', 'country']

    X = dataset[names]
    y = dataset['oil prices']

    return X, y, names
