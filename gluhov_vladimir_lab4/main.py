import math

import pandas
from matplotlib import pyplot as plt
from scipy.cluster import hierarchy as clust
from scipy.cluster import hierarchy

CLUSTER_NUMBER = 10
linkage_methods = ['single', 'complete', 'weighted', 'centroid']


def read_data():
    data = pandas.read_csv('Mobile dataset.csv')
    data = data.dropna()
    data = data[['sales_price', 'display_size']]
    data = clear_data(data)
    return data[['sales_price', 'display_size']]


def clear_data(data):
    columns = data.columns
    result = data
    for column in columns:
        mean = data[column].mean()
        std = data[column].std()
        result = result.drop(result[result[column] > mean + std * 2].index)
        result = result.drop(result[result[column] < mean - std * 2].index)
    return result


def get_model(data, method):
    clusters = hierarchy.linkage(data, method=method)
    return clusters

def plot_drawing(model, method, data):
    description = 'linkage method - {0}, clusters - {1}'.format(method, CLUSTER_NUMBER)
    cm = plt.cm.RdBu
    current_subplot = plt.subplot(1, 1, 1)
    clusters = clust.fcluster(Z=model, t=CLUSTER_NUMBER, criterion='maxclust')
    current_subplot.scatter(data[['sales_price']], data[['display_size']], c=clusters, cmap=cm)
    current_subplot.set_title(description)
    current_subplot.set_xlabel("sales_price")
    current_subplot.set_ylabel('display_size')
    plt.show()


if __name__ == '__main__':
    data = read_data()
    for method in linkage_methods:
        model = get_model(data, method)
        plot_drawing(model, method, data)
