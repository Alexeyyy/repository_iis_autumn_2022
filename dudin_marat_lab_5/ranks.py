import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler


def rank_to_dict(ranks, names):

    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_ranks(data):

    clf = LogisticRegression(random_state=0)
    clf.fit(data[['Manufacturer','Prod. year']], data['Price'])

    ranks = dict()
    ranks["Logistic Regression"] = rank_to_dict(clf.coef_[0], ['Prod. year','Manufacturer'])
    print(ranks)
    return ranks
