from datetime import datetime

import pandas

from constants import *


def edit_price(price):
    if price < MIN_PRICE:
        price = MIN_VALUE
    elif MIN_PRICE <= price <= MAX_PRICE:
        price = MID_VALUE
    elif price > MAX_PRICE:
        price = MAX_VALUE
    return price


def create_data():
    data = pandas.read_csv('car_price_prediction.csv', on_bad_lines='skip')
    data = data.sample(frac=1)
    data = data[:NUMBER_OF_LINES]

    unique_numbers = list(set(data['Manufacturer']))
    data['Manufacturer'] = data['Manufacturer'].apply(unique_numbers.index)
    data['Price'] = data['Price'].apply(edit_price)

    return data[['Prod. year', 'Price', 'Manufacturer']]
