from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import numpy as np

from constants import ONE_HUNDRED_PERCENT
from data import create_data
from ranks import create_ranks


def calculate_mean(ranks):
    mean = {}

    for key, value in ranks.items():
        for item in value.items():
            if (item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print("MEAN", mean)


def train_and_test_logistic(data):

    X_train, X_test, y_train, y_test = train_test_split(
        data[['Prod. year', 'Manufacturer']], data['Price'], test_size=0.05, random_state=42)

    clf = LogisticRegression(random_state=0)
    clf.fit(X_train, y_train)

    logistic_predict = clf.predict(X_test)
    logistic_MAPE = error_calculation(y_test, logistic_predict)
    print("MAPE value: ", logistic_MAPE)


def error_calculation(Y_actual, Y_Predicted):
    mape = np.mean(np.abs((Y_actual - Y_Predicted)/(Y_actual+1)))*ONE_HUNDRED_PERCENT
    return ONE_HUNDRED_PERCENT - mape


def get_ranks_and_mean():
    data = create_data()

    ranks = create_ranks(data)
    calculate_mean(ranks)
    train_and_test_logistic(data)
