import pandas as pd
from sklearn.compose import make_column_transformer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder


def moving_time_to_word_category(time):
    min_time = data['moving_time'].min()
    max_time = data['moving_time'].max()
    mean_time = (max_time - min_time) / 2 + min_time
    if time < mean_time:
        return 'small'
    return 'long'


data = pd.read_csv('data.csv')
# clean data
ind_missing = data[data['moving_time'] < 500.0].index
data = data.drop(ind_missing, axis=0)
ind_missing1 = data[data['max_speed'] == 0.0].index
data = data.drop(ind_missing1, axis=0)
ind_missing2 = data[data['length_2d'] < 1.0].index
data = data.drop(ind_missing2, axis=0)
ind_missing3 = data[data['downhill'] < 1.0].index
data = data.drop(ind_missing3, axis=0)
ind_missing4 = data[data['uphill'] < 1.0].index
data = data.drop(ind_missing4, axis=0)
# time to word
data['moving_time'] = data['moving_time'].apply(moving_time_to_word_category)
df = data.loc[:, ['max_speed', 'downhill', 'length_2d', 'uphill', 'moving_time']]
X = df.drop('moving_time', axis='columns')
y = df.moving_time
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.1, random_state=42)
column_trans = make_column_transformer(
    (OneHotEncoder(handle_unknown='ignore'), ['max_speed', 'length_2d']),
    remainder='passthrough'
)
mlp = MLPClassifier(random_state=321,
                    solver="adam",
                    activation="logistic",
                    alpha=1,
                    hidden_layer_sizes=(2,),
                    max_iter=2000,
                    tol=0.01)
pipe = make_pipeline(column_trans, mlp)
pipe.fit(X_train, y_train)
# predict
predictions = pipe.predict(X_test)
score = accuracy_score(y_test, predictions)
print("Оценка качества - ", score)
