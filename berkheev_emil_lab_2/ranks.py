import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter
from models import create_models
import operator

class RankCalculator:
    def __init__(self):
        linear, rfe, rfr = create_models()
        ranks = self.get_ranks(linear, rfe, rfr)

        print("MEAN","\n" , self.calculate_mean_and_sort_list(ranks))
        
    def get_ranks(self, linear, rfe, rfr):
        ranks = dict()
        names = ["x%s" % i for i in range(1, 15)]

        ranks['Linear regression'] = self.rank_to_dict(linear.coef_, names)
        ranks['RFE'] = self.rank_to_dict(rfe.ranking_, names)
        ranks['RFR'] = self.rank_to_dict(rfr.feature_importances_, names)

        return ranks

    def roundDict(self, dictA, ranks):
        for key, value in dictA.items():
            res = value / len(ranks)
            dictA[key] = round(res,4)

    def calculate_mean_and_sort_list(self, ranks):
        mean = {}
        
        for key, value in ranks.items():
            print(key,"\n", value)
            print("\n")
            for item in value.items():
                if item[0] not in mean:
                    mean[item[0]] = 0
                    mean[item[0]] += item[1]
        self.roundDict(mean, ranks)

        return dict(sorted(mean.items(), key = operator.itemgetter(1), reverse = True))


    def rank_to_dict(self, ranks, names):
        ranks = np.abs(ranks)
        
        #Transform features by scaling each feature to a given range
        minmax = MinMaxScaler()

        #Split by features amount
        ranks = minmax.fit_transform(np.array(ranks).reshape(len(ranks),1)).ravel()
        targetDict = dict(zip(names, ranks))
        self.roundDict(targetDict, ranks)
        return dict(sorted(targetDict.items(), key = operator.itemgetter(1), reverse = True))
