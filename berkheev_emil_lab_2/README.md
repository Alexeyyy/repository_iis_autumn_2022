# Лабораторная работа №2
## Выполнил студент ПИбд-41 Берхеев Эмиль
## Тема: Ранжирование признаков

**Как запустить лабораторную работу**

* Необходимо установить: python, sklearn, numpy
* Запустить проект (стартовая точка класс main)

**Что делает программа**

* Выполняет ранжирование признаков для регрессионной модели

**Что делает программа**

*Входные данные*
*14 признаков
*750 наблюдений

*Модели*
*Ridge
*RFE
*RFR

*Выводы*
Линейная регрессия выявила признаки x4,x1,x2,x14, которые являются действительно важными
RFE выявила x9, x8, x13, x7, из данных признаков выделить действительно важные не представляется возможным
RFR выявил x14, x2, x4, x1, 3 из которых являются действительно важными, а x14, зависящий от них, его влияние может быть уточнено через них

По результатам линейная регрессия является наиболее предпочтительной


### Результаты тестирования

![alt text](images/1.png "Params")

![alt text](images/2.png "Results")

