import numpy as np
from sklearn.linear_model import Ridge
from sklearn.feature_selection import RFE
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor


def generate_dataset():
    np.random.seed(0)
    size = 750

    x = np.random.uniform(0, 1, (size, 14))

    y = (10 * np.sin(np.pi * x[:, 0] * x[:, 1]) + 20 * (x[:, 2] - .5) ** 2 +
         10 * x[:, 3] + 5 * x[:, 4] ** 5 + np.random.normal(0, 1))

    x[:, 10:] = x[:, :4] + np.random.normal(0, .025, (size, 4))

    return x, y

def create_models():
    x,y = generate_dataset()
    ridge = Ridge()
    ridge.fit(x, y)
    
    estimator = SVR(kernel="linear")
    rfe = RFE(estimator)
    rfe.fit(x, y)
    
    rfr = RandomForestRegressor(bootstrap=True)
    rfr.fit(x, y)


    return ridge, rfe, rfr
