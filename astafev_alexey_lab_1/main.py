import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.datasets import make_moons
from sklearn import metrics

cm_bright = ListedColormap(['#8B0000', '#FF0000'])
cm_bright1 = ListedColormap(['#FF4500', '#FFA500'])

def create_moons():
    x, y = make_moons(noise=0.3, random_state=0)
    rng = np.random.RandomState(2)
    x += 2 * rng.uniform(size=x.shape)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=.4, random_state=42)

    linear_regretion(X_train, X_test, y_train, y_test)
    polynomial_regretion(X_train, X_test, y_train, y_test)
    ridge_regretion(X_train, X_test, y_train, y_test)


def linear_regretion(x_train, x_test, y_train, y_test):
    model = LinearRegression().fit(x_train, y_train)
    y_predict = model.intercept_ + model.coef_ * x_test
    plt.title('Линейная регрессия')
    print('Линейная регрессия')
    plt.scatter(x_train[:, 0], x_train[:, 1], c=y_train, cmap=cm_bright)
    plt.scatter(x_test[:, 0], x_test[:, 1], c=y_test, cmap=cm_bright1, alpha=0.7)
    plt.plot(x_test, y_predict, color='red')
    print('MAE', metrics.mean_absolute_error(y_test, y_predict[:, 1]))
    print('MSE', metrics.mean_squared_error(y_test, y_predict[:, 1]))
    plt.show()


def polynomial_regretion(x_train, x_test, y_train, y_test):
    polynomial_features = PolynomialFeatures(degree=4)
    X_polynomial = polynomial_features.fit_transform(x_train, y_train)
    base_model = LinearRegression()
    base_model.fit(X_polynomial, y_train)
    y_predict = base_model.predict(X_polynomial)
    plt.title('Полиномиальная регрессия')
    plt.scatter(x_train[:, 0], x_train[:, 1], c=y_train, cmap=cm_bright)
    plt.scatter(x_test[:, 0], x_test[:, 1], c=y_test, cmap=cm_bright1, alpha=0.7)
    plt.plot(x_train, y_predict, color='blue')
    plt.show()
    print('Полиномиальная регрессия')
    print('MAE', metrics.mean_absolute_error(y_train, y_predict))
    print('MSE', metrics.mean_squared_error(y_train, y_predict))


def ridge_regretion(X_train, X_test, y_train, y_test):
    model = Pipeline([('poly', PolynomialFeatures(degree=4)), ('ridge', Ridge(alpha=1.0))])
    model.fit(X_train, y_train)
    y_predict = model.predict(X_test)
    plt.title('Гребневая полиномиальная регрессия')
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright)
    plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright1, alpha=0.7)
    plt.plot(X_test, y_predict, color='blue')
    plt.show()
    print('Гребневая полиномиальная регрессия')
    print('MAE', metrics.mean_absolute_error(y_test, y_predict))
    print('MSE', metrics.mean_squared_error(y_test, y_predict))


create_moons()