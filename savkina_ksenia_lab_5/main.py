from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures

from converting_data import get_data
from ranking_data import get_ranking


def polynomial(x_train, y_train, x_test, y_test, degree):
    poly = PolynomialFeatures(degree=degree)
    x_poly = poly.fit_transform(x_train)
    poly.fit(x_poly, y_train)
    pol_reg = LinearRegression()
    pol_reg.fit(x_poly, y_train)
    y_predict = pol_reg.predict(poly.fit_transform(x_test))
    print('degree = ' + str(degree))
    print('Ошибка:', mean_absolute_percentage_error(y_test, y_predict))


def main():
    data = get_data()

    names = ['Your name ', 'Your gender ', 'Your age ', 'How do you describe your current level of fitness ?',
             'How often do you exercise?', 'Do you exercise ___________ ?',
             'What time if the day do you prefer to exercise?', 'How long do you spend exercising per day ?',
             'Would you say you eat a healthy balanced diet ?', 'How healthy do you consider yourself?',
             'Have you ever purchased a fitness equipment?']

    x = data[names]

    y = data['How important is exercise to you ?']

    ranking_data = get_ranking(x, y, names)

    print("MEAN")
    print(ranking_data)

    highlighted_names = ['How long do you spend exercising per day ?', 'How often do you exercise?', 'Your name ',
                         'Have you ever purchased a fitness equipment?', 'Your gender ']

    x = data[highlighted_names]
    x_train, x_test, y_train, y_test = train_test_split(x, y)

    for value in range(1, 6):
        polynomial(x_train, y_train, x_test, y_test, value)


if __name__ == '__main__':
    main()
