from sklearn.manifold import TSNE
from data import fit, load
from plots import show_plots, create_plot_info


if __name__ == '__main__':
    labels = {
        "x": "Season",
        "y": "Value",
    }

    X, Y = load('PKRX.csv')

    model = TSNE(
        learning_rate=800,
        n_iter=1000,
        init='pca',
        perplexity=30,
        angle=0.5,
    )
    x_axis, y_axis = fit(model, X)

    show_plots(x=x_axis, y=y_axis, colors=Y.to_list(), plot_name=create_plot_info(model), labels=labels)
