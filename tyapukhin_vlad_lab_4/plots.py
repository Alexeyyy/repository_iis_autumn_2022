from sklearn.manifold import TSNE
import plotly.express as px


def show_plots(x, y, colors, plot_name, labels):
    fig = px.scatter(None, x=x, y=y, labels=labels, opacity=1, color=colors)
    fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')
    fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='lightgrey',
                     zeroline=True, zerolinewidth=1, zerolinecolor='lightgrey',
                     showline=True, linewidth=1, linecolor='black')

    fig.update_layout(title_text=plot_name, plot_bgcolor='white')
    fig.update_traces(marker=dict(size=4))

    fig.write_html(f'images/figure.html')


def create_plot_info(m: TSNE):
    return f'learning_rate={m.learning_rate}; ' +\
           f'perplexity={m.perplexity}; ' + \
           f'n_iter={m.n_iter}; ' + \
           f'init={m.init}; ' + \
           f'angle={m.angle}'
