import pandas as pd
from datetime import datetime


def load(path):
    dataset = pd.read_csv(path)
    dataset['Date'] = dataset['Date'].apply(date_to_season)

    X = dataset[['Open', 'High', 'Low', 'Close']]
    y = dataset['Date']

    return X, y


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return 'Winter'
    if 3 <= date.month <= 5:
        return 'Spring'
    if 6 <= date.month <= 8:
        return 'Summer'
    if 9 <= date.month <= 11:
        return 'Autumn'


def fit(model, x):
    transformed = model.fit_transform(x)

    return transformed[:, 0], transformed[:, 1]
