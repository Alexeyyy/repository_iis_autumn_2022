from sklearn.neural_network import MLPRegressor
from sklearn.metrics import accuracy_score
import numpy as np

from scores import MAPE


def random_state_fit(i, x, y, x_test, y_test):
    mlr = MLPRegressor(random_state=i, max_iter=2000, n_iter_no_change=20,
                       activation='relu', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)
    mlr.fit(x, y)
    y_pred = mlr.predict(x_test)
    acc = 100 - MAPE(y_test, y_pred)
    print('random state', i, "\naccuracy", acc)

    return acc
