from dataset import load_dataset
from fit import random_state_fit
from statistic import statistic
from plot import save_plot

for filename in ['30', '3']:
    x_train, x_test, y_train, y_test = load_dataset(
        filename=f'genres_features_{filename}_sec.csv')

    history = []

    for i in range(2):
        acc = random_state_fit(i, x_train, y_train, x_test, y_test)
        history.append(acc)

    statistic(history)
