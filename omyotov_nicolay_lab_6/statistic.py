import numpy as np


def statistic(data):
    data_min = min(data)
    print('min:', data_min)

    data_median = median(data)
    print('median:', data_median)

    data_max = max(data)
    print('max:', data_max)

    data_std = np.std(data)
    print('std:', data_std)

    return data_min, data_median, data_max, data_std


def median(list):
    return np.median(np.array(list))
