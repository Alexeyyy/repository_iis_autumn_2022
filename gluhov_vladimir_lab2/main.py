from ranks import calc_mean, get_ranks
from sklearn.linear_model import Lasso
from sklearn.feature_selection import RFE, f_regression
import numpy as np
import conf

def generate_dataset():
    np.random.seed(0)
    size = 750
    x = np.random.uniform(0, 1, (size, conf.FEATURES_AMOUNT))

    # Задаем функцию-выход: регрессионную проблему Фридмана
    y = (10 * np.sin(np.pi*x[:, 0]*x[:, 1]) + 20*(x[:, 2] - .5)**2 +
         10*x[:, 3] + 5*x[:, 4]**5 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    x[:, 10:] = x[:, :4] + np.random.normal(0, .025, (size, 4))
    return x, y


def fit_models(x, y):
    lasso = Lasso(alpha=0.001)
    lasso.fit(x, y)

    rfe = RFE(lasso, step=2)
    rfe.fit(x, y)

    f, pval = f_regression(x, y, center=False)

    return lasso, rfe, f


x, y = generate_dataset()

lasso, rfe, f = fit_models(x, y)

ranks = get_ranks(lasso, rfe, f)

mean = calc_mean(ranks)

print("MEAN", mean)