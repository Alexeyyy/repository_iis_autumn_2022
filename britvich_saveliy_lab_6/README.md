# Лабораторная работа №6

## Выполнил студент ПИбд-41 Бритвич Савелий

### Нейронная сеть

#### Как запустить лабораторную работу

1. Установить python, pandas, sklearn
2. Запустить команду `python main.py`

##### Использованные технологии

python, pandas, sklearn

###### Что делает программа?

Программа на оcнове данных из файла weather.csv обучает модель нейроной сети MLPClassifier.

####### Тестирование

[Видео] https://youtu.be/yg2yq6zHzn8