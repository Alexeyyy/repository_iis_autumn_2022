import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from funcClassifier import random_state_fit
import statistics

names=["longFact","latFact","glevelFact", "ColdDays"]

def bring(data):
    return data[names], data['Temp']

def worker():
    data = pd.read_csv('weather1.csv')

    x, y = bring(data)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.2, random_state=52)

    funcs = ['relu','identity','tanh','logistic']
    acc = []
    for i in range(0,len(funcs)):
        i = int(i)
        acc.append(random_state_fit(i, x_train, y_train, x_test, y_test, funcs[i]))

    print("\n Results: ")
    print(f' Min is {min(acc)}, Median is {statistics.median(acc)}, Max is {max(acc)}')
