import pandas as pd
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder, PolynomialFeatures

data = pd.read_csv('data.csv', sep=',')
features = ['moving_time', 'max_speed', 'difficulty', 'min_elevation', 'downhill', 'length_2d']

# prepare data for regression
data2 = pd.DataFrame()
le = LabelEncoder()
data2['length_2d'] = le.fit_transform(data['length_2d'])
data2['downhill'] = data['downhill']
data2['uphill'] = data['uphill']
scaler = preprocessing.MinMaxScaler()
data2 = pd.DataFrame(data=scaler.fit_transform(data2), columns=data2.columns)
data2['time'] = data['moving_time']

х = data2.drop('time', axis=1)
y = data2['time']

print(х)
print(y)

x_train, x_test, y_train, y_test = train_test_split(х, y, test_size=0.1)
lin = LinearRegression()
polynomial_features = PolynomialFeatures(degree=9)
pipeline = Pipeline([("Linear", polynomial_features), ("linear_regression", lin)])
pipeline.fit(x_train, y_train)
y_predict = lin.predict(polynomial_features.fit_transform(x_test))
print('Предсказание: ', y_predict)
print('Оценка качества:', pipeline.score(x_test, y_test))
print('Ошибка:', mean_absolute_percentage_error(y_test, y_predict))
