from models import create_model
from ranks import calculate_mean_and_sort_list, get_ranks
from data import load


if __name__ == '__main__':
    X, y, names = load('TSLA.csv')

    lasso = create_model(X, y, alpha=0.001)

    ranks = get_ranks(lasso, names)

    print("MEAN", calculate_mean_and_sort_list(ranks))
