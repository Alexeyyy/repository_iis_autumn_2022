from sklearn.linear_model import Lasso


def create_model(x, y, alpha):
    lasso = Lasso(alpha=alpha)
    lasso.fit(x, y)

    return lasso
