import pandas as pd
from datetime import datetime

WINTER = 1
SPRING = 2
SUMMER = 3
AUTUMN = 4


def load(path):
    dataset = pd.read_csv(path)
    dataset['Date'] = dataset['Date'].apply(date_to_season)
    dataset['Transaction'] = dataset['Transaction'].apply(transaction_to_number)
    dataset['Cost'] = dataset['Cost'].apply(int)
    names = ['Date', 'Transaction']
    X = dataset[names]
    y = dataset['Cost']

    return X, y, names


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return WINTER
    if 3 <= date.month <= 5:
        return SPRING
    if 6 <= date.month <= 8:
        return SUMMER
    if 9 <= date.month <= 11:
        return AUTUMN


def transaction_to_number(transaction_str):
    if transaction_str == 'Option Exercise':
        return 1
    if transaction_str == 'Sale':
        return 2
