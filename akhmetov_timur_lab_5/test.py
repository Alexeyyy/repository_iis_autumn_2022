from models import create_model
from sklearn.metrics import mean_absolute_percentage_error


def calculate_mape(x_train, x_test, y_train, y_test, alpha):
    lasso = create_model(x_train, y_train, alpha=alpha)
    lasso_predict = lasso.predict(x_test)

    # Convert to lists to calculate MAPE
    y_test = list(y_test)
    lasso_predict = list(lasso_predict)

    return mean_absolute_percentage_error(y_test, lasso_predict)
