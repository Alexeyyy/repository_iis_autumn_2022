import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression, Ridge, Perceptron

from chart import Chart


class Regression:
    def __init__(self):
        x, data = self.create_data_classification()

        linear = self.create_regression(data, LinearRegression())
        ridge = self.create_regression(data, Ridge(alpha=1))
        perceptron = self.create_regression(data, Perceptron())

        chart = Chart()
        chart.create_plot(x, data, linear, ridge, perceptron)


    def create_data_classification(self):
        x, y = make_classification(n_samples=500, n_features=2,
                                   n_redundant=0, n_informative=2, n_clusters_per_class=1)
        rng = np.random.RandomState(2)
        x += 2 * rng.uniform(size=x.shape)
        return x, train_test_split(
            x, y, test_size=.05, random_state=42)


    def create_regression(self, train_test_splits, model):
        if isinstance(model, LinearRegression):
            model.fit(train_test_splits[0], train_test_splits[2])  # train
            score = model.score(train_test_splits[1], train_test_splits[3])  # test
            return model, score

        if isinstance(model, Perceptron):
            model.fit(train_test_splits[0], y=train_test_splits[2])
            scores = cross_val_score(model, train_test_splits[1], train_test_splits[3],
                                     scoring="neg_mean_squared_error", cv=5)

            return model, -scores.mean()

        if isinstance(model, Ridge):
            pipeline = Pipeline([("polynomial_features", PolynomialFeatures(degree=3)), ("ridge_regression", model)])
            pipeline.fit(train_test_splits[0], train_test_splits[2])
            scores = cross_val_score(pipeline, train_test_splits[1], train_test_splits[3],
                                     scoring="neg_mean_squared_error", cv=5)

            return pipeline, -scores.mean()