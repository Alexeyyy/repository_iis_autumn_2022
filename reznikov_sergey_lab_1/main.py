from matplotlib import pyplot as plt

from dataset import generate_dataset, generate_test
from plot import create_plot
from regression import linear_regression, polynomial_regression, ridge_regression


if __name__ == '__main__':
    # Получаем данные и генерим тестовую
    x, y = generate_dataset()
    x_train, x_test, y_train, y_test = generate_test(x, y)

    # Показалось концептуально верно работать с 2 измерениями, вместо 3 данных
    # Здесь генерим дату, с которой будет удобно работать
    x_train_data = x_train[:, 0].reshape(-1, 1)
    y_train_data = x_train[:, 1].reshape(-1, 1)
    x_test_data = x_test[:, 0].reshape(-1, 1)
    y_test_data = x_test[:, 1].reshape(-1, 1)

    # Готовим данные для прохождения иттераций
    model_list = [linear_regression(x_train_data, y_train_data, x_test_data, y_test_data),
                  polynomial_regression(x_train_data, y_train_data, x_test_data, y_test_data),
                  ridge_regression(x_train_data, y_train_data, x_test_data, y_test_data)]
    names_list = ["Linear", "Polynomial", "Ridge"]

    # Через иттератор отрисовываем каждый график в сабплотах и потом выводим его
    for i in range(len(model_list)):
        current_subplot = plt.subplot(1, 3, i+1)
        create_plot(current_subplot, model_list[i][0], model_list[i][1], x, y, names_list[i])
    plt.show()
