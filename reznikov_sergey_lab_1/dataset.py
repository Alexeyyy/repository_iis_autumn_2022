import numpy as np
from sklearn.datasets import make_circles
from sklearn.model_selection import train_test_split


# Генерим круги
def generate_dataset():
    X, y = make_circles(n_samples=500, noise=0.3, random_state=0)
    rng = np.random.RandomState(2)
    X += 4 * rng.uniform(size=X.shape)
    return X, y


# Разбиваем на выборки
def generate_test(X, y):
    return train_test_split(X, y, test_size=.4)
