from matplotlib import pyplot as plt


# Рисуем полученную модель
def create_plot(current_subplot, model, score, x, y, title):
    # Задаем цвет и в заголовок пишем название и оценку
    cm = plt.cm.RdBu
    current_subplot.set_title(title + (', score is: %.5f' % score))

    # Получаем данные по x, сортируем и на основании predict рисуем график
    x_data = sorted(x[:, 0].reshape(-1, 1))
    current_subplot.plot(x_data, model.predict(x_data).reshape(-1, 1))

    # Выводим весь набор данных
    current_subplot.scatter(
        x[:, 0], x[:, 1], c=y, cmap=cm)
