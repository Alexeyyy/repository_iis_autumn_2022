from dataset import load_dataset
from fit import fit_models
from ranks import calc_mean, get_ranks

x, y, names = load_dataset()

lm, ridge, lasso, rfe, f = fit_models(x, y)

ranks = get_ranks(lm, ridge, lasso, rfe, f, names)

mean = calc_mean(ranks)

print("MEAN", mean)
