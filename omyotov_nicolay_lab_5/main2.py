
from sklearn.model_selection import train_test_split

from dataset import load_dataset
from lasso_test import lasso_test


# Load dataset
X, Y, name = load_dataset()

# Split dataset
X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.05, random_state=42)

for i in range(6):
    lasso_test(X_train, X_test, y_train, y_test, i/pow(10, i))
