from scores import MAPE
from sklearn.linear_model import Lasso


def lasso_test(X_train, X_test, y_train, y_test, alpha):
    lasso = Lasso(alpha=alpha)

    # Train model
    lasso.fit(X_train, y_train)

    # Show score of model
    lasso_predict = lasso.predict(X_test)
    lasso_MAPE = MAPE(y_test, lasso_predict)
    print("MAPE value: ", lasso_MAPE)
    Accuracy = 100 - lasso_MAPE
    print(
        'Accuracy of Lasso Regression(alpha={:}): {:0.2f}%.'.format(alpha, Accuracy))
