###### ПИбд-41 Омётов Николай

# Лабораторная работа №5

## Регрессия

### Как запустить лабораторную работу

1. Установить python, numpy, sklearn
1. Запустить команду `python main1.py`
1. Запустить команду `python main2.py`

### Использованные технологии

python, numpy, sklearn

### Что делает программа?

Цель программы: на основе данных о звуковых файлах разных жанров обучить модель регрессии на предсказание темпа звукового файла.

Вариант 18: Лассо-регрессия

# 1 шаг определение признаков

LINEAR [('perceptr_mean', 1.0), ('harmony_mean', 0.4365), ('chroma_stft_mean', 0.0503), ('zero_crossing_rate_mean', 0.0332), ('rms_mean', 0.0274), ('mfcc12_mean', 0.001), ('mfcc13_mean', 0.0004), ('mfcc17_mean', 0.0004), ('mfcc10_mean', 0.0003), ('mfcc16_mean', 0.0003), ('mfcc2_mean', 0.0002), ('mfcc5_mean', 0.0002), ('mfcc8_mean', 0.0002), ('mfcc11_mean', 0.0002), ('mfcc3_mean', 0.0001), ('mfcc4_mean', 0.0001), ('mfcc6_mean', 0.0001), ('mfcc7_mean', 0.0001), ('mfcc14_mean', 0.0001), ('mfcc15_mean', 0.0001), ('mfcc18_mean', 0.0001), ('mfcc20_mean', 0.0001), ('spectral_centroid_mean', 0.0), ('spectral_bandwidth_mean', 0.0), ('rolloff_mean', 0.0), ('mfcc1_mean', 0.0), ('mfcc9_mean', 0.0), ('mfcc19_mean', 0.0)]

RIDGE [('perceptr_mean', 1.0), ('harmony_mean', 0.4234), ('chroma_stft_mean', 0.1344), ('zero_crossing_rate_mean', 0.0921), ('rms_mean', 0.0738), ('mfcc12_mean', 0.0027), ('mfcc17_mean', 0.0011), ('mfcc13_mean', 0.001), ('mfcc16_mean', 0.001), ('mfcc10_mean', 0.0007), ('mfcc11_mean', 0.0006), ('mfcc2_mean', 0.0005), ('mfcc5_mean', 0.0005), ('mfcc8_mean', 0.0004), ('mfcc4_mean', 0.0003), ('mfcc3_mean', 0.0002), ('mfcc6_mean', 0.0002), ('mfcc7_mean', 0.0002), ('mfcc14_mean', 0.0002), ('mfcc18_mean', 0.0002), ('mfcc20_mean', 0.0002), ('mfcc15_mean', 0.0001), ('mfcc19_mean', 0.0001), ('spectral_centroid_mean', 0.0), ('spectral_bandwidth_mean', 0.0), ('rolloff_mean', 0.0), ('mfcc1_mean', 0.0), ('mfcc9_mean', 0.0)]

LASSO [('chroma_stft_mean', 1.0), ('rms_mean', 0.5551), ('zero_crossing_rate_mean', 0.4785), ('mfcc12_mean', 0.0202), ('mfcc17_mean', 0.0082), ('mfcc13_mean', 0.0075), ('mfcc16_mean', 0.0074), ('mfcc10_mean', 0.0057), ('mfcc11_mean', 0.0046), ('mfcc5_mean', 0.0041), ('mfcc2_mean', 0.0038), ('mfcc8_mean', 0.0033), ('mfcc4_mean', 0.002), ('mfcc6_mean', 0.0019), ('mfcc7_mean', 0.0019), ('mfcc3_mean', 0.0018), ('mfcc18_mean', 0.0015), ('mfcc14_mean', 0.0014), ('mfcc20_mean', 0.0013), ('mfcc15_mean', 0.0006), ('mfcc19_mean', 0.0006), ('spectral_centroid_mean', 0.0004), ('mfcc1_mean', 0.0002), ('mfcc9_mean', 0.0002), ('spectral_bandwidth_mean', 0.0001), ('rolloff_mean', 0.0001), ('harmony_mean', 0.0), ('perceptr_mean', 0.0)]

RFE [('chroma_stft_mean', 1.0), ('rms_mean', 1.0), ('zero_crossing_rate_mean', 1.0), ('mfcc3_mean', 1.0), ('mfcc5_mean', 1.0), ('mfcc6_mean', 1.0), ('mfcc7_mean', 1.0), ('mfcc8_mean', 1.0), ('mfcc10_mean', 1.0), ('mfcc11_mean', 1.0), ('mfcc12_mean', 1.0), ('mfcc13_mean', 1.0), ('mfcc16_mean', 1.0), ('mfcc17_mean', 1.0), ('mfcc14_mean', 0.8571), ('mfcc18_mean', 0.8571), ('mfcc2_mean', 0.7143), ('mfcc4_mean', 0.7143),
('mfcc15_mean', 0.5714), ('mfcc20_mean', 0.5714), ('mfcc1_mean', 0.4286), ('mfcc9_mean', 0.4286), ('spectral_centroid_mean', 0.2857), ('mfcc19_mean', 0.2857), ('spectral_bandwidth_mean', 0.1429), ('rolloff_mean', 0.1429), ('harmony_mean', 0.0), ('perceptr_mean', 0.0)]

f_regression [('chroma_stft_mean', 1.0), ('spectral_bandwidth_mean', 0.8568), ('mfcc2_mean', 0.6548), ('spectral_centroid_mean', 0.5976), ('rolloff_mean', 0.5493), ('zero_crossing_rate_mean', 0.4555), ('mfcc4_mean', 0.3797), ('rms_mean', 0.3036), ('mfcc1_mean', 0.1824), ('mfcc6_mean', 0.142), ('mfcc10_mean', 0.0894), ('mfcc8_mean', 0.0884), ('mfcc11_mean', 0.0778), ('mfcc17_mean', 0.0747), ('mfcc9_mean', 0.0706), ('mfcc15_mean', 0.0611), ('mfcc13_mean', 0.0583), ('mfcc12_mean', 0.0397), ('mfcc19_mean', 0.0379), ('mfcc7_mean', 0.0269), ('mfcc3_mean', 0.0173), ('perceptr_mean', 0.0125), ('mfcc14_mean', 0.0111), ('mfcc20_mean', 0.0071), ('mfcc16_mean', 0.0051), ('harmony_mean', 0.0033), ('mfcc18_mean', 0.0003), ('mfcc5_mean', 0.0)]

MEAN [('perceptr_mean', 0.2), ('harmony_mean', 0.0873), ('chroma_stft_mean', 0.0101), ('zero_crossing_rate_mean', 0.0066), ('rms_mean', 0.0055), ('mfcc12_mean', 0.0002), ('mfcc13_mean', 0.0001), ('mfcc17_mean', 0.0001), ('mfcc10_mean', 0.0001), ('mfcc16_mean', 0.0001), ('mfcc2_mean', 0.0), ('mfcc5_mean', 0.0), ('mfcc8_mean', 0.0), ('mfcc11_mean', 0.0), ('mfcc3_mean', 0.0), ('mfcc4_mean', 0.0), ('mfcc6_mean', 0.0), ('mfcc7_mean', 0.0), ('mfcc14_mean', 0.0), ('mfcc15_mean', 0.0), ('mfcc18_mean', 0.0), ('mfcc20_mean', 0.0), ('spectral_centroid_mean', 0.0),
('spectral_bandwidth_mean', 0.0), ('rolloff_mean', 0.0), ('mfcc1_mean', 0.0), ('mfcc9_mean', 0.0), ('mfcc19_mean', 0.0)]

Отсечение признаков, у которых MEAN ниже 0.001. Выделенные признаки для дальнейшего обучения модели Lasso.

- perceptr_mean
- harmony_mean
- chroma_stft_mean
- zero_crossing_rate_mean
- rms_mean

# 2 шаг обучение модели Lasso с разными параметрами

Линейная регрессия вычисляет коэффициенты каждой переменной модели. Увеличение сложности данных сильно влияет на значение коэффициентов. Это делает модель чувствительной к дальнейшим входным данным и активно изменяет её, теряя силу ранних данных.

Для исправления этой проблемы используется регрессия Лассо, также известная как регрессия L1. Mодель влияет на силу значений коэффициентов. Таким образом, он манипулирует функцией потерь, включая дополнительные затраты для переменных модели, которые имеют большое значение коэффициентов. Это штрафует модель за абсолютные значения коэффициента. Тем самым он позволяет значению коэффициентов (которые не вносят вклад в предикторную переменную) стать равным нулю. Кроме того, он удаляет эти входные функции из модели.

Использовалась оценка MAPE, так как темп - положительное число и проблем с вычислением формулы не возникло.

Файл genres_features_30_sec.csv

- Lasso Regression(alpha=1.0): 82.67%
- Lasso Regression(alpha=0.1): 82.67%
- Lasso Regression(alpha=0.01): 82.48%
- Lasso Regression(alpha=0.001): 82.46%
- Lasso Regression(alpha=0.0001): 82.58%
- Lasso Regression(alpha=1e-05): 82.59%

Файл genres_features_3_sec.csv

- Lasso Regression(alpha=1.0): 77.06%
- Lasso Regression(alpha=0.1): 77.06%
- Lasso Regression(alpha=0.01): 77.05%
- Lasso Regression(alpha=0.001): 77.04%
- Lasso Regression(alpha=0.0001): 77.06%
- Lasso Regression(alpha=1e-05): 77.06%

# Выводы

Были выбраны основные признаки для обучения модели, которые имеют большее влияние на предсказание. Проведены несколько тестов по обучению модели Lasso с разными alpha (силой влияния регуляризации), но это не оказало большого влияния. Аугментация привела к снижению качества работы модели. Точность в 82.59% приемлемая.
