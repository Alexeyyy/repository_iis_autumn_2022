import config
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter
from sklearn.linear_model import Lasso, Ridge, LinearRegression


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 4), ranks)
    ranks = dict(zip(names, ranks))
    ranks = sorted(ranks.items(), key=itemgetter(1), reverse=True)
    return ranks


def flip_array(arr):
    return -1 * arr + np.max(arr)


def get_ranks(lm: LinearRegression, ridge: Ridge, lasso: Lasso, rfe, f, names):
    ranks = dict()

    ranks[config.LINEAR_TITLE] = rank_to_dict(lm.coef_, names)
    ranks[config.RIDGE_TITLE] = rank_to_dict(ridge.coef_, names)
    ranks[config.LASSO_TITLE] = rank_to_dict(lasso.coef_, names)
    ranks[config.RFE_TITLE] = rank_to_dict(flip_array(rfe.ranking_), names)
    ranks[config.F_REGRESSION_TITLE] = rank_to_dict(f, names)

    for key, value in ranks.items():
        print(key, value, '\n')

    return ranks


def calc_mean(ranks):
    mean = {}
    for key, value in ranks.items():
        for item in value:
            if (item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]
    for key, value in mean.items():
        res = value/len(ranks)
        mean[key] = round(res, 4)
    return sorted(mean.items(), key=itemgetter(1), reverse=True)
