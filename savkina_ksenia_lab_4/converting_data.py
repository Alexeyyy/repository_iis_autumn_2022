import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


def gender_to_number(gender):
    if gender == "Male":
        return 0
    return 1


def age_to_number(age):
    limits = age.split(' ')
    left = int(limits[0])
    if limits[2] != 'above':
        right = int(limits[2])
    else:
        right = 60
    return (left + right) / 2


def frequency_to_number(frequency):
    if frequency == "Never":
        return 0
    elif frequency == "1 to 2 times a week":
        return 1
    elif frequency == "2 to 3 times a week":
        return 2
    elif frequency == "3 to 4 times a week":
        return 3
    elif frequency == "5 to 6 times a week":
        return 4
    elif frequency == "Everyday":
        return 5


def exercise_status_to_number(status):
    if status == "I don't really exercise":
        return 0
    elif status == "Alone":
        return 1
    elif status == "With a friend":
        return 2
    elif status == "With a group":
        return 3
    elif status == "Within a class environment":
        return 4


def time_to_number(time):
    if time == "Early morning":
        return 0
    elif time == "Afternoon":
        return 1
    elif time == "Evening":
        return 2


def training_time_to_number(training_time):
    if training_time == "I don't really exercise":
        return 0
    elif training_time == "30 minutes":
        return 1
    elif training_time == "1 hour":
        return 2
    elif training_time == "2 hours":
        return 3
    elif training_time == "3 hours and above":
        return 4


def diet_to_number(diet):
    if diet == "No":
        return 0
    elif diet == "Not always":
        return 1
    elif diet == "Yes":
        return 2


def yes_no_to_number(answer):
    if answer == "No":
        return 0
    return 1


def get_data():
    vectorizer = TfidfVectorizer()

    # Создаем датафрейм
    data = pd.read_csv('fitness analysis.csv')

    data['Your gender '] = data['Your gender '].apply(gender_to_number)
    data['Your age '] = data['Your age '].apply(age_to_number)

    train_test_feature_matrix = vectorizer.fit_transform(data['Your name ']).toarray()
    a = pd.DataFrame(train_test_feature_matrix)
    data['Your name '] = a[a.columns[1:]].apply(lambda x: sum(x.dropna().astype(float)), axis=1)

    data['How often do you exercise?'] = data[
        'How often do you exercise?'].apply(frequency_to_number)
    data['Do you exercise ___________ ?'] = data[
        'Do you exercise ___________ ?'].apply(exercise_status_to_number)
    data['What time if the day do you prefer to exercise?'] = data[
        'What time if the day do you prefer to exercise?'].apply(time_to_number)
    data['How long do you spend exercising per day ?'] = data[
        'How long do you spend exercising per day ?'].apply(training_time_to_number)
    data['Would you say you eat a healthy balanced diet ?'] = data[
        'Would you say you eat a healthy balanced diet ?'].apply(diet_to_number)
    data['Have you ever purchased a fitness equipment?'] = data[
        'Have you ever purchased a fitness equipment?'].apply(yes_no_to_number)
    data['Have you ever recommended your friends to follow a fitness routine?'] = data[
        'Have you ever recommended your friends to follow a fitness routine?'].apply(yes_no_to_number)

    data.pop('Timestamp')
    data.pop('What barriers, if any, prevent you from exercising more regularly?           (Please select all that '
             'apply)')
    data.pop('What form(s) of exercise do you currently participate in ?                        (Please select all '
             'that apply)')
    data.pop('What prevents you from eating a healthy balanced diet, If any?                         (Please select '
             'all that apply)')
    data.pop('What motivates you to exercise?         (Please select all that applies )')

    return data
