import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram

from converting_data import get_data


def main():
    data = get_data()

    # Исключаем информацию об уровне физ. подготовки, сохраняем для дальнейшего использования
    varieties = list(data.pop('How do you describe your current level of fitness ?'))

    # Извлекаем измерения как массив NumPy
    samples = data.values

    methods = ['single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward']

    for method in methods:
        # Реализация иерархической кластеризации при помощи функции linkage
        mergings = linkage(samples, method=method)

        # Строим дендрограмму, указав параметры удобные для отображения
        dendrogram(mergings,
                   labels=varieties,
                   leaf_rotation=90,
                   leaf_font_size=8,
                   )

        plt.title(method)
        plt.show()


if __name__ == '__main__':
    main()
