Ссылка на видео:https://youtu.be/gehr0Kmq95w


Выходные данные: решение задачи классификации с помощью дерева 
решений:
Признаки:
['Pclass' 'Name' 'Sex']
Важность признаков:
[0.10488003 0.59848156 0.29663841]


Два наиболее важных признака – Name (ФИО) и Sex (пол).

Ход работы
1. Сначала считываются данные из файла titanic.csv. Для этого 
используется функция pandas.read_csv.
2. Все строковые значения в колонках Name, Sex приводятся к числу с 
использованием функции pandas.DataFrame.apply.
3. Удаляются пустые значения в колонках Pclass, Name, Sex. 
4. Отбираются нужные столбцы (Pclass, Name, Sex). 
5. Выбирается целевая переменная (Survived).
6. Создается решающее дерево из класса
sklearn.tree.DecisionTreeСlassifier. 
7. С помощью функции fit производится обучение решающего дерева.
8. Распечатываем результат, используя поля feature_names_in_ (названия 
признаков) и feature_importances_ (важность признаков).
Заключение
В ходе лабораторной работы я научилсь использовать решающие 
деревья для определения важности признаков.