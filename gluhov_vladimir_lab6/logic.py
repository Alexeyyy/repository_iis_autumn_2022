import numpy as np
import pandas
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier

from constants import *


def create_data():
    data = pandas.read_csv('Mobile dataset.csv', on_bad_lines='skip')

    data['num_of_ratings'] = data['num_of_ratings']
    data['battery_capacity'] = data['battery_capacity'].apply(Pop_editing)
    data['sales_price'] = data['sales_price'].apply(int)

    return data


def Pop_editing(pop):
    if pop < MIN_POP:
        pop = MIN_VALUE
    elif MIN_POP <= pop <= MAX_POP:
        pop = MID_VALUE
    elif pop > MAX_POP:
        pop = MAX_VALUE
    return pop


def create_train_and_test(data):
    return train_test_split(
        data[['sales_price', 'battery_capacity']], data['num_of_ratings'], test_size=0.95, random_state=42)


def random_state_fit(x, y, x_test, y_test):
    mlr = MLPClassifier(max_iter=2000, n_iter_no_change=20, activation='relu', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)
    mlr.fit(x, y)
    y_pred = mlr.predict(x_test)
    accuracy = accuracy_score(y_test, np.round(y_pred))

    return accuracy


def algoritm_grade():
    data = create_data()
    x_train, x_test, y_train, y_test = create_train_and_test(data)

    accuracy = random_state_fit(x_train, y_train, x_test, y_test)

    print("Точность - ", accuracy)