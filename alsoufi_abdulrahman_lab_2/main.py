import numpy as np
from sklearn.feature_selection import RFE
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.preprocessing import MinMaxScaler


def make_friedman(size):
    np.random.seed(0)
    X = np.random.uniform(0, 1, (size, 14))
    # Задаем функцию-выход: регрессионную проблему Фридмана
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2
         + 10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))  # имя_признака: оценка_признака


def rank_to_dict_rfe(ranks, names):
    ranks = map(lambda x: 1 / x, ranks)
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def sort_dict_by_values(dict):
    sorted_dict = {}
    sorted_keys = sorted(dict, key=dict.get, reverse=True)
    for key in sorted_keys:
        sorted_dict[key] = dict[key]
    return sorted_dict


def sort(dict_ranks):
    for key in dict_ranks:
        dict_ranks[key] = sort_dict_by_values(dict_ranks[key])


X, Y = make_friedman(750)

# Linear Regression
linear_reg = LinearRegression()
linear_reg.fit(X, Y)
# Randomized Lasso
lasso = Lasso(alpha=.05)
lasso.fit(X, Y)
# Recursive Feature Elimination – RFE
rfe = RFE(linear_reg)
rfe.fit(X, Y)

# names signs
names = ["x%s" % i for i in range(1, 15)]
dict_ranks = {
    'Linear Regression\n': rank_to_dict(linear_reg.coef_, names),
    'Randomized Lasso\n': rank_to_dict(lasso.coef_, names),
    'Recursive Feature Elimination\n': rank_to_dict_rfe(rfe.ranking_, names)
}
sort(dict_ranks)

for key in dict_ranks:
    print(key, dict_ranks[key])
