import pandas as pd
from bringingData import bring
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_percentage_error

data = pd.read_csv('StudentsPerformance.csv')

x, y = bring(data, 1)

X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.02, random_state=42)


reg = LinearRegression()

reg.fit(X_train, y_train)

reg_predict = reg.predict(X_test)
print(mean_absolute_percentage_error(y_true=y_test,y_pred=reg_predict))