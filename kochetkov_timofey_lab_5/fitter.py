from sklearn.linear_model import Lasso, Ridge, LinearRegression
from sklearn.feature_selection import RFE, f_regression


def fit_models(x, y):
    reg = LinearRegression()
    reg.fit(x, y)

    ridge = Ridge(alpha=0.001)
    ridge.fit(x, y)

    lasso = Lasso(alpha=0.001)
    lasso.fit(x, y)

    rfe = RFE(lasso, step=21)
    rfe.fit(x, y)

    f, pval = f_regression(x, y, center=False)

    return reg, ridge, lasso, rfe, f
