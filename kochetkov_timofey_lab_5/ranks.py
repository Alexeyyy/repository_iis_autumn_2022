import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter
from config import names

def rank_to_dict(ranks):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 4), ranks)
    ranks = dict(zip(names, ranks))
    ranks = sorted(ranks.items(), key=itemgetter(1), reverse=True)
    return ranks

def change_array(arr):
    return-1 * arr + np.max(arr)

def get_ranks(reg, ridge, lasso, rfe, f):
    ranks = dict()

    ranks["Linear"] = rank_to_dict(reg.coef_)
    ranks["Ridge"] = rank_to_dict(ridge.coef_)
    ranks["Lasso"] = rank_to_dict(lasso.coef_)
    ranks["RFE"] = rank_to_dict(change_array(rfe.ranking_))
    ranks["F regression"] = rank_to_dict(f)

    for key, value in ranks.items():
        print(key, value)
        print('\n')
    return ranks

def calc_mean(ranks):
    mean = {}
    for key, value in ranks.items():
        for item in value:
            if (item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]
    for key, value in mean.items():
        res = value/len(ranks)
        mean[key] = round(res, 4)
    mean = sorted(mean.items(), key=itemgetter(1), reverse=True)
    print("MEAN", mean)

