import pandas as pd
from bringingData import bring
from fitter import fit_models
from ranks import calc_mean, get_ranks

data = pd.read_csv('StudentsPerformance.csv')

x, y = bring(data, 0)
# print(x["parental level of education"])
reg, ridge, lasso, rfe, f = fit_models(x, y)

ranks = get_ranks(reg, ridge, lasso, rfe, f)
calc_mean(ranks)

