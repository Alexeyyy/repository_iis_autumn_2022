import pandas
import numpy as np
from sklearn.tree import DecisionTreeClassifier


# The problem of the passengers of the Titanic
def solve_task(path):
    dataset = pandas.read_csv(path, index_col='PassengerId')

    dataset = dataset.loc[(np.isnan(dataset['Sex']) == False) & (np.isnan(dataset['Age']) == False) &
                          (np.isnan(dataset['sibSp']) == False)]

    X = dataset[['Sex', 'Age', 'sibSp']]

    print(X.head())

    y = dataset['Survived']

    clf = DecisionTreeClassifier()
    clf.fit(X, y)

    print('\nImportance of signs:', clf.feature_importances_)


if __name__ == '__main__':

    solve_task('titanic.csv')
