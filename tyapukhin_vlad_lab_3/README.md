# Лабораторная работа №3
### Выполнил студент ПИбд-41 Тяпухин Влад
### Тема: Деревья решений

**Как запустить лабораторную работу**

* Необходимо установить: python, sklearn, numpy, pandas
* Запустить команду `main` из классов `main1.py` и `main2.py`

**Что делает программа**

***Часть №1***

По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений), в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров два наиболее важных признака из трех рассматриваемых (по варианту).

Вариант - 23

Признаки - Sex, Age, sibSp

Файл с данными - titanic.csv

***Часть №2***

Решите с помощью библиотечной реализации дерева решений задачу из лабораторной работы «Веб-сервис «Дерево решений» по предмету «Методы искусственного интеллекта» на 99% ваших данных. 
Также требуется найти по различным характеристикам наиболее влиятельные признаки.
Проверьте работу модели на оставшемся проценте, сделайте вывод.

В качестве основного признака выбран Close (цена в конце дня)

Зависимые признаки - Date (время года), Open (цена в начале дня), High, Low

Файл с данными - PKRX.csv

### Результаты экспериментов

***Часть №1***

В результате эксперимента наиболее зависимыми признаками оказались возраст и пол человека:

![alt text](images/part_one.png "Part_1")


***Часть №2***

Эксперимент #1

В результате эксперимента наиболее значимым признаком была выявлена максимальная цена в течении дня. Время года не оказывает практически никакого влияния.
Модель, обученная на 99% данных показала на 1% данных неудовлетворительный рейтинг в 0.33.

Такой рейтинг является неточным из-за двух причин:
* Рассматриваемые данные имели пробелы между днями и учитывали только один год (2021-2022)
* Для обучения с помощью библиотеки DecisionTreeСlassifier были необходимы целочисленные данные в `y`, иначе выдавалось исключение `Unknown label type: 'continuous'`. Для курса валют такое округление может является критичным.  


![alt text](images/part_two.png "Part_2")
