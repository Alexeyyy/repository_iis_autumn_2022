import pandas
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from datetime import datetime

WINTER = 0
SPRING = 1
SUMMER = 2
AUTUMN = 3


# The problem of currencies
def solve_task(path):
    dataset = pandas.read_csv(path)

    dataset['Date'] = dataset['Date'].apply(date_to_season)

    dataset = dataset.loc[(np.isnan(dataset['Open']) == False) & (np.isnan(dataset['High']) == False) &
                          (np.isnan(dataset['Low']) == False) & (np.isnan(dataset['Close']) == False)]

    def calculate_score():
        X = dataset[['Date', 'Open', 'High', 'Low']]
        y = round_value(dataset['Close'].copy())

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.01, random_state=42)

        clf = DecisionTreeClassifier(random_state=241)
        clf.fit(X_train, y_train)

        print('\n', X.head())

        print('\nImportance of signs:', clf.feature_importances_)

        return clf.score(X_test, y_test)

    print("\nScore:", calculate_score())


def round_value(dictionary):
    for k, v in dictionary.items():
        dictionary[k] = int(round(dictionary[k]))

    return dictionary


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return WINTER
    if 3 <= date.month <= 5:
        return SPRING
    if 6 <= date.month <= 8:
        return SUMMER
    if 9 <= date.month <= 11:
        return AUTUMN


if __name__ == '__main__':
    solve_task('PKRX.csv')
