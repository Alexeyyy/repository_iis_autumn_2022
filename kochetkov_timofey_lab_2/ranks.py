import numpy as np
from sklearn.preprocessing import MinMaxScaler

def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def change_array(arr):
    return-1 * arr + np.max(arr)


def get_ranks(lasso, rfe, f):
    ranks = dict()
    names = ["x%s" % i for i in range(1, 15)]

    ranks["Lasso"] = rank_to_dict(lasso.coef_, names)
    ranks["RFE"] = rank_to_dict(change_array(rfe.ranking_), names)
    ranks["F regression"] = rank_to_dict(f, names)

    return ranks



