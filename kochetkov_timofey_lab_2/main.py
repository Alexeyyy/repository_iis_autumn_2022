from generate_dataset import generate
from fitter import fit
from ranks import get_ranks
from mean import print_mean

x, y = generate()

lasso, rfe, f = fit(x, y)

ranks = get_ranks(lasso, rfe, f)

print_mean(ranks)