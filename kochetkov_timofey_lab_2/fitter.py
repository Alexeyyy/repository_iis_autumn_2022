from sklearn.linear_model import Lasso, LinearRegression
from sklearn.feature_selection import RFE, f_regression


def fit(x, y):
    lasso = Lasso(alpha=0.01)
    lasso.fit(x, y)

    lr = LinearRegression()
    lr.fit(x, y)

    rfe = RFE(lr)
    rfe.fit(x, y)

    f, pval = f_regression(x, y, center=False)

    return lasso, rfe, f
