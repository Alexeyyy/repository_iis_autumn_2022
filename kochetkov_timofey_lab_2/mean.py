from operator import itemgetter

def print_mean(ranks):
    mean = {}
    for key, value in ranks.items():
        print(key, value)
        for item in value.items():
            if(item[0] not in mean):
                mean[item[0]] = 0
                mean[item[0]] += item[1]
    for key, value in mean.items():
        res = value/len(ranks)
        mean[key] = round(res, 2)
    mean = sorted(mean.items(), key=itemgetter(1), reverse=True)
    print("MEAN", mean)