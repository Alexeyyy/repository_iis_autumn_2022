import sys

import numpy
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.models import Sequential
from keras.utils import np_utils


def process_file():
    filename = "master and margarita.txt"
    raw_text = open(filename).read()
    # перевод символов в нижний регистр, чтобы уменьшить словарный запас, который должна выучить сеть
    raw_text = raw_text.lower()
    return raw_text


def divide_into_sequences(raw_text):
    # выбор уникальных символов
    chars = sorted(list(set(raw_text)))
    # преобразование символов в целые числа
    char_to_int = dict((c, i) for i, c in enumerate(chars))

    n_chars = len(raw_text)
    seq_length = 100
    dataX = []
    dataY = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    return dataX, dataY, chars


def get_converted_data(dataX, dataY, chars):
    n_vocab = len(chars)
    n_patterns = len(dataX)
    # преобразование для keras
    X = numpy.reshape(dataX, (n_patterns, 100, 1))
    X = X / float(n_vocab)
    y = np_utils.to_categorical(dataY)
    return X, y


def define_model(X, y):
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    return model


def fit_model(model, X, y):
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    # определение контрольных точек
    filepath = "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]
    model.fit(X, y, epochs=20, batch_size=128, callbacks=callbacks_list)


def load_model(model):
    filename = "two_layers/weights-improvement-10-1.9752.hdf5"
    model.load_weights(filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam')


def generate_characters(dataX, model, chars):
    # случайный отрывок
    start = numpy.random.randint(0, len(dataX) - 1)
    pattern = dataX[start]
    int_to_char = dict((i, c) for i, c in enumerate(chars))
    print("Seed:")
    print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")

    for i in range(1000):
        x = numpy.reshape(pattern, (1, len(pattern), 1))
        x = x / float(len(chars))
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)
        sys.stdout.write(chars[index])
        pattern.append(index)
        pattern = pattern[1:len(pattern)]

    print("\nDone.")


def main():
    raw_text = process_file()
    dataX, dataY, chars = divide_into_sequences(raw_text)
    X, y = get_converted_data(dataX, dataY, chars)
    model = define_model(X, y)

    # fit_model(model, X, y)

    load_model(model)
    generate_characters(dataX, model, chars)


if __name__ == '__main__':
    main()
