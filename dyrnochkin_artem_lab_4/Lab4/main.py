from sklearn.cluster import KMeans
import pandas
from matplotlib import pyplot as plt


def main():
    factors = ["minTFact", "maxTFact", "longFact"]
    k_means = KMeans(n_clusters=4)
    data = pandas.read_csv("weather.csv")

    i = 0
    for i in range(len(factors)):
        corr = data[["Temp", factors[i]]]
        k_means.fit(corr)
        plt.xlabel(factors[i], fontsize=14, fontweight="bold")
        plt.ylabel("Temperature", fontsize=14, fontweight="bold")

        plt.scatter(corr.values[:, 0], corr.values[:, 1], c=k_means.labels_)
        plt.show()


if __name__ == '__main__':
    main()
