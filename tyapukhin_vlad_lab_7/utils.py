import os
import sys
import numpy

from keras.callbacks import ModelCheckpoint
from constants import CHECKPOINT_DIR


def read_text(filename):
    return open(filename, 'r', encoding='UTF-8').read().lower()


def chars_to_ids(value):
    return dict((c, i) for i, c in enumerate(value))


def ids_to_chars(value):
    return dict((i, c) for i, c in enumerate(value))


def prepare_text(text, ids, seq_length):
    dataX = []
    dataY = []

    for i in range(0, len(text) - seq_length, 1):
        seq_in = text[i:i + seq_length]
        seq_out = text[i + seq_length]

        dataX.append([ids[char] for char in seq_in])
        dataY.append(ids[seq_out])

    return dataX, dataY


def print_pattern(pattern, chars):
    print('\n---PATTERN---')
    for char in [chars[value] for value in pattern]:
        sys.stdout.write(char)


def generate_characters(model, pattern, chars, n_vocab, prediction_len):
    print('\n---PREDICATION---')
    for i in range(prediction_len):
        x = numpy.reshape(pattern, (1, len(pattern), 1)) / float(n_vocab)
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)

        sys.stdout.write(chars[index])

        pattern.append(index)
        pattern = pattern[1:len(pattern)]


def get_or_create_checkpoint():
    checkpoint_prefix = os.path.join(CHECKPOINT_DIR, 'weights-improvement-{epoch:02d}-{loss:.4f}.hdf5')

    return ModelCheckpoint(
        filepath=checkpoint_prefix,
        monitor='loss',
        verbose=1,
        save_best_only=True,
        mode='min',
    )
