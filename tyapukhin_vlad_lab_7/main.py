import numpy
from keras.utils import to_categorical

from utils import chars_to_ids, ids_to_chars, prepare_text, generate_characters, read_text, print_pattern
from neural_network import Model
from constants import SEQ_LEN, N_EPOCHS, BATCH_SIZE, PREDICTION_LEN, SEQ_IN

if __name__ == '__main__':
    # Read text from file
    text = read_text('The Picture of Dorian Gray (last chapter).txt')

    # The unique characters in the file
    vocab = sorted(set(text))

    n_vocab = len(vocab)

    # Convert from characters to character IDs
    ids = chars_to_ids(vocab)

    # Convert from character IDs to characters
    chars = ids_to_chars(vocab)

    # Prepare dataset of input to output
    dataX, dataY = prepare_text(text=text, ids=ids, seq_length=SEQ_LEN)

    n_patterns = len(dataX)

    X = numpy.reshape(dataX, (n_patterns, SEQ_LEN, 1))

    # Normalize
    X = X / float(n_vocab)

    # Convert the target character into one hot encoding
    y = to_categorical(dataY)

    model = Model(X, y)
    model.load_model_weights('training_checkpoints_launch_4/weights-improvement-22-0.2522.hdf5')
    model.compile_model()
    # model.fit_model(X, y, BATCH_SIZE, N_EPOCHS)

    pattern = [ids[char] for char in SEQ_IN.lower()]

    print_pattern(pattern, chars)

    generate_characters(model, pattern, chars, n_vocab, PREDICTION_LEN)
