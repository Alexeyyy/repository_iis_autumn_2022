SEQ_IN = 'It was better not to think of the past. Nothing could alter that. It\n' + \
         'was of himself, and of his own future, that he had to think. James Vane\n'

SEQ_OUT = 'was hidden in a nameless grave in Selby churchyard. Alan Campbell had\n' + \
          'shot himself one night in his laboratory, but had not revealed the\n' + \
          'secret that he had been forced to know.'

SEQ_LEN = len(SEQ_IN)
PREDICTION_LEN = len(SEQ_OUT)

N_EPOCHS = 50
BATCH_SIZE = 128

N_UNITS = 256
DROPOUT_RATE = 0.2

CHECKPOINT_DIR = 'training_checkpoints_launch_4'
