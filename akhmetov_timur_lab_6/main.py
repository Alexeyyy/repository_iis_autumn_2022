from sklearn.metrics import mean_absolute_percentage_error
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from data import load


if __name__ == '__main__':
    X, y, names = load('TSLA.csv')

    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.1, random_state=42)

    clf = MLPClassifier(hidden_layer_sizes=(100, 100), activation='relu', solver='lbfgs', max_iter=1000)
    clf.fit(X_train, Y_train)

    y_predict = clf.predict(X_test)
    score = clf.score(X_test, Y_test)

    print(f'Score: {score}\nMAPE: {mean_absolute_percentage_error(Y_test, y_predict)}')
