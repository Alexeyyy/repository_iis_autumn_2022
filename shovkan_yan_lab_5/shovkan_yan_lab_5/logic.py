import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import pandas


def create_data():
    data = pandas.read_csv('ds_salaries.csv', on_bad_lines='skip')

    data['experience_level'] = data['experience_level'].apply(experience_level_to_num)
    data['company_size'] = data['company_size'].apply(company_size_to_num)
    data['salary_in_usd'] = data['salary_in_usd'].apply(int)

    return data


def experience_level_to_num(experience_level):
    if experience_level == "EN":
        return 0
    if experience_level == "MI":
        return 1
    if experience_level == "SE":
        return 2
    if experience_level == "EX":
        return 3


def company_size_to_num(company_size):
    if company_size == "S":
        return 0
    if company_size == "M":
        return 1
    if company_size == "L":
        return 2


def calculate_mean(ranks):
    mean = {}

    for key, value in ranks.items():
        for item in value.items():
            if item[0] not in mean:
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print("Среднее: ", mean)


def train_and_test_logistic(data):

    x_train, x_test, y_train, y_test = train_test_split(
        data[['experience_level', 'company_size']], data['salary_in_usd'], test_size=0.05, random_state=42)

    clf = LogisticRegression(random_state=0)
    clf.fit(x_train, y_train)

    logistic_predict = clf.predict(x_test)
    print("Ошибка: ", error_calculation(y_test, logistic_predict))


def error_calculation(y_actual, y_predicted):
    mape = np.mean(np.abs((y_actual - y_predicted)/(y_actual+1)))*100
    return 100 - mape


def get_ranks_and_mean():
    data = create_data()

    ranks = create_ranks(data)
    calculate_mean(ranks)
    train_and_test_logistic(data)


def rank_to_dict(ranks, names):

    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_ranks(data):
    clf = LogisticRegression(random_state=0)
    clf.fit(data[['experience_level', 'company_size']], data['salary_in_usd'])

    ranks = dict()
    ranks["Logistic Regression"] = rank_to_dict(clf.coef_[0], ['experience_level', 'company_size'])
    print(ranks)
    return ranks

