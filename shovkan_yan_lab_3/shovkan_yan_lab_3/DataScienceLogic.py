import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split


def create_data():
    data = pd.read_csv('ds_salaries.csv', on_bad_lines='skip')

    unique_numbers = list(set(data['experience_level']))
    data['experience_level'] = data['experience_level'].apply(unique_numbers.index)

    unique_numbers = list(set(data['job_title']))
    data['job_title'] = data['job_title'].apply(unique_numbers.index)

    return data


def processing_data():
    data = create_data()

    clf = DecisionTreeClassifier(random_state=241)

    # Выбор фич
    y = data["salary"]
    x = data[["experience_level", "job_title", ]]
    print(x)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.05, random_state=42)
    clf.fit(x_train, y_train)

    # Качество модели
    print(clf.score(x_test, y_test))

    # Важность
    print(clf.feature_importances_)


