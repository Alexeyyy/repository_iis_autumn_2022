import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split


def processing_data():
    data = pd.read_csv("train_and_test2.csv", index_col="Passengerid")
    clf = DecisionTreeClassifier(random_state=241)

    # Выбор фич
    y = data["2urvived"]
    x = data[["sibsp", "Parch", "Age", ]]
    print(x)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.05, random_state=42)
    clf.fit(x_train, y_train)

    # Качество модели
    print(clf.score(x_test, y_test))

    # Важность
    print(clf.feature_importances_)



