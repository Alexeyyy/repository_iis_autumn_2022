import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.axes import Axes
from matplotlib import pyplot as plt
import constants


class Chart:
    global count
    count = 0

    def create_chart(self, train, perceptron, perceptron10, perceptron100, xx0, xx1):
        cm = plt.cm.RdBu
        cm_bright = ListedColormap(['#FF0000', '#0000FF'])

        current_subplot = plt.subplot(constants.ROW, constants.COLUMN, self.increment())
        current_subplot.scatter(train[0][:, 0], train[0][:, 1], c=train[2], cmap=cm_bright)

        current_subplot = plt.subplot(constants.ROW, constants.COLUMN, self.increment())
        current_subplot.scatter(train[1][:, 0], train[1][:, 1], c=train[3], cmap=cm_bright, alpha=0.6)

        current_subplot = plt.subplot(constants.ROW, constants.COLUMN, self.increment())
        self.create_gradient(perceptron[0], current_subplot=current_subplot, title='perceptron', score=perceptron[1], xx0=xx0,
                             xx1=xx1, cm=cm)

        current_subplot = plt.subplot(constants.ROW, constants.COLUMN, self.increment())
        self.create_gradient(perceptron10[0], current_subplot=current_subplot, title='perceptron10',
                             score=perceptron10[1], xx0=xx0, xx1=xx1, cm=cm)

        current_subplot = plt.subplot(2, constants.COLUMN, self.increment())
        self.create_gradient(perceptron100[0], current_subplot=current_subplot,
                             title='Perceptron100', score=perceptron100[1], xx0=xx0, xx1=xx1, cm=cm)

    def create_plot(self, x, train, perceptron, perceptron10, perceptron100):
        current_chart = 3
        h = .02
        x0_min, x0_max = x[:, 0].min() - .5, x[:, 0].max() + .6
        x1_min, x1_max = x[:, 1].min() - .5, x[:, 1].max() + .6
        xx0, xx1 = np.meshgrid(np.arange(x0_min, x0_max, h),
                               np.arange(x1_min, x1_max, h))

        cm_bright = ListedColormap(['#FF0000', '#0000FF'])

        self.create_chart(train, perceptron, perceptron10, perceptron100, xx0, xx1)

        for i in range(current_chart, constants.MAX_CHART + 1):
            current_subplot = plt.subplot(constants.ROW, constants.COLUMN, i)
            current_subplot.scatter(
                train[0][:, 0], train[0][:, 1], c=train[2], cmap=cm_bright)
            current_subplot.scatter(
                train[1][:, 0], train[1][:, 1], c=train[3], cmap=cm_bright, alpha=0.6)

        plt.show()

    def create_gradient(self, model, current_subplot: Axes, title: str, score: float, xx0, xx1, cm):
        current_subplot.set_title(title)
        try:
            if hasattr(model, "predict"):
                Z = model.predict(np.c_[xx0.ravel(), xx1.ravel()])


        except AttributeError:
            pass

        Z = Z.reshape(xx0.shape)
        current_subplot.contourf(xx0, xx1, Z, cmap=cm, alpha=.8)
        current_subplot.set_xlim(xx0.min(), xx0.max())
        current_subplot.set_ylim(xx0.min(), xx1.max())
        current_subplot.set_xticks(())
        current_subplot.set_yticks(())
        current_subplot.text(xx0.max() - .3, xx1.min() + .3, ('%.2f' % score),
                             size=15, horizontalalignment='right')

    def increment(self):
        global count
        count += 1
        return count


