import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.datasets import make_classification, make_circles
from sklearn.linear_model import LinearRegression, Ridge, Perceptron

from chart import Chart


class Regression:
    def __init__(self):
        x, data = self.create_data_circles()

        perceptron = self.create_regression(data, Perceptron())
        perceptron10 = self.create_regression(data, MLPClassifier(solver='lbfgs', alpha=0.01, hidden_layer_sizes = (10,), random_state = 1))
        perceptron100 = self.create_regression(data, MLPClassifier(solver='lbfgs', alpha=0.01, hidden_layer_sizes = (100,), random_state = 1))

        chart = Chart()
        chart.create_plot(x, data, perceptron, perceptron10, perceptron100)


    def create_data_circles(self):
        x, y = make_circles(noise=0.2, factor=0.5)
        rng = np.random.RandomState(2)
        x += 2 * rng.uniform(size=x.shape)
        return x, train_test_split(
            x, y, test_size=.05, random_state=42)


    def create_regression(self, train_test_splits, model):
        if isinstance(model, Perceptron):
            model.fit(train_test_splits[0], train_test_splits[2])  # train
            score = model.score(train_test_splits[1], train_test_splits[3])  # test
            return model, score

        if isinstance(model, MLPClassifier):
            model.fit(train_test_splits[0], y=train_test_splits[2])
            score = model.score(train_test_splits[1], train_test_splits[3])
            return model, score
