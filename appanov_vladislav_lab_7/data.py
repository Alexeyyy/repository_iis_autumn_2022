import numpy as np
import pandas
from collections import Counter

from constants import NUMBER_OF_LINES


def create_data():
    data = pandas.read_csv('scrubbed.csv', on_bad_lines='skip')
    data = data.sample(frac=1)
    data = data[:NUMBER_OF_LINES]
    data = list(data['comments'])

    text_sample = ' '.join(data)
    return text_sample


def text_to_seq(text_sample=create_data()):
    char_counts = Counter(text_sample)
    char_counts = sorted(char_counts.items(), key=lambda x: x[1], reverse=True)

    sorted_chars = [char for char, _ in char_counts]

    char_to_idx = {char: index for index, char in enumerate(sorted_chars)}
    idx_to_char = {v: k for k, v in char_to_idx.items()}
    sequence = np.array([char_to_idx[char] for char in text_sample])

    return sequence, char_to_idx, idx_to_char
