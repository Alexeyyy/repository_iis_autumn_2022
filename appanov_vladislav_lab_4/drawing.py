import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import AgglomerativeClustering

from constants import *
from ufo import create_data


def draw_clusters():
    data = create_data()
    cluster = AgglomerativeClustering(n_clusters=NUMBER_OF_CLUSTERS, affinity='euclidean', linkage='ward')

    for time in [MONTH, WEEK, DAY, HOUR, MIN]:
        plt.xlabel("Время года", fontsize=FONT_SIZE, fontweight="bold")
        plt.ylabel("Время контакта в секундах", fontsize=FONT_SIZE, fontweight="bold")
        plt.xticks(range(4), ['Зима', 'Весна', 'Лето', 'Осень'])

        data = data.loc[data['duration (seconds)'] < time]
        corr_vonappa = np.array(data)
        cluster.fit_predict(corr_vonappa)

        plt.scatter(corr_vonappa[:, 0], corr_vonappa[:, 1], c=cluster.labels_, cmap='rainbow')
        plt.show()
