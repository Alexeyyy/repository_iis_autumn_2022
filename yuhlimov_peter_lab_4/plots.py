import numpy as np
from matplotlib import pyplot as plt


def show_plots(cluster, data):
    for price in [30, 70, 100]:
        plt.xlabel("Unemployment rate", fontsize=14, fontweight="bold")
        plt.ylabel("Oil prices", fontsize=14, fontweight="bold")
        plt.xticks(range(3), ['Low', 'Middle', 'High'])

        data = data.loc[data['oil prices'] < price]
        X = np.array(data)
        cluster.fit_predict(X)

        plt.scatter(X[:, 0], X[:, 1], c=cluster.labels_, cmap='rainbow')
        plt.show()
