import numpy as np
import pandas as pd
from constants import LOW, MIDDLE, HIGH


def load(path):
    dataset = pd.read_csv(path)

    max_value, average_value, min_value = get_max_average_min_from_list(list(dataset['unemploymentrate']))
    
    def get_nearest_value(value):
        result = min([max_value, average_value, min_value], key=lambda x: abs(value - x))
        if result == max_value:
            return HIGH
        if result == average_value:
            return MIDDLE
        if result == min_value:
            return LOW

    dataset = dataset.loc[(np.isnan(dataset['oil prices']) == False) & (np.isnan(dataset['unemploymentrate']) == False)]

    dataset['oil prices'] = dataset['oil prices'].apply(round)
    dataset['unemploymentrate'] = dataset['unemploymentrate'].apply(get_nearest_value)

    return dataset[['unemploymentrate', 'oil prices']]


def get_max_average_min_from_list(value):
    return max(value), round((max(value) - min(value)) / 2, 2), min(value)
