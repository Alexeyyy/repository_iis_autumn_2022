from sklearn.cluster import AgglomerativeClustering
from data import load
from plots import show_plots


if __name__ == '__main__':

    data = load('Economic Data - 9 Countries (1980-2020).csv')

    cluster = AgglomerativeClustering(
        n_clusters=2,
        affinity='euclidean',
        linkage='ward',
    )

    show_plots(cluster=cluster, data=data)
