# Лабораторная работа №7
## Выполнил студент ПИбд-41 Берхеев Эмиль

### Как запустить лабораторную работу

* Необходимо установить: python, sklearn, numpy, pandas
* Перейти на сайт <https://colab.research.google.com>
* Запустить файл lab7.ipynb
* Добавить данные в сэмпл дату, в нашем случае произведения Пушкина на английском

**Что делает программа**

Цель программы: генерация текста на основе рекурентной нейронной сети.

Для данной работы был выбран google colab, в связи с не столь впечатляющими показателями моей локальной вычислительной машины

5 эпох прошли за 8 минут.

Функция потерь на 5й эпохе - 2.0326

### Результаты прогонов

![alt text](images/1.png "Количество символов в тексте")

![alt text](images/2.png "Выделенные паттерны")

![alt text](images/3.png "5 эпох")

*Можно так же использовать результаты вычислений прошлых этапов, они сохраняются в коллабе и их можно подгрузить с помощью model.load_weights ("*.hdf5")

![alt text](images/6.png "Локальное сохранение")

![alt text](images/4.png "Полученный текст 1/2")

![alt text](images/5.png "Полученный текст 2/2")

Выводы: Текст генерируется с большым количеством "of the courtess", алгоритм считает, что составлять текст из наиболее популярного словосочетания является предпочтительным.

Предложения по улучшению: пересмотреть структуру определения паттернов, архитектуры обработки в общем.
