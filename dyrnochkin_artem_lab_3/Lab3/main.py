import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split


def main():
    # Загружаем данные
    data = pd.read_csv('titanic.csv', index_col='Passengerid')
    # Инициализируем
    clf = DecisionTreeClassifier(random_state=255)

    # Выбираем данные
    Y = data['2urvived']
    X = data[['Pclass', 'Parch', 'Fare', ]]
    print(X)

    # Разделяем данные
    X_train, X_test, y_train, y_test = train_test_split(
        X, Y, test_size=0.05, random_state=42)

    clf.fit(X_train, y_train)

    print(clf.score(X_test, y_test))

    selected = clf.feature_importances_
    print(selected)


if __name__ == '__main__':
    main()
