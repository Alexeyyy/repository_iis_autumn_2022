import pandas
from sklearn.tree import DecisionTreeClassifier


# Приводим к инту
def device_to_int(device):
    if device == "Computer":
        return 3
    elif device == "Mobile":
        return 2
    elif device == "Tab":
        return 1
    return 0


# Читаем данные
data = pandas.read_csv('students_online.csv')
data.dropna()
print(data)

# Улучшаем вид
data['Device'] = data['Device'].apply(device_to_int)

# Получаем выборку
train_data = data.iloc[0:int(len(data) * 0.99), :]
test_data = data.iloc[int(len(data) * 0.99):, :]

# Создаём и обучаем дерево
clf = DecisionTreeClassifier(random_state=241)
clf.fit(train_data[['Age', 'Device']],
        train_data['Flexibility Level'])

# Выводим значение
parameters = clf.feature_importances_
print(parameters)
score = clf.score(test_data[['Age', 'Device']],
                  test_data['Flexibility Level'])
print(score)
