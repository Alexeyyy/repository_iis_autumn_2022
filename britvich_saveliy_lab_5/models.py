from sklearn.linear_model import LinearRegression


def create_model(x, y):
    linear = LinearRegression()
    linear.fit(x, y)

    return linear
