import pandas as pd
from datetime import datetime

def load(path):
    dataset = pd.read_csv(path)

    names = ['qFact', 'longFact', 'latFact', 'glevelFact', 'minTFact', 'maxTFact']

    X = dataset[names]
    y = dataset['Temp']

    return X, y, names

