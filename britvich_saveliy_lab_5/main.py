from models import create_model
from ranks import calculate_mean_and_sort_list, get_ranks, calculate_mape
from data import load

from sklearn.model_selection import train_test_split


if __name__ == '__main__':
    X, y, names = load('weather.csv')

    linear = create_model(X, y)

    ranks = get_ranks(linear, names)

    print("MEAN", calculate_mean_and_sort_list(ranks))

    for test_size in [0.11, 0.51, 0.01, 0.001, 0.05, 0.5]:
        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=test_size, random_state=42)

        mape = calculate_mape(X_train, X_test, Y_train, Y_test)
        print(f'MAPE for test_size={test_size} is {float("{:.3f}".format(mape))}')
