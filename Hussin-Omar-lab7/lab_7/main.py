import numpy as np

from keras.layers import Dense, SimpleRNN, Embedding
from keras.models import Sequential
from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer


with open('File.txt', 'r', encoding='utf-8') as f:
    texts = f.read()
    texts = texts.replace('\ufeff', '')

Max_W_C = 1000
tokenizer = Tokenizer(num_words=Max_W_C, filters='!"#$.%&;()*+,/:;<=>&@[\\]^_{|}~\t\n\r', lower=True, split=' ', char_level=False)
tokenizer.fit_on_texts([texts])

dist = list(tokenizer.word_counts.items())
print(dist[:10])

data = tokenizer.texts_to_sequences([texts])
res = np.array(data[0])

inp_words = 3
n = res.shape[0] - inp_words

X = np.array([res[i:i + inp_words] for i in range(n)])
Y = to_categorical(res[inp_words:], num_classes=Max_W_C)

model = Sequential()
model.add(Embedding(Max_W_C, 256, input_length = inp_words))
model.add(SimpleRNN(128, activation='linear'))
model.add(Dense(Max_W_C, activation='softmax'))
model.summary()

model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')

history = model.fit(X, Y, batch_size=32, epochs=120)


def buildPhrase(texts, str_len=100):
    res = texts
    data = tokenizer.texts_to_sequences([texts])[0]
    for i in range(str_len):
        x = data[i: i + inp_words]
        inp = np.expand_dims(x, axis=0)

        pred = model.predict(inp)
        indx = pred.argmax(axis=1)[0]
        data.append(indx)

        res += " " + tokenizer.index_word[indx]

    return res


res = buildPhrase("One day I")
print(res)