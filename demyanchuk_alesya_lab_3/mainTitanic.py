import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from config import *
pd.options.mode.chained_assignment = None

# 1 часть
FILE_PATH = "train.csv"
REQUIRED_COLUMNS = ['Sex', 'Cabin', 'Embarked', 'Survived']
INDEX_COLUMN = 'PassengerId'
TARGET_COLUMN = 'Survived'

def read_data(file_path, required_colums, index_col):
    data = pd.read_csv(file_path, index_col = index_col)
    required_data = data[required_colums]
    return required_data

def Sex_to_bool(sex):
    if sex == "male":
        return 0
    return 1

def prepare_data(data):
    m_data = data[data['Cabin'].isnull() == False]

    m_data['Sex'] = m_data['Sex'].apply(Sex_to_bool)
    m_data['Embarked'] = m_data['Embarked'].fillna(np.random.choice(['S', 'C', 'Q']))

    le = LabelEncoder()
    le.fit(m_data['Cabin'])
    m_data['Cabin'] = le.transform(m_data['Cabin'])
    m_data = pd.get_dummies(m_data, columns=['Embarked'], prefix='embarked')

    return m_data

def print_classifier_info(feature_importances):
    feature_names = ['Sex', 'Cabin', 'Embarked']
    embarked_score = feature_importances[-3:].sum()
    scores = np.append(feature_importances[:2], embarked_score)
    scores = map(lambda score: round(score, 2), scores)
    print(dict(zip(feature_names, scores)))

if __name__ == '__main__':
    titanic_data = read_data(FILE_PATH, REQUIRED_COLUMNS, INDEX_COLUMN)
    processed_data = prepare_data(titanic_data)

    classifier_tree = DecisionTreeClassifier()
    x = processed_data.drop(TARGET_COLUMN, axis=1)
    y = processed_data[TARGET_COLUMN]

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.8,
                                                        random_state=None)

    classifier_tree.fit(x_train, y_train)
    print_classifier_info(classifier_tree.feature_importances_)
    print("Оценка качества - ", classifier_tree.score(x_test, y_test))


