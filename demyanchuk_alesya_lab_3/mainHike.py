import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier

pd.options.mode.chained_assignment = None

FILE_PATH = "data.csv"
REQUIRED_COLUMNS = ['uphill', 'downhill', 'max_speed', 'difficulty']
INDEX_COLUMN = '_id'
TARGET_COLUMN = 'difficulty'

def read_data(file_path, required_colums, index_col):
    data = pd.read_csv(file_path, index_col=index_col)
    required_data = data[required_colums]
    return required_data

    
def prepare_data(data):
    m_data = data
    le = LabelEncoder()
    le.fit(m_data['uphill'])
    m_data['uphill'] = le.transform(m_data['uphill'])

    le = LabelEncoder()
    le.fit(m_data['downhill'])
    m_data['downhill'] = le.transform(m_data['downhill'])

    unique_numbers = list(set(data['difficulty']))
    m_data['difficulty'] = data['difficulty'].apply(unique_numbers.index)

    return m_data

def print_classifier_info(feature_importances):
    feature_names = ['uphill', 'downhill', 'max_speed',]
    embarked_score = feature_importances[-3:].sum()
    scores = np.append(feature_importances[:2], embarked_score)
    scores = map(lambda score: round(score, 2), scores)
    print(dict(zip(feature_names, scores)))

if __name__ == '__main__':
    hike_data = read_data(FILE_PATH, REQUIRED_COLUMNS, INDEX_COLUMN)
    processed_data = prepare_data(hike_data)

    classifier_tree = DecisionTreeClassifier()
    x = processed_data.drop(TARGET_COLUMN, axis=1)
    y = processed_data[TARGET_COLUMN]

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.8,
                                                        random_state=None)

    classifier_tree.fit(x_train, y_train)
    print_classifier_info(classifier_tree.feature_importances_)
    print("Оценка качества (задача классификации) - ", classifier_tree.score(x_test, y_test))


