import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_moons
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline


def linear(X_train, X_test, y_train, y_test):
    model = LinearRegression().fit(X_train, y_train)
    y_predict = model.predict(X_test)
    plt.title('Линейная регрессия')
    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_predict, color='blue')
    plt.show()


def polynomial(X_train, y_train):
    polynomial_features = PolynomialFeatures(degree=4)  # коэффициенты полинома
    X_polynomial = polynomial_features.fit_transform(X_train)
    base_model = LinearRegression()
    base_model.fit(X_polynomial, y_train)
    y_predict = base_model.predict(X_polynomial)
    plt.title('Полиномиальная регрессия')
    plt.scatter(X_train, y_train, color='black')
    plt.plot(X_train, y_predict, color='blue')
    plt.show()


def ridge(X_train, X_test, y_train, y_test):
    model = Pipeline([('poly', PolynomialFeatures(degree=4)), ('ridge', Ridge(alpha=1.0))])
    model.fit(X_train, y_train)
    y_predict = model.predict(X_test)
    plt.title('Гребневая полиномиальная регрессия')
    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_predict, color='blue')
    plt.show()


make_moons = make_moons(noise=0.3, random_state=777)
X, y = make_moons
X = X[:, np.newaxis, 1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
linear(X_train, X_test, y_train, y_test)
polynomial(X_train, y_train)
ridge(X_train, X_test, y_train, y_test)





