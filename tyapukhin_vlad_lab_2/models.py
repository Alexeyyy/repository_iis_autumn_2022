from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import f_regression
from sklearn.ensemble import RandomForestRegressor


def create_models(x, y):
    lr = LinearRegression()
    lr.fit(x, y)

    rfr = RandomForestRegressor(bootstrap=True)
    rfr.fit(x, y)

    f, p_val = f_regression(x, y, center=True)

    return lr, rfr, f
