import numpy as np
from sklearn.preprocessing import MinMaxScaler
from operator import itemgetter


def get_ranks(linear, rfr, f):
    ranks = dict()
    names = ["x%s" % i for i in range(1, 15)]

    ranks['Linear regression'] = sort_by_desc(rank_to_dict(linear.coef_, names))
    ranks['RFR'] = sort_by_desc(rank_to_dict(rfr.feature_importances_, names))
    ranks['f_regression'] = sort_by_desc(rank_to_dict(f, names))

    return ranks


def sort_by_desc(dictionary):
    return dict(sorted(dictionary.items(), key=itemgetter(1), reverse=True))


def calculate_mean_and_sort_list(ranks):
    mean = {}

    for key, value in ranks.items():
        print(key, value)
        for item in value.items():
            if item[0] not in mean:
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    return sort_by_desc(mean)


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)

    minmax = MinMaxScaler()

    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))
