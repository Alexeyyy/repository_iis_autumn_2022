from models import create_models
from data import generate_dataset
from ranks import calculate_mean_and_sort_list, get_ranks


if __name__ == '__main__':
    x, y = generate_dataset()

    linear, rfr, f = create_models(x, y)

    ranks = get_ranks(linear, rfr, f)

    print("MEAN", calculate_mean_and_sort_list(ranks))
