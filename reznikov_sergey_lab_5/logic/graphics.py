import math

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.figure import Figure
from matplotlib import pyplot as plt


def create_plot(model, score, x, y, title, column1, column2):
    fig = Figure(figsize=(9, 6))
    ax = fig.subplots()

    # Задаем цвет и в заголовок пишем название и оценку
    cm = plt.cm.RdBu
    ax.set_title(title + (', score is: %.5f' % score))

    # Получаем данные по x, сортируем и на основании predict рисуем график
    x_data = sorted(x.reshape(-1, 1))
    ax.plot(x_data, model.predict(x_data).reshape(-1, 1))

    ax.set_xlabel(column1)
    ax.set_ylabel(column2)

    # Выводим весь набор данных
    ax.scatter(
        x, y, cmap=cm)

    return fig
