import pandas as pd

economy_file = "dataset/countries_of_the_world.csv"
population_file = "dataset/world_population.csv"
mixed_data_file = "dataset/mixed_data.csv"


def read_csv(filename):
    return pd.read_csv(filename).dropna()


def mix_data():
    pop_data_set = read_csv(population_file).dropna()
    econ_data_set = read_csv(economy_file).dropna()
    econ_data_set['Country'] = econ_data_set['Country']\
        .apply(lambda x: x.rstrip())
    econ_data_set['Literacy (%)'] = econ_data_set['Literacy (%)']\
        .apply(lambda x: float(x.replace(',', '.')))
    econ_data_set['Infant mortality (per 1000 births)'] = econ_data_set['Infant mortality (per 1000 births)']\
        .apply(lambda x: float(x.replace(',', '.')))
    econ_data_set = econ_data_set[
        ['Country', 'GDP ($ per capita)', 'Literacy (%)', 'Infant mortality (per 1000 births)']]
    result_dataset = pd.merge(pop_data_set, econ_data_set, on='Country')
    return result_dataset


def save_data(dataset, file_name):
    dataset.to_csv(file_name)


data = mix_data()
save_data(data, mixed_data_file)
