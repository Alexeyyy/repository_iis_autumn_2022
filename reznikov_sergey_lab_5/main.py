import numpy as np
import pandas as pd
from flask import Flask, render_template, redirect, request
from matplotlib.figure import Figure

from logic.data_forming import get_image
from logic.graphics import create_plot
from logic.models import get_model, linear_regression, polynomial_regression, ridge_regression
from read_data import read_csv

app = Flask(__name__)
mix_data_file = "dataset/mixed_data.csv"
economy_file = "dataset/countries_of_the_world.csv"
population_file = "dataset/world_population.csv"


@app.route('/hello', methods=['GET', 'POST'])
def hello():
    return render_template('index.html')


@app.route('/', methods=['GET', "POST"])
def redirect_home_to_hello():
    return redirect("/hello", code=302)


@app.route('/dataset', methods=['GET', 'POST'])
def get_dataset():
    data = read_csv(mix_data_file)
    return render_template('dataset.html', tables=[data.to_html(classes='data')], titles=data.columns.values)


@app.route('/regression', methods=['GET'])
def show_regressions():
    data = read_csv(mix_data_file)

    column1 = request.args.get('column1', default="2022 Population", type=str)
    column2 = request.args.get('column2', default="1970 Population", type=str)

    x_data = np.asarray(data[column1].tolist()).reshape(-1, 1)
    y_data = np.asarray(data[column2].tolist()).reshape(-1, 1)

    model_list = [linear_regression(x_data, y_data),
                  polynomial_regression(x_data, y_data),
                  ridge_regression(x_data, y_data)]
    names_list = ["Линейная регрессия", "Полиномиальная регрессия", "Ридж регрессия"]

    pictures = []
    # Через иттератор отрисовываем каждый график в сабплотах и потом выводим его
    for i in range(len(model_list)):
        figure = create_plot(model_list[i][0], model_list[i][1], x_data, y_data, names_list[i], column1, column2)
        pictures.append(get_image(figure))

    return render_template('regressions.html', image1=pictures[0], image2=pictures[1], image3=pictures[2])


def test_regressions():
    data = read_csv(mix_data_file)
    list_of_params = ['2022 Population', '1970 Population', 'Area (km²)', 'Density (per km²)',
                      'Growth Rate', 'World Population Percentage',
                      'GDP ($ per capita)', 'Literacy (%)', 'Infant mortality (per 1000 births)']
    results = pd.DataFrame(columns=["column1", "column2", "regression", "score"])
    index = 0
    for i in range(len(list_of_params)):
        for j in range(i, len(list_of_params), 1):
            if (i == j):
                continue
            column1 = list_of_params[i]
            column2 = list_of_params[j]
            x_data = np.asarray(data[column1].tolist()).reshape(-1, 1)
            y_data = np.asarray(data[column2].tolist()).reshape(-1, 1)

            model_list = [linear_regression(x_data, y_data),
                          polynomial_regression(x_data, y_data),
                          ridge_regression(x_data, y_data)]
            names_list = ["Линейная регрессия", "Полиномиальная регрессия", "Ридж регрессия"]

            max_score = -1000
            name = names_list[0]

            for k in range(len(model_list)):
                if (model_list[k][1] > max_score):
                    max_score = model_list[k][1]
                    name = names_list[k]

            results.loc[index] = [column1] + [column2] + [name] + [max_score]
            index = index + 1

    results = results.sort_values(by=['score'], ascending=False)
    print(results.to_string())


if __name__ == '__main__':
    app.run(debug=True)
