import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_circles
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Perceptron

# 11.Данные: make_circles (noise=0.2, factor=0.5, random_state=rs)
# генерируем данные по варианту
x, y = make_circles(noise=0.2, factor=0.5, random_state=10)
x = x[:, np.newaxis, 1]
x = StandardScaler().fit_transform(x)
x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=.5, random_state=42)


# Полиномиальную регрессию (со степенью 4)
def polinominal(x_train, y_train):
    model = PolynomialFeatures(degree=4).fit(x_train, y_train)
    x_poly = model.fit_transform(x_train)
    lin = LinearRegression()
    lin.fit(x_poly, y_train)
    plt.scatter(x_train, y_train, color='green')
    plt.plot(x_train, lin.predict(x_poly), color='red')
    plt.show()
    print('Полиноминальная регрессия')
    print('Оценка качества:', lin.score(x_poly, y_train))


def lineal(x, y, x_train, y_train):
    model = LinearRegression().fit(x_train, y_train)
    plt.scatter(x, y, color='green')
    plt.plot(x, model.predict(x), color='red')
    plt.show()
    print('Линейная регрессия')
    print('Оценка качества:', model.score(x_train, y_train))


def parceptron(x_test, x_train, y_train):
    sc = StandardScaler()
    sc.fit(x_train)
    x_train_std = sc.transform(x_train)
    x_test_std = sc.transform(x_test)
    model = Perceptron(eta0=0.1, random_state=1).fit(x_train_std, y_train)
    plt.scatter(x_train, y_train, color='green')
    plt.plot(x_test_std, model.predict(x_test_std), color='red')
    plt.show()
    print('Персептрон')
    print('Оценка качества:', model.score(x_train, y_train))


lineal(x_test, y_test, x_train, y_train)
polinominal(x_train, y_train)
parceptron(x_test, x_train, y_train)
