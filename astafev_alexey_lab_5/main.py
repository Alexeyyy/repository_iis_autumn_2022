from sklearn.linear_model import Lasso
from sklearn.model_selection import train_test_split
from data import load
from ranks import calculate_mean_and_sort_list, get_ranks
from sklearn.metrics import mean_absolute_percentage_error


def calculate_mape(x_train, x_test, y_train, y_test, alpha):
    lasso = Lasso(alpha=alpha).fit(x_train, y_train)
    lasso_predict = lasso.predict(x_test)

    y_test = list(y_test)
    lasso_predict = list(lasso_predict)

    return mean_absolute_percentage_error(y_test, lasso_predict)


if __name__ == '__main__':
    alphas = [[0.1, 0.1], [0.01, 0.1], [0.1, 0.01], [0.01, 0.01], [0.01, 0.001], [0.001, 0.01], [0.001, 0.001], [0.1, 0.001], [0.001, 0.1]]
    X, Y, names = load()
    lasso = Lasso(alpha=0.001).fit(X, Y)

    ranks = get_ranks(lasso, names)

    print("MEAN", calculate_mean_and_sort_list(ranks))
    print()


    for test_size, alpha in alphas:
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size, random_state=42)

        mape = calculate_mape(X_train, X_test, Y_train, Y_test, alpha=alpha)
        print(f'MAPE: test_size={test_size} and alpha={alpha} is {float("{:.3f}".format(mape))}')





