from data_generation import dataset_generate, dataset_split
from model import linear_model, poly_model, ridge_poly_model
from plots import show_plot


def work():
    # Generate dataset
    dataset = dataset_generate()

    # Split dataset
    spited_dataset = dataset_split(dataset)

    my_linear_model, linear_model_score = linear_model(spited_dataset)
    my_poly_model, poly_model_score = poly_model(spited_dataset)
    my_polynomial_model, polynomial_model_score = ridge_poly_model(spited_dataset)

    show_plot(dataset[0], spited_dataset[0], spited_dataset[1], spited_dataset[2], spited_dataset[3],
              my_linear_model, linear_model_score,
              my_poly_model, poly_model_score,
              my_polynomial_model, polynomial_model_score)


if __name__ == '__main__':
    work()

