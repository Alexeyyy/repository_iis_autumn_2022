from sklearn.linear_model import LinearRegression, Ridge
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures


def linear_model(dataset):
    lin_reg = LinearRegression()
    lin_reg.fit(dataset[0], dataset[2])
    model_score = lin_reg.score(dataset[1], dataset[3])
    return lin_reg, model_score


def poly_model(dataset):
    pipline = make_pipeline(PolynomialFeatures(4), LinearRegression())
    pipline.fit(dataset[0], dataset[2])
    model_score = pipline.score(dataset[1], dataset[3])
    return pipline, model_score


def ridge_poly_model(dataset):
    pipline = make_pipeline(PolynomialFeatures(4), Ridge(alpha=1))
    pipline.fit(dataset[0], dataset[2])
    model_score = pipline.score(dataset[1], dataset[3])
    return pipline, model_score
