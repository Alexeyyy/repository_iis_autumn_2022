Юхлимов П. А. ПИбд-41 Вариант 27(6)

# Лабораторная 1

## Работа с типовыми наборами данных и различными моделями

### 1)Как запустить:
	- Установить библиотеки python, numpy, sklearn, matplotlib
	- Запустить программу через команду python main.py

### 2)Какие технологии использовал:
	python, numpy, sklearn, matplotlib

### 3)Что делает программа:
	Генерирует данные и обучает модели по варианту:
	- Linear Regression
	- Polynomial Regression (degree = 4)
	- Ridge Polynomial regression (degree = 4, alpha = 1.0)
	А затем выводит графики

### 4)Тесты:
![alt text](plots.jpg "Plots")

**Работы программы:**
<https://disk.yandex.ru/i/pk32KpS_0BxILw>

