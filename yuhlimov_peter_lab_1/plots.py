import numpy as np
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.colors import ListedColormap


def show_plot(X, x_train, x_test, y_train, y_test, my_linear_model, linear_model_score, my_poly_model, poly_model_score,
              pipeline, polynomial_model_score):
    h = .02  # шаг регулярной сетки
    x0_min, x0_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    x1_min, x1_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    xx0, xx1 = np.meshgrid(np.arange(x0_min, x0_max, h), np.arange(x1_min, x1_max, h))
    cm = plt.cm.RdBu

    cm_bright = ListedColormap(['#FF0000', '#0000FF'])

    for i in range(5):
        if i < 1:
            current_subplot = plt.subplot(3, 3, 2)
            current_subplot.scatter(
                x_train[:, 0], x_train[:, 1], c=y_train, cmap=cm_bright)
        elif i < 2:
            current_subplot = plt.subplot(3, 3, 5)
            current_subplot.scatter(
                x_test[:, 0], x_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)
        else:
            current_subplot = plt.subplot(3, 3, 5 + i)
            if i == 2:
                show_gradient(my_linear_model, current_subplot=current_subplot,
                              name='Linear Regression', score=linear_model_score, xx0=xx0, xx1=xx1, cm=cm)

            elif i == 3:
                show_gradient(my_poly_model, current_subplot=current_subplot,
                              name='Poly Regression', score=poly_model_score, xx0=xx0, xx1=xx1, cm=cm)

            elif i == 4:
                current_subplot.set_title('RidgePolyRegression')
                show_gradient(pipeline, current_subplot=current_subplot,
                              name='Ridge Poly Regression', score=polynomial_model_score, xx0=xx0, xx1=xx1, cm=cm)

            current_subplot.scatter(
                x_train[:, 0], x_train[:, 1], c=y_train, cmap=cm_bright)
            current_subplot.scatter(
                x_test[:, 0], x_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)

    plt.show()


# Отрисовка градиента
def show_gradient(model, current_subplot: Axes, name: str, score: float, xx0, xx1, cm):
    current_subplot.set_title(name)
    if hasattr(model, "decision_function"):
        Z = model.decision_function(np.c_[xx0.ravel(), xx1.ravel()])
    elif hasattr(model, "predict_proba"):
        Z = model.predict_proba(np.c_[xx0.ravel(), xx1.ravel()])[:, 1]
    elif hasattr(model, "predict"):
        Z = model.predict(np.c_[xx0.ravel(), xx1.ravel()])
    else:
        return

    Z = Z.reshape(xx0.shape)
    current_subplot.contourf(xx0, xx1, Z, cmap=cm, alpha=.8)
    current_subplot.set_xlim(xx0.min(), xx0.max())
    current_subplot.set_ylim(xx0.min(), xx1.max())
    current_subplot.set_xticks(())
    current_subplot.set_yticks(())
    current_subplot.set_title(name)
    current_subplot.text(xx0.max() - .3, xx1.min() + .3, ('%.2f' % score).lstrip('0'), size=15,
                         horizontalalignment='right')
