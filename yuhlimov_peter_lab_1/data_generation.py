import numpy as np
from sklearn.datasets import make_circles, make_classification
from sklearn.model_selection import train_test_split


def dataset_generate():
    X, y = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=0, n_clusters_per_class=1)
    random = np.random.RandomState(2)
    X += 2.5 * random.uniform(size=X.shape)
    return X, y


def dataset_split(dataset):
    x, y = dataset[0], dataset[1]
    return train_test_split(x, y, test_size=0.4, random_state=42)
