from sklearn.linear_model import LinearRegression, Perceptron, Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline


def launch_linear_regression(x_train, x_test, y_train, y_test):
    my_linear_model = LinearRegression()
    my_linear_model.fit(x_train, y_train)
    linear_model_score = my_linear_model.score(
        x_test, y_test)
    print('linear_model_score: ', linear_model_score)
    return my_linear_model, linear_model_score


def launch_perceptron(x_train, x_test, y_train, y_test):
    my_perceptron_model = Perceptron()
    my_perceptron_model.fit(x_train, y_train)
    perceptron_model_score = my_perceptron_model.score(
        x_test, y_test)
    print('perceptron_model_score: ', perceptron_model_score)
    return my_perceptron_model, perceptron_model_score


def launch_ridge_poly_regression(x_train, x_test, y_train, y_test):
    my_polynomial_model = PolynomialFeatures(degree=3, include_bias=False)
    ridge = Ridge(alpha=1)
    pipeline = Pipeline(
        [("polynomial_features", my_polynomial_model), ("ridge_regression", ridge)])
    pipeline.fit(x_train, y_train)
    scores = cross_val_score(pipeline, x_test, y_test,
                             scoring="neg_mean_squared_error", cv=5)
    polynomial_model_score = -scores.mean()
    print('mean polynomial_model_score: ', polynomial_model_score)
    return my_polynomial_model, polynomial_model_score
