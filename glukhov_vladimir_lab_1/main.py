from dataset import generate_dataset, split_dataset
from model import launch_linear_regression, launch_perceptron, launch_ridge_poly_regression
from plots import show_plot

x, y = generate_dataset()

x_train, x_test, y_train, y_test = split_dataset(x, y)

my_linear_model, linear_model_score = launch_linear_regression(
    x_train, x_test, y_train, y_test)
my_perceptron_model, perceptron_model_score = launch_perceptron(
    x_train, x_test, y_train, y_test)
my_polynomial_model, polynomial_model_score = launch_ridge_poly_regression(
    x_train, x_test, y_train, y_test)

show_plot(x, x_train, x_test, y_train, y_test,
          my_linear_model, linear_model_score,
          my_perceptron_model, perceptron_model_score,
          my_polynomial_model, polynomial_model_score)
