import pandas
from datetime import datetime

WINTER = 0
SPRING = 1
SUMMER = 2
AUTUMN = 3


def load(path):
    dataset = pandas.read_csv(path)

    dataset['Date'] = dataset['Date'].apply(date_to_season)
    dataset['Transaction'] = dataset['Transaction'].apply(transaction_to_number)
    dataset['Cost'] = dataset['Cost'].apply(int)

    X = dataset[['Date', 'Transaction']]
    y = dataset['Cost']

    return X, y


def date_to_season(date_str):
    date = datetime.strptime(date_str, "%Y-%m-%d")
    if date.month == 12 or (1 <= date.month <= 2):
        return WINTER
    if 3 <= date.month <= 5:
        return SPRING
    if 6 <= date.month <= 8:
        return SUMMER
    if 9 <= date.month <= 11:
        return AUTUMN


def transaction_to_number(transaction_str):
    if transaction_str == 'Option Exercise':
        return 1
    if transaction_str == 'Sale':
        return 2


def fit(model, x):
    transformed = model.fit_transform(x)

    return transformed[:, 0], transformed[:, 1]

