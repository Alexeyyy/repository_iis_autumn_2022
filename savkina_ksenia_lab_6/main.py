import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

from converting_data import get_data


def print_result(arr, name):
    print(name, ": min = ", min(arr), " median = ", np.median(arr), " max = ", max(arr), " std = ", np.std(arr))


def main():
    data = get_data()

    names = ['Your name ', 'Your gender ', 'Your age ', 'How do you describe your current level of fitness ?',
             'How often do you exercise?', 'Do you exercise ___________ ?',
             'What time if the day do you prefer to exercise?', 'How long do you spend exercising per day ?',
             'Would you say you eat a healthy balanced diet ?', 'How healthy do you consider yourself?',
             'Have you ever purchased a fitness equipment?']

    x = data[names]

    y = data['How important is exercise to you ?']

    x_train, x_test, y_train, y_test = train_test_split(x, y)

    acc_mlp = []
    acc_mlp_norm = []

    # нормализация данных
    scaler = StandardScaler()
    x_train_scaled = scaler.fit_transform(x_train)
    x_test_scaled = scaler.transform(x_test)

    for i in range(1, 6):
        print('random_state = ', i)
        # работа с нормализованными данными
        mlp = MLPClassifier(random_state=i, max_iter=2000, n_iter_no_change=20,
                            activation='tanh', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)
        mlp.fit(x_train, y_train)
        predictions = mlp.predict(x_test)
        acc = accuracy_score(y_test, predictions)
        print('MLP accuracy: ', acc)
        acc_mlp.append(acc)

        # работа с ненормализованными данными
        mlp = MLPClassifier(random_state=i, max_iter=2000, n_iter_no_change=20,
                            activation='tanh', alpha=0.01, hidden_layer_sizes=[100], tol=0.0000001)

        mlp.fit(x_train_scaled, y_train)
        predictions = mlp.predict(x_test_scaled)
        acc = accuracy_score(y_test, predictions)
        print('MLP with norm accuracy: ', acc)
        acc_mlp_norm.append(acc)

    print_result(acc_mlp, "MLP")
    print_result(acc_mlp_norm, "MLP with norm")


if __name__ == '__main__':
    main()
