import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap

def show_plot(x, xTrain, xTest, yTrain, yTest, models, scores, legendNames):
    h = .02  # шаг регулярной сетки
    x0_min, x0_max = x[:, 0].min() - .5, x[:, 0].max() + .5
    x1_min, x1_max = x[:, 1].min() - .5, x[:, 1].max() + .5
    xx0, xx1 = np.meshgrid(np.arange(x0_min, x0_max, h),
                           np.arange(x1_min, x1_max, h))
    cm = plt.cm.RdBu
    cm_bright = ListedColormap(['#FF0000', '#0000FF'])
    for i in range(9):
        current_subplot = plt.subplot(3, 3, i+1)
        if i < 3:
            current_subplot.scatter(
                xTrain[:, 0], xTrain[:, 1], c=yTrain, cmap=cm_bright)
            current_subplot.set_title(legendNames[i])
        elif i < 6:
            current_subplot.scatter(
                xTest[:, 0], xTest[:, 1], c=yTest, cmap=cm_bright, alpha=0.6)
            current_subplot.set_title('Тестовые данные')
        else:
            # current_subplot.set_title(title)
            if hasattr(models[i-6], "decision_function"):
                Z = models[i-6].decision_function(np.c_[xx0.ravel(), xx1.ravel()])
            elif hasattr(models[i-6], "predict_proba"):
                Z = models[i-6].predict_proba(np.c_[xx0.ravel(), xx1.ravel()])[:, 1]
            elif hasattr(models[i-6], "predict"):
                Z = models[i-6].predict(np.c_[xx0.ravel(), xx1.ravel()])
            else:
                return

            show_plot_gradient(current_subplot, Z, xx0, xx1, cm, scores[i-6])

            current_subplot.scatter(
                xTrain[:, 0], xTrain[:, 1], c=yTrain, cmap=cm_bright)
            current_subplot.scatter(
                xTest[:, 0], xTest[:, 1], c=yTest, cmap=cm_bright, alpha=0.6)

    plt.show()

def show_plot_gradient(current_subplot, Z, xx0, xx1, cm, score):
    Z = Z.reshape(xx0.shape)
    current_subplot.contourf(xx0, xx1, Z, cmap=cm, alpha=.8)
    current_subplot.set_xlim(xx0.min(), xx0.max())
    current_subplot.set_ylim(xx0.min(), xx1.max())
    current_subplot.set_xticks(())
    current_subplot.set_yticks(())
    current_subplot.text(xx0.max() - .3, xx1.min() + .3, ('%.2f' % score),
                         size=15, horizontalalignment='right')