from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import Ridge, Lasso, LinearRegression


def uploadPolyRegression(xTrain, xTest, yTrain, yTest):
    model = PolynomialFeatures(degree=4, include_bias=False)
    ridge = Ridge()
    pipeline = Pipeline(
        [("polynomial_features", model), ("ridge_regression", ridge)])
    pipeline.fit(xTrain, yTrain)
    scores = cross_val_score(pipeline, xTest, yTest,
                             scoring="neg_mean_squared_error", cv=5)
    score = -scores.mean()
    return pipeline, score

