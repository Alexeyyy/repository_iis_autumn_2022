from generate import generateTestdataset, generateDataset
from linearModel import uploadLinearModel
from perceptronModel import uploadPerceptron
from polyModel import uploadPolyRegression
from graph import show_plot

X,Y = generateDataset()
xTrain, xTest, yTrain, yTest = generateTestdataset(X, Y)

linearModel, LinealScore = uploadLinearModel(xTrain, xTest, yTrain, yTest)

polyModel, polyScore = uploadPolyRegression(xTrain, xTest, yTrain, yTest)

perceptronModel, perceptronScore = uploadPerceptron(xTrain, xTest, yTrain, yTest)

print('LinealScore: ', LinealScore)
print('polyScore: ', polyScore)
print('perceptronScore: ', perceptronScore)

models = [linearModel,polyModel,perceptronModel]
scores = [LinealScore, polyScore, perceptronScore]
legendNames = ['Линейная модель', 'Полиномиальная', 'Перцептрон']
show_plot(X, xTrain, xTest, yTrain, yTest,
          models, scores, legendNames)


