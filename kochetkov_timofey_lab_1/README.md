# Интеллектуальные информационные системы
## Лабораторная 1
### Кочетков Тимофей Сергеевич, ПИбд-41

**Как запустить**
python main.py
**Использованные библиотеки**
0. python
1. sklearn
2. numpy
3. matplotlib

**Работа программы**
 Вариант 15
> Сначала генерируем данные с помощью make_classification
> Потом инициализируем модели 
· Линейную регрессию 
· Полиномиальную регрессию (со степенью 4) 
· Персептрон
> получаем оценки моделей
> выводим результаты на график

Наилучший результат у полиномиальной регрессии 
Ссылка на видео: <https://drive.google.com/drive/folders/1BdLmfO0VxJNmrN3kheCBbjkLmXquEoX_?usp=sharing>