from sklearn.linear_model import Perceptron

def uploadPerceptron(xTrain, xTest, yTrain, yTest):
    model = Perceptron()
    model.fit(xTrain, yTrain)
    score = model.score(xTest, yTest)
    return model, score
