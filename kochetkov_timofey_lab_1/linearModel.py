from sklearn.linear_model import LinearRegression

def uploadLinearModel(xTrain, xTest, yTrain, yTest):
    model = LinearRegression()
    model.fit(xTrain, yTrain)
    score = model.score(xTest, yTest)
    return model, score
