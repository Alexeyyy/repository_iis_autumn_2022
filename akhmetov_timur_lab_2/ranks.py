import numpy as np
from sklearn.preprocessing import MinMaxScaler


def get_ranks(linear, rfr, f):
    ranks = dict()
    features = ["x%s" % i for i in range(1, 15)]

    ranks['Linear'] = rank_to_dict(linear.coef_, features)
    ranks['RFR'] = rank_to_dict(rfr.feature_importances_, features)
    ranks['f_reg'] = rank_to_dict(f, features)

    return ranks


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()

    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def mean_calc_and_sort(ranks):
    mean = {}

    for key, value in ranks.items():
        print(key, value)
        for item in value.items():
            if item[0] not in mean:
                mean[item[0]] = 0
                mean[item[0]] += item[1]

    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    return mean




