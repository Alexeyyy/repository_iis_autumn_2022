from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import f_regression
from sklearn.linear_model import LinearRegression

from data import generate_data
from ranks import mean_calc_and_sort, get_ranks


if __name__ == '__main__':
    x, y = generate_data()

    linear = LinearRegression()
    linear.fit(x, y)

    rfr = RandomForestRegressor(bootstrap=True)
    rfr.fit(x, y)

    f, p_val = f_regression(x, y, center=True)

    ranks = get_ranks(linear, rfr, f)

    print("mean", mean_calc_and_sort(ranks))
