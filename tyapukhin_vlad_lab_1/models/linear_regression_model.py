from sklearn.linear_model import LinearRegression


class LinearRegressionModel:
    def __init__(self, x_train, x_test, y_train, y_test):
        self.x_train = x_train
        self.x_test = x_test
        self.y_train = y_train
        self.y_test = y_test

        self.__instance = self.__create_instance()
        self.__score = self.__calculate_score()

    def __create_instance(self):
        model = LinearRegression()
        model.fit(self.x_train, self.y_train)
        return model

    def __calculate_score(self):
        return self.__instance.score(self.x_test, self.y_test)

    def get_instance_and_score(self):
        return self.__instance, self.__score
