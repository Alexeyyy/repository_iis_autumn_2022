from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline


class RidgePolyRegressionModel:
    def __init__(self, x_train, x_test, y_train, y_test):
        self.x_train = x_train
        self.x_test = x_test
        self.y_train = y_train
        self.y_test = y_test

        self.__instance = self.__create_instance()
        self.__score = self.__calculate_score()

    def __create_instance(self):
        polynomial_model = PolynomialFeatures(degree=3, include_bias=False)
        ridge = Ridge(alpha=1)
        pipeline = Pipeline([("polynomial_features", polynomial_model), ("ridge_regression", ridge)])
        pipeline.fit(self.x_train, self.y_train)
        return pipeline

    def __calculate_score(self):
        return -cross_val_score(self.__instance, self.x_test, self.y_test, scoring="neg_mean_squared_error", cv=5).mean()

    def get_instance_and_score(self):
        return self.__instance, self.__score
