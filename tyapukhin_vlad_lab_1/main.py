from models.linear_regression_model import LinearRegressionModel
from models.poly_regression_model import PolyRegressionModel
from models.ridge_poly_regression_model import RidgePolyRegressionModel

from plots import show_plot
from data import Data


if __name__ == '__main__':

    data = Data()

    x, y = data.x, data.y

    x_train, x_test, y_train, y_test = data.x_train, data.x_test, data.y_train, data.y_test

    linear_model, linear_model_score = LinearRegressionModel(
        x_train,
        x_test,
        y_train,
        y_test,
    ).get_instance_and_score()

    polynomial_model, polynomial_model_score = PolyRegressionModel(
        x_train,
        x_test,
        y_train,
        y_test,
    ).get_instance_and_score()

    ridge_polynomial_model, ridge_polynomial_model_score = RidgePolyRegressionModel(
        x_train,
        x_test,
        y_train,
        y_test,
    ).get_instance_and_score()

    show_plot(
        x,
        x_train,
        x_test,
        y_train,
        y_test,
        linear_model,
        linear_model_score,
        polynomial_model,
        polynomial_model_score,
        ridge_polynomial_model,
        ridge_polynomial_model_score,
    )
