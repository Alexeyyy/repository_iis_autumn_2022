import numpy as np
from sklearn.datasets import make_circles
from sklearn.model_selection import train_test_split


class Data:
    def __init__(self):
        self.x = None
        self.y = None

        self.__generate_dataset()
        self.x_train, self.x_test, self.y_train, self.y_test = self.__split_dataset()

    def __generate_dataset(self):
        x, y = make_circles(noise=0.2, factor=0.5, random_state=1)
        x += 3 * np.random.RandomState(2).uniform(size=x.shape)
        self.x, self.y = x, y

    def __split_dataset(self):
        return train_test_split(self.x, self.y, test_size=.05, random_state=42)
