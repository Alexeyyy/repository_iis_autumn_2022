import numpy as np
from enum import Enum
from matplotlib.colors import ListedColormap
from matplotlib.axes import Axes
from matplotlib import pyplot as plt

TRAIN_DATA_ROW_LENGTH = 3
TEST_DATA_ROW_LENGTH = 6


class Regression(Enum):
    LINEAR = 6
    POLYNOMIAL = 7
    RIDGE_POLYNOMIAL = 8


def show_plot_based_on_train_data(current_subplot, x_train, y_train, cm_bright):
    current_subplot.scatter(
        x_train[:, 0],
        x_train[:, 1],
        c=y_train,
        cmap=cm_bright,
    )


def show_plot_based_on_test_data(current_subplot, x_test, y_test, cm_bright):
    current_subplot.scatter(
        x_test[:, 0],
        x_test[:, 1],
        c=y_test,
        cmap=cm_bright,
        alpha=0.6,
    )


def show_plot(
        x,
        x_train,
        x_test,
        y_train,
        y_test,
        linear_model,
        linear_model_score,
        polynomial_model,
        polynomial_model_score,
        ridge_polynomial_model,
        ridge_polynomial_model_score,
):
    h = .02
    x0_min, x0_max = x[:, 0].min() - .5, x[:, 0].max() + .5
    x1_min, x1_max = x[:, 1].min() - .5, x[:, 1].max() + .5
    xx0, xx1 = np.meshgrid(
        np.arange(x0_min, x0_max, h),
        np.arange(x1_min, x1_max, h)
    )
    cm = plt.cm.RdBu

    cm_bright = ListedColormap(['#0000FF', '#FF0000'])

    fig = plt.figure(figsize=(14, 8))

    def define_model_by_index(index):
        match index:
            case Regression.LINEAR.value:
                return linear_model, 'Linear regression', linear_model_score
            case Regression.POLYNOMIAL.value:
                return polynomial_model, 'Polynomial regression', polynomial_model_score
            case Regression.RIDGE_POLYNOMIAL.value:
                return ridge_polynomial_model, 'Ridge polynomial regression', ridge_polynomial_model_score

    for i in range(9):
        current_subplot = plt.subplot(3, 3, i + 1)
        fig.add_subplot(current_subplot)

        if i < TRAIN_DATA_ROW_LENGTH:
            show_plot_based_on_train_data(current_subplot, x_train, y_train, cm_bright)
        elif i < TEST_DATA_ROW_LENGTH:
            show_plot_based_on_test_data(current_subplot, x_test, y_test, cm_bright)
        else:
            model, title, score = define_model_by_index(i)
            show_gradient(model, current_subplot=current_subplot, title=title, score=score, xx0=xx0, xx1=xx1, cm=cm)

            current_subplot.scatter(x_train[:, 0], x_train[:, 1], c=y_train, cmap=cm_bright)
            current_subplot.scatter(x_test[:, 0], x_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)

    plt.show()


def show_gradient(model, current_subplot: Axes, title: str, score: float, xx0, xx1, cm):
    current_subplot.set_title(title, loc='left', fontdict={'fontsize': 15})
    if hasattr(model, "decision_function"):
        Z = model.decision_function(np.c_[xx0.ravel(), xx1.ravel()])
    elif hasattr(model, "predict_proba"):
        Z = model.predict_proba(np.c_[xx0.ravel(), xx1.ravel()])[:, 1]
    elif hasattr(model, "predict"):
        Z = model.predict(np.c_[xx0.ravel(), xx1.ravel()])
    else:
        return

    Z = Z.reshape(xx0.shape)
    current_subplot.contourf(xx0, xx1, Z, cmap=cm, alpha=.5)
    current_subplot.set_xlim(xx0.min(), xx0.max())
    current_subplot.set_ylim(xx0.min(), xx1.max())
    current_subplot.set_xticks(())
    current_subplot.set_yticks(())
    current_subplot.text(xx0.max() - .3, xx1.min() + .3, ('%.2f' % score), size=20, horizontalalignment='right')
