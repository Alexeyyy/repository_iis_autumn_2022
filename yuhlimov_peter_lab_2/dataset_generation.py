import numpy as np


def generate():
    np.random.seed(0)
    size = 750
    X = np.random.uniform(0, 1, (size, 14))
    # Задаем функцию-выход: регрессионную проблему Фридмана
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y
