import dataset_generation
from models import rfe_model, rand_lasso_model, lasso_model
from models_processing import rank_to_dict, process_mean, range_model_ranks


def work():
    X, Y = dataset_generation.generate()
    rfe = rfe_model(X, Y)
    rand_lasso = rand_lasso_model(X, Y)
    lasso = lasso_model(X, Y)

    ranks = {"Lasso": rank_to_dict(lasso.coef_), "Randomize Lasso": rank_to_dict(rand_lasso.coef_),
             "Recursive Feature Elimination": rank_to_dict(rfe.ranking_)}

    process_mean(ranks)
    print()
    range_model_ranks(ranks)


if __name__ == '__main__':
    work()
