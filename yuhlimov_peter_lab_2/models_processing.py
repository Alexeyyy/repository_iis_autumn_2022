from sklearn.preprocessing import MinMaxScaler
import numpy as np

names = ["x%s" % i for i in range(1, 15)]


def rank_to_dict(ranks):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def process_mean(ranks: {}):
    # Создаем пустой список для данных
    mean = {}
    # «Бежим» по списку ranks
    for key, value in ranks.items():
        # «Пробегаемся» по списку значений ranks, которые являются парой имя:оценка
        for item in value.items():
            # имя будет ключом для нашего mean
            # если элемента с текущим ключем в mean нет - добавляем
            if item[0] not in mean:
                mean[item[0]] = 0
            # суммируем значения по каждому ключу-имени признака
            mean[item[0]] += item[1]

    # находим среднее по каждому признаку
    for key, value in mean.items():
        res = value / len(ranks)
        mean[key] = round(res, 2)

    # сортируем и распечатываем список
    mean = sorted(mean.items(), key=lambda item: item[1], reverse=True)
    print(mean)


def range_model_ranks(ranks: {}):
    for key, value in ranks.items():
        ranks[key] = sorted(value.items(), key=lambda item: item[1], reverse=True)
    for key, value in ranks.items():
        print(key)
        print(value)
