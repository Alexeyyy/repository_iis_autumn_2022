from RandomizedLasso import RandomizedLasso
from sklearn.feature_selection import RFE
from sklearn.linear_model import Lasso, LinearRegression


def rfe_model(X, Y):
    lr = LinearRegression()
    lr.fit(X, Y)

    rfe = RFE(lr)
    rfe.fit(X, Y)
    return rfe


def rand_lasso_model(X, Y):
    rand_lasso = RandomizedLasso(alpha=.05)
    rand_lasso.fit(X, Y)
    return rand_lasso


def lasso_model(X, Y):
    lasso = Lasso(alpha=.05)
    lasso.fit(X, Y)
    return lasso
