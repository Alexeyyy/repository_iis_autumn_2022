import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

# Load dataset
data = pd.read_csv('titanic.csv', index_col='Passengerid')
# Init Classifier
clf = DecisionTreeClassifier(random_state=241)

# Choose features
Y = data['2urvived']
X = data[['Pclass', 'Age', 'Fare', ]]
print(X)

# Split dataset
X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.05, random_state=42)

# Train model
clf.fit(X_train, y_train)

# Show score of model
print(clf.score(X_test, y_test))

# Show feature importances
importances = clf.feature_importances_
print(importances)
