import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

# Load dataset
data = pd.read_csv('genres_features_3_sec.csv', index_col='filename')
# Init Classifier
clf = DecisionTreeClassifier(random_state=241)

# Choose features
Y = data['label']
X = data[['chroma_stft_var',
          'rms_var',
          'spectral_centroid_var',
          'spectral_bandwidth_var',
          'rolloff_var',
          'zero_crossing_rate_var',
          'harmony_var',
          'perceptr_var',
          'tempo',
          'mfcc1_var',
          'mfcc2_var',
          'mfcc3_var',
          'mfcc4_var',
          'mfcc5_var',
          'mfcc6_var',
          'mfcc7_var',
          'mfcc8_var',
          'mfcc9_var',
          'mfcc10_var',
          'mfcc11_var',
          'mfcc12_var',
          'mfcc13_var',
          'mfcc14_var',
          'mfcc15_var',
          'mfcc16_var',
          'mfcc17_var',
          'mfcc18_var',
          'mfcc19_var',
          'mfcc20_var',
          'chroma_stft_mean',
          'rms_mean',
          'spectral_centroid_mean',
          'spectral_bandwidth_mean',
          'rolloff_mean',
          'zero_crossing_rate_mean',
          'harmony_mean',
          'perceptr_mean',
          'mfcc1_mean',
          'mfcc2_mean',
          'mfcc3_mean',
          'mfcc4_mean',
          'mfcc5_mean',
          'mfcc6_mean',
          'mfcc7_mean',
          'mfcc8_mean',
          'mfcc9_mean',
          'mfcc10_mean',
          'mfcc11_mean',
          'mfcc12_mean',
          'mfcc13_mean',
          'mfcc14_mean',
          'mfcc15_mean',
          'mfcc16_mean',
          'mfcc17_mean',
          'mfcc18_mean',
          'mfcc19_mean',
          'mfcc20_mean',
          ]]
print(X)

# Split dataset
X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.01, random_state=42)

# Train model
clf.fit(X_train, y_train)

# Show score of model
print(clf.score(X_test, y_test))

# Show feature importances
importances = clf.feature_importances_
print(importances)
