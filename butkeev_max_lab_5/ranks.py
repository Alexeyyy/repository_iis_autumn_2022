import numpy as np
from sklearn.linear_model import LogisticRegression, Lasso
from sklearn.preprocessing import MinMaxScaler


def rank_to_dict(ranks, names):

    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(
        np.array(ranks).reshape(len(names), 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_ranks(data):

    clf = Lasso(alpha=0.001)
    clf.fit(data[['Genre', 'Runtime']], data['IMDB Score'])

    ranks = dict()
    ranks["LASSO"] = rank_to_dict(clf.coef_, ['Genre', 'Runtime'])
    print(ranks)
    return ranks
