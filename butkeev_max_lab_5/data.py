from datetime import datetime

import pandas

from constants import NUMBER_OF_LINES


def rating(score):
    if score >= 4:
        score = 3
    elif score >= 3 and score < 4:
        score = 2
    elif score < 3:
        score = 1
    return score


def create_data():
    data = pandas.read_csv('Netflix_Originals.csv', on_bad_lines='skip')

    data['IMDB Score'].apply(rating)

    unique_numbers = list(set(data['Genre']))
    data['Genre'] = data['Genre'].apply(unique_numbers.index)

    return data
