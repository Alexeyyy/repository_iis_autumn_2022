import pandas
from sklearn.tree import DecisionTreeClassifier

if __name__ == "__main__":
    # Читаем данные из файла
    data = pandas.read_csv('resources/titanic_data.csv', index_col='PassengerId')
    # Удаляем nan'ы
    data.dropna()
    # Выбираем нужные массивы данных
    corr = data[['Pclass', 'Parch', 'Fare']]
    print(corr)
    y = data['Survived']
    # Создаём и обучаем дерево
    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(corr, y)
    # Получаем и выводим параметры
    parameters = clf.feature_importances_
    print(parameters)
    score = clf.score(corr, y)
    print(score)
