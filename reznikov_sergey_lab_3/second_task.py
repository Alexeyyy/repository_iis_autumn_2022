import pandas
from sklearn.tree import DecisionTreeClassifier


# Приводим девайсы к инту по степени мобильности (чем мобильнее, тем выше значение)
def device_to_int(device):
    if device == "Mobile":
        return 2
    elif device == "Tab":
        return 1
    return 0


# Читаем данные из файла и удаляем nan
data = pandas.read_csv('resources/students.csv')
data.dropna()
print(data)

# Обработка девайсов в приемлемый вид
data['Device'] = data['Device'].apply(device_to_int)

# Получаем тестовую и тренеровочную выборки
train_data = data.iloc[0:int(len(data) * 0.99), :]
test_data = data.iloc[int(len(data) * 0.99):, :]

# Создаём и обучаем дерево
clf = DecisionTreeClassifier(random_state=241)
clf.fit(train_data[['Age', 'Device']],
        train_data['Flexibility Level'])

# Получаем и выводим параметры
parameters = clf.feature_importances_
print(parameters)
score = clf.score(test_data[['Age', 'Device']],
                  test_data['Flexibility Level'])
print(score)
