import matplotlib.pyplot as matPylt
import numpy as npy
from sklearn import metrics
from sklearn.datasets import make_moons
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split

def linear(X_train, X_test, y_train, y_test):
    model = LinearRegression().fit(X_train, y_train)
    y_predict = model.predict(X_test)
    matPylt.title('Линейное регрессия')
    matPylt.scatter(X_test, y_test, color='gray')
    matPylt.plot(X_test, y_predict, color='orange')
    matPylt.show()
    print('Линейное регрессия')
    print('Среднее абсолютное ошибк:', metrics.mean_absolute_error(y_test, y_predict))
    print('Среднеквадратичное ошибк:', metrics.mean_squared_error(y_test, y_predict))


def polynomial(X_train, y_train):
    polynomial_features = PolynomialFeatures(degree=3)
    X_polynomial = polynomial_features.fit_transform(X_train)
    base_model = LinearRegression()
    base_model.fit(X_polynomial, y_train)
    y_predict = base_model.predict(X_polynomial)
    matPylt.title('Полиномиальное регрессия')
    matPylt.scatter(X_train, y_train, color='gray')
    matPylt.plot(X_train, y_predict, color='orange')
    matPylt.show()
    print('Полиномиальное регрессия:')
    print('Среднее абсолютное ошибк:', metrics.mean_absolute_error(y_train, y_predict))
    print('Среднеквадратичное ошибк:', metrics.mean_squared_error(y_train, y_predict))


def ridge(X_train, X_test, y_train, y_test):
    model = Pipeline([('poly', PolynomialFeatures(degree=3)), ('ridge', Ridge(alpha=1.0))])
    model.fit(X_train, y_train)
    y_predict = model.predict(X_test)
    matPylt.title('Гребневое полиномиальное регрессия')
    matPylt.scatter(X_test, y_test, color='gray')
    matPylt.plot(X_test, y_predict, color='orange')
    matPylt.show()
    print('Гребневое полиномиальное регрессия:')
    print('Среднее абсолютное ошибк:', metrics.mean_absolute_error(y_test, y_predict))
    print('Среднеквадратичное ошибк:', metrics.mean_squared_error(y_test, y_predict))


moon_dataset = make_moons(noise=0.3, random_state=0)
X, y = moon_dataset
X = X[:, npy.newaxis, 1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)
linear(X_train, X_test, y_train, y_test)
polynomial(X_train, y_train)
ridge(X_train, X_test, y_train, y_test)
