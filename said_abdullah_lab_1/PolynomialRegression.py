from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from matplotlib import pyplot as plt
from sklearn import metrics
import numpy as np


class Polynomial:

    def polynomial(self, X, y):
        poly_reg = PolynomialFeatures(degree=3)
        X_poly = poly_reg.fit_transform(X)
        pol_reg = LinearRegression()
        pol_reg.fit(X_poly, y)
        y_pred = pol_reg.predict(X_poly)

        print('2. Polynomial Regression:')
        print('-Mean Absolute Error:', metrics.mean_absolute_error(y, y_pred))
        print('-Mean Squared Error:', metrics.mean_squared_error(y, y_pred))
        print('-Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y, y_pred)))
        print('')

        plt.scatter(X, y, color='red')
        plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
        plt.show()
