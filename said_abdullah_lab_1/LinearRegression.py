from sklearn.linear_model import LinearRegression
from matplotlib import pyplot as plt
from sklearn import metrics
import numpy as np


class Linear:

    def linear(self, X_train, X_test, y_train, y_test):
        model = LinearRegression().fit(X_train, y_train)
        y_pred = model.predict(X_test)

        print('1. Linear Regression:')
        print('-Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
        print('-Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
        print('-Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))
        print('')

        plt.scatter(X_test, y_test, color='red')
        plt.plot(X_test, y_pred, color='blue', linewidth=3)
        plt.show()
