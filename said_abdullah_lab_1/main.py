import numpy as np
from sklearn.datasets import make_circles
from LinearRegression import Linear
from PolynomialRegression import Polynomial
from RidgePolynomialRegression import RidgeRegression

models_1 = Linear()
models_2 = Polynomial()
models_3 = RidgeRegression()

data = make_circles(noise=0.2, factor=0.5, random_state=1)

X = np.array(data[0])
y = np.array(data[1])

X = X[:, np.newaxis, 1]

X_train = X[:-20]
X_test = X[-20:]
y_train = y[:-20]
y_test = y[-20:]

models_1.linear(X_train, X_test, y_train, y_test)

models_2.polynomial(X_train, y_train)

models_3.ridge(X_train, X_test, y_train, y_test)
