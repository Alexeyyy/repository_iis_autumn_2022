from sklearn.linear_model import Ridge
from matplotlib import pyplot as plt
from sklearn import metrics
import numpy as np


class RidgeRegression:

    def ridge(self, X_train, X_test, y_train, y_test):
        clf = Ridge(alpha=1.0)
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)

        print('3. Ridged Polynomial Regression:')
        print('-Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
        print('-Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
        print('-Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))

        plt.scatter(X_test, y_test, color='red')
        plt.plot(X_test, y_pred, color='blue', linewidth=3)
        plt.show()
