# import pandas
import pandas
from sklearn.tree import DecisionTreeClassifier


def main2():
    data = pandas.read_csv('Mobile dataset.csv')
    data.dropna()
    print(data)


    data['display_size'] = data['display_size'].apply(int)

    train_data = data.iloc[0:int(len(data) * 0.99), :]
    test_data = data.iloc[0:int(len(data) * 0.99):, :]

    clf = DecisionTreeClassifier(random_state=41)
    clf.fit(train_data[['display_size', 'num_of_ratings']],
            train_data['sales_price'])

    # Выводим значение
    parameters = clf.feature_importances_
    print(parameters)
    score = clf.score(test_data[['display_size', 'num_of_ratings']],
                      test_data['sales_price'])
    print(score)
