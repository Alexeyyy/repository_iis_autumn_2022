import numpy as np
from sklearn.feature_selection import RFE
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.preprocessing import MinMaxScaler


def generate_data():
    np.random.seed(0)
    size = 750
    X = np.random.uniform(0, 1, (size, 14))
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y


def rank_to_dict(ranks, names, isRFE):
    if isRFE:
        ranks = map(lambda x: 1 / x, ranks)
    else:
        ranks = np.abs(ranks)
        minmax = MinMaxScaler()
        ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def generate_mean(ranks):
    mean_dict = {}
    for key, value in ranks.items():
        for item in value:
            if item not in mean_dict:
                mean_dict[item] = 0
            mean_dict[item] += value[item]
    for key, value in mean_dict.items():
        res = value / len(ranks)
        mean_dict[key] = round(res, 2)

    return mean_dict


def sort_dict(dict):
    sorted_tuples = sorted(dict.items(), key=lambda item: item[1], reverse=True)
    sorted_dict = {k: v for k, v in sorted_tuples}
    return sorted_dict


def main():
    X, Y = generate_data()

    lr = LinearRegression()
    lr.fit(X, Y)

    lasso = Lasso(alpha=.05)
    lasso.fit(X, Y)

    rfe = RFE(lr)
    rfe.fit(X, Y)

    names = ["x%s" % i for i in range(1, 15)]

    ranks = {"Linear reg": rank_to_dict(lr.coef_, names, isRFE=False),
             "Lasso": rank_to_dict(lasso.coef_, names, isRFE=False),
             "RFE": rank_to_dict(rfe.ranking_, names, isRFE=True)}

    for key, value in ranks.items():
        ranks[key] = sort_dict(value)
    for key, value in ranks.items():
        print(key)
        print(value)

    mean_dict = generate_mean(ranks)

    mean = sort_dict(mean_dict)
    print("MEAN")
    print(mean)


if __name__ == "__main__":
    main()
