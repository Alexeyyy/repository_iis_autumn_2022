from funcs import *

x, y = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=0,
                           n_clusters_per_class=1)

x = x[:, np.newaxis, 1]

x_train, x_test, y_train, y_test = train_test_split(x, y)

lin(x_train, x_test, y_train, y_test)
polynom(x_train, y_train)
greb_polynom(x_train, x_test, y_train, y_test)
